��7Q      �docutils.nodes��document���)��}�(�refnames�}��indirect_targets�]��substitution_names�}�hh�
decoration�N�	rawsource�� ��ids�}�(�creating-the-user-configuration�h �section���)��}�(�
attributes�}�(�classes�]��ids�]�ha�backrefs�]��dupnames�]��names�]��creating the user configuration�au�line�KI�parent�h)��}�(h}�(h]�h]��volatility-implicitation�ah]�h]�h]��volatility implicitation�auh"Kh#h�source��bC:\Users\j_hat\OneDrive\Documents\americanpricer\Docs\source\tutorials\vol_implicitation\index.rst�hh�tagname�hhh�children�]�(h �title���)��}�(h}�(h]�h]�h]�h]�h]�uh"Kh#h$h.h/hhh0h3h�Volatility Implicitation�h1]�h �Text����Volatility Implicitation�����}�(hh=h#h5ubaubh �topic���)��}�(h}�(h]�h]�h]�h]�h]�uh"Nh#h$h.h/hhh0hEh��In this section, we will introduce the different steps in order to get a volatility smile from the market
quotations of american options.�h1]�(h4)��}�(h}�(h]�h]�h]�h]�h]�uh#hGh0h3h�Section contents�h1]�h@�Section contents�����}�(hhYh#hQubaubh �	paragraph���)��}�(h}�(h]�h]�h]�h]�h]�uh"Kh#hGh.h/h0h_h��In this section, we will introduce the different steps in order to get a volatility smile from the market
quotations of american options.�h1]�h@��In this section, we will introduce the different steps in order to get a volatility smile from the market
quotations of american options.�����}�(hhih#haubaubeubh)��}�(h}�(h]�h]��specifying-the-underlying�ah]�h]�h]��specifying the underlying�auh"Kh#h$h.h/hhh0hhhh1]�(h4)��}�(h}�(h]�h]�h]�h]�h]�uh"Kh#hoh.h/hhh0h3h�Specifying the underlying�h1]�h@�Specifying the underlying�����}�(hh�h#hzubaubh`)��}�(h}�(h]�h]�h]�h]�h]�uh"Kh#hohhh.h/h0h_h��First, one needs to specify the futures contract on which the option is written. This is defined by an underlying name
such as GOLD or WTI and a futures maturity.�h1]�h@��First, one needs to specify the futures contract on which the option is written. This is defined by an underlying name
such as GOLD or WTI and a futures maturity.�����}�(hh�h#h�ubaubh`)��}�(h}�(h]�h]�h]�h]�h]�uh"Kh#hohhh.h/h0h_h��The classes relevant to contract creation are located in the module Backends. We will first import the needed
classes from there. We will also need to import :class:`~datetime.date` in order to specify the maturity.�h1]�(h@��The classes relevant to contract creation are located in the module Backends. We will first import the needed
classes from there. We will also need to import �����}�(h��The classes relevant to contract creation are located in the module Backends. We will first import the needed
classes from there. We will also need to import �h#h�ub�sphinx.addnodes��pending_xref���)��}�(h}�(�	py:module�N�refwarn���	reftarget��datetime.date��	refdomain��py�h]�h]��refexplicit��h]�h]��reftype��class��py:class�N�refdoc��!tutorials/vol_implicitation/index�h]�uh"Kh#h�h.h/h0h�h�:class:`~datetime.date`�h1]�h �literal���)��}�(h}�(h]�(�xref�h��py-class�eh]�h]�h]�h]�uh#h�h0h�hh�h1]�h@�date�����}�(hhh#h�ubaubaubh@�" in order to specify the maturity.�����}�(h�" in order to specify the maturity.�h#h�ubeubh �doctest_block���)��}�(h}�(h]��	xml:space��preserve�h]�h]�h]�h]�uh"Nh#hoh.Nhhh0h�h�H>>> from Backends.Contract import Contract
>>> from datetime import date�h1]�h@�H>>> from Backends.Contract import Contract
>>> from datetime import date�����}�(hhh#h�ubaubh`)��}�(h}�(h]�h]�h]�h]�h]�uh"Kh#hohhh.h/h0h_h�6Then we specify the different elements of the contract�h1]�h@�6Then we specify the different elements of the contract�����}�(hh�h#h�ubaubh�)��}�(h}�(h]�h�h�h]�h]�h]�h]�uh"Nh#hoh.Nhhh0h�h��>>> from Backends.Contract import Contract
>>> from datetime import date
>>> contract_maturity = date(2017,12,1)
>>> index_name = "WTI"
>>> contract = Contract(index_name, contract_maturity)�h1]�h@��>>> from Backends.Contract import Contract
>>> from datetime import date
>>> contract_maturity = date(2017,12,1)
>>> index_name = "WTI"
>>> contract = Contract(index_name, contract_maturity)�����}�(hhh#h�ubaubeubh)��}�(h}�(h]�h]��specifying-quotation-dates�ah]�h]�h]��specifying quotation dates�auh"Kh#h$h.h/hhh0hhhh1]�(h4)��}�(h}�(h]�h]�h]�h]�h]�uh"Kh#j  h.h/hhh0h3h�Specifying quotation dates�h1]�h@�Specifying quotation dates�����}�(hj  h#j  ubaubh`)��}�(h}�(h]�h]�h]�h]�h]�uh"K!h#j  hhh.h/h0h_hX^  Once we define the contract, we need to define the different dates for which the smile needs to be calculated. The only
quantities needed for that are a start date and an end date that will be included in the time interval. All the dates in
between and for which option quotations are available in the database will be effectively taken into account.�h1]�h@X^  Once we define the contract, we need to define the different dates for which the smile needs to be calculated. The only
quantities needed for that are a start date and an end date that will be included in the time interval. All the dates in
between and for which option quotations are available in the database will be effectively taken into account.�����}�(hj#  h#j  ubaubh`)��}�(h}�(h]�h]�h]�h]�h]�uh"K%h#j  hhh.h/h0h_h�DThe definition of these dates is simply done by adding the two lines�h1]�h@�DThe definition of these dates is simply done by adding the two lines�����}�(hj1  h#j)  ubaubh�)��}�(h}�(h]�h�h�h]�h]�h]�h]�uh"Nh#j  h.Nhhh0h�hX
  >>> from Backends.Contract import Contract
>>> from datetime import date
>>> contract_maturity = date(2017,12,1)
>>> index_name = "WTI"
>>> contract = Contract(index_name, contract_maturity)
>>> start_value_date = date(2017, 1, 1)
>>> end_value_date = date(2017,1,4)�h1]�h@X
  >>> from Backends.Contract import Contract
>>> from datetime import date
>>> contract_maturity = date(2017,12,1)
>>> index_name = "WTI"
>>> contract = Contract(index_name, contract_maturity)
>>> start_value_date = date(2017, 1, 1)
>>> end_value_date = date(2017,1,4)�����}�(hhh#j7  ubaubeubh)��}�(h}�(h]�h]��!choosing-the-smile-s-granualarity�ah]�h]�h]��!choosing the smile's granualarity�auh"K1h#h$h.h/hhh0hhhh1]�(h4)��}�(h}�(h]�h]�h]�h]�h]�uh"K1h#jE  h.h/hhh0h3h�!Choosing the smile's granualarity�h1]�h@�!Choosing the smile's granualarity�����}�(hjX  h#jP  ubaubh`)��}�(h}�(h]�h]�h]�h]�h]�uh"K3h#jE  hhh.h/h0h_hX"  For a given contract, many options with many strikes are quoted in the market. The smile is parametrized using a sub set
set of these options in order to have a reduced number of parameters. The user can choose to keep 5 or 7 strikes that
would be sufficient to reproduce the entire strike.�h1]�h@X"  For a given contract, many options with many strikes are quoted in the market. The smile is parametrized using a sub set
set of these options in order to have a reduced number of parameters. The user can choose to keep 5 or 7 strikes that
would be sufficient to reproduce the entire strike.�����}�(hjf  h#j^  ubaubh`)��}�(h}�(h]�h]�h]�h]�h]�uh"K7h#jE  hhh.h/h0h_h��To specify the strikes for which the volatility will be computed, one should specify the different moneynesses represented in
deltas. This is illustrated in the added line below:�h1]�h@��To specify the strikes for which the volatility will be computed, one should specify the different moneynesses represented in
deltas. This is illustrated in the added line below:�����}�(hjt  h#jl  ubaubh�)��}�(h}�(h]�h�h�h]�h]�h]�h]�uh"Nh#jE  h.Nhhh0h�hX[  >>> from Backends.Contract import Contract
>>> from datetime import date
>>> contract_maturity = date(2017,12,1)
>>> index_name = "WTI"
>>> contract = Contract(index_name, contract_maturity)
>>> start_value_date = date(2017, 1, 1)
>>> end_value_date = date(2017,1,4)
>>> import numpy as np
>>> moneynesses = np.array([0.05, 0.25, 0.5, 0.75, 0.95])�h1]�h@X[  >>> from Backends.Contract import Contract
>>> from datetime import date
>>> contract_maturity = date(2017,12,1)
>>> index_name = "WTI"
>>> contract = Contract(index_name, contract_maturity)
>>> start_value_date = date(2017, 1, 1)
>>> end_value_date = date(2017,1,4)
>>> import numpy as np
>>> moneynesses = np.array([0.05, 0.25, 0.5, 0.75, 0.95])�����}�(hhh#jz  ubaubh`)��}�(h}�(h]�h]�h]�h]�h]�uh"KDh#jE  hhh.h/h0h_h��For example, the moneyness 0.5 corresponds to that At-The-Money strike (equal to the underlying price), whereas the
moneyness 0.95 corresponds to a strike lower that the underlying level and 0.05 correponds to a strike higher than the
underlying level.�h1]�h@��For example, the moneyness 0.5 corresponds to that At-The-Money strike (equal to the underlying price), whereas the
moneyness 0.95 corresponds to a strike lower that the underlying level and 0.05 correponds to a strike higher than the
underlying level.�����}�(hj�  h#j�  ubaubeubhh)��}�(h}�(h]�h]��building-the-volatility-smile�ah]�h]�h]��building the volatility smile�auh"K]h#h$h.h/hhh0hhhh1]�(h4)��}�(h}�(h]�h]�h]�h]�h]�uh"K]h#j�  h.h/hhh0h3h�Building the volatility smile�h1]�h@�Building the volatility smile�����}�(hj�  h#j�  ubaubh`)��}�(h}�(h]�h]�h]�h]�h]�uh"K_h#j�  hhh.h/h0h_h��In order to build the smile, the four previously presented elements (underlying, dates, moneynesses and configuration)
should be passed to a builder situated in the module Smile.Implicitation. To import it, one should add the following lines�h1]�h@��In order to build the smile, the four previously presented elements (underlying, dates, moneynesses and configuration)
should be passed to a builder situated in the module Smile.Implicitation. To import it, one should add the following lines�����}�(hj�  h#j�  ubaubh�)��}�(h}�(h]�h�h�h]�h]�h]�h]�uh"Nh#j�  h.Nhhh0h�h�k>>> from Smile.Implicitation.MoneynessSmileRepresentationBuilder import MoneynessSmileRepresentationBuilder�h1]�h@�k>>> from Smile.Implicitation.MoneynessSmileRepresentationBuilder import MoneynessSmileRepresentationBuilder�����}�(hhh#j�  ubaubh`)��}�(h}�(h]�h]�h]�h]�h]�uh"Kdh#j�  hhh.h/h0h_h�QUtilizing the different steps presented previously, one gets the following script�h1]�h@�QUtilizing the different steps presented previously, one gets the following script�����}�(hj�  h#j�  ubaubh�)��}�(h}�(h]�h�h�h]�h]�h]�h]�uh"Nh#j�  h.Nhhh0h�hX@  >>> from Backends.Contract import Contract
>>> from datetime import date
>>> contract_maturity = date(2017,12,1)
>>> index_name = "WTI"
>>> contract = Contract(index_name, contract_maturity)
>>> start_value_date = date(2017, 1, 1)
>>> end_value_date = date(2017,1,4)
>>> import numpy as np
>>> moneynesses = np.array([0.05, 0.25, 0.5, 0.75, 0.95])
>>> from Configurations import Configurations
>>> pricing_parameters = Configurations.Pricing.DEFAULT_PRICING_PARAMETERS
>>> database_config = Configurations.DataBase.DEFAULT_DB_CONFIG
>>> from Smile.Implicitation.MoneynessSmileRepresentationBuilder import MoneynessSmileRepresentationBuilder
>>> smile_builder = MoneynessSmileRepresentationBuilder(database_config, pricing_parameters, moneynesses)
>>> smiles = smile_builder.build_for_dates(start_value_date,end_value_date, contract)�h1]�h@X@  >>> from Backends.Contract import Contract
>>> from datetime import date
>>> contract_maturity = date(2017,12,1)
>>> index_name = "WTI"
>>> contract = Contract(index_name, contract_maturity)
>>> start_value_date = date(2017, 1, 1)
>>> end_value_date = date(2017,1,4)
>>> import numpy as np
>>> moneynesses = np.array([0.05, 0.25, 0.5, 0.75, 0.95])
>>> from Configurations import Configurations
>>> pricing_parameters = Configurations.Pricing.DEFAULT_PRICING_PARAMETERS
>>> database_config = Configurations.DataBase.DEFAULT_DB_CONFIG
>>> from Smile.Implicitation.MoneynessSmileRepresentationBuilder import MoneynessSmileRepresentationBuilder
>>> smile_builder = MoneynessSmileRepresentationBuilder(database_config, pricing_parameters, moneynesses)
>>> smiles = smile_builder.build_for_dates(start_value_date,end_value_date, contract)�����}�(hhh#j�  ubaubh`)��}�(h}�(h]�h]�h]�h]�h]�uh"Kvh#j�  hhh.h/h0h_h��The object smiles is a dictionary mapping each value date into a volatility smile corresponding to that value date.
In order to display the results, move on to the next section.�h1]�h@��The object smiles is a dictionary mapping each value date into a volatility smile corresponding to that value date.
In order to display the results, move on to the next section.�����}�(hj�  h#j�  ubaubeubh)��}�(h}�(h]�h]��formatting-results�ah]�h]�h]��formatting results�auh"Kzh#h$h.h/hhh0hhhh1]�(h4)��}�(h}�(h]�h]�h]�h]�h]�uh"Kzh#j�  h.h/hhh0h3h�Formatting Results�h1]�h@�Formatting Results�����}�(hj  h#j   ubaubh`)��}�(h}�(h]�h]�h]�h]�h]�uh"K|h#j�  hhh.h/h0h_h��In order to format smile results, a str method was implemented for the class :class:`~Smile.Representations.MoneynessSmileRepresentation`.
It is therefore sufficient to print the smile for each value date using these few lines�h1]�(h@�MIn order to format smile results, a str method was implemented for the class �����}�(h�MIn order to format smile results, a str method was implemented for the class �h#j  ubh�)��}�(h}�(h�Nh��h��2Smile.Representations.MoneynessSmileRepresentation��	refdomain��py�h]�h]��refexplicit��h]�h]��reftype��class�h�Nh�h�h]�uh"K|h#j  h.h/h0h�h�<:class:`~Smile.Representations.MoneynessSmileRepresentation`�h1]�h�)��}�(h}�(h]�(h�j"  �py-class�eh]�h]�h]�h]�uh#j  h0h�hj+  h1]�h@�MoneynessSmileRepresentation�����}�(hhh#j-  ubaubaubh@�Y.
It is therefore sufficient to print the smile for each value date using these few lines�����}�(h�Y.
It is therefore sufficient to print the smile for each value date using these few lines�h#j  ubeubh�)��}�(h}�(h]�h�h�h]�h]�h]�h]�uh"Nh#j�  h.Nhhh0h�hX�  >>> from Backends.Contract import Contract
>>> from datetime import date
>>> contract_maturity = date(2017,12,1)
>>> index_name = "WTI"
>>> contract = Contract(index_name, contract_maturity)
>>> start_value_date = date(2017, 1, 1)
>>> end_value_date = date(2017,1,4)
>>> import numpy as np
>>> moneynesses = np.array([0.05, 0.25, 0.5, 0.75, 0.95])
>>> from Configurations import Configurations
>>> pricing_parameters = Configurations.Pricing.DEFAULT_PRICING_PARAMETERS
>>> database_config = Configurations.DataBase.DEFAULT_DB_CONFIG
>>> from Smile.Implicitation.MoneynessSmileRepresentationBuilder import MoneynessSmileRepresentationBuilder
>>> smile_builder = MoneynessSmileRepresentationBuilder(database_config, pricing_parameters, moneynesses)
>>> smiles = smile_builder.build_for_dates(start_value_date, end_value_date, contract)
>>> print(smiles[end_value_date])
Moneyness |0.050|0.250|0.500|0.750|0.950|
Vols      |0.382|0.330|0.279|0.244|0.252|
Skew      |0.370|0.185|0.000|-0.123|-0.096|
<BLANKLINE>�h1]�h@X�  >>> from Backends.Contract import Contract
>>> from datetime import date
>>> contract_maturity = date(2017,12,1)
>>> index_name = "WTI"
>>> contract = Contract(index_name, contract_maturity)
>>> start_value_date = date(2017, 1, 1)
>>> end_value_date = date(2017,1,4)
>>> import numpy as np
>>> moneynesses = np.array([0.05, 0.25, 0.5, 0.75, 0.95])
>>> from Configurations import Configurations
>>> pricing_parameters = Configurations.Pricing.DEFAULT_PRICING_PARAMETERS
>>> database_config = Configurations.DataBase.DEFAULT_DB_CONFIG
>>> from Smile.Implicitation.MoneynessSmileRepresentationBuilder import MoneynessSmileRepresentationBuilder
>>> smile_builder = MoneynessSmileRepresentationBuilder(database_config, pricing_parameters, moneynesses)
>>> smiles = smile_builder.build_for_dates(start_value_date, end_value_date, contract)
>>> print(smiles[end_value_date])
Moneyness |0.050|0.250|0.500|0.750|0.950|
Vols      |0.382|0.330|0.279|0.244|0.252|
Skew      |0.370|0.185|0.000|-0.123|-0.096|
<BLANKLINE>�����}�(hhh#j@  ubaubeubeubh.h/hhh0hhhh1]�(h4)��}�(h}�(h]�h]�h]�h]�h]�uh"KIh#hh.h/hhh0h3h�Creating the user configuration�h1]�h@�Creating the user configuration�����}�(hjW  h#jO  ubaubh`)��}�(h}�(h]�h]�h]�h]�h]�uh"KKh#hhhh.h/h0h_hX�  Since the implied volatility is obtained through numerical solving, some parameters need to be passed to the pricer such
the numerical parameters of the pricing algorithm. Moreover, in order to retrieve the option quotations, we need to specify
the configuration relative to the database connexion. The classes relevant to user configuration are
located in the module Configurations.Configurations. First, we will import the needed classes from there:�h1]�h@X�  Since the implied volatility is obtained through numerical solving, some parameters need to be passed to the pricer such
the numerical parameters of the pricing algorithm. Moreover, in order to retrieve the option quotations, we need to specify
the configuration relative to the database connexion. The classes relevant to user configuration are
located in the module Configurations.Configurations. First, we will import the needed classes from there:�����}�(hje  h#j]  ubaubh�)��}�(h}�(h]�h�h�h]�h]�h]�h]�uh"Nh#hh.Nhhh0h�h�->>> from Configurations import Configurations�h1]�h@�->>> from Configurations import Configurations�����}�(hhh#jk  ubaubh`)��}�(h}�(h]�h]�h]�h]�h]�uh"KRh#hhhh.h/h0h_h�zFor ease of use, default parameters are defined in the Configurations module, which we can call using these lines of code:�h1]�h@�zFor ease of use, default parameters are defined in the Configurations module, which we can call using these lines of code:�����}�(hj�  h#jy  ubaubh�)��}�(h}�(h]�h�h�h]�h]�h]�h]�uh"Nh#hh.Nhhh0h�h��>>> from Configurations import Configurations
>>> pricing_parameters = Configurations.Pricing.DEFAULT_PRICING_PARAMETERS
>>> database_config = Configurations.DataBase.DEFAULT_DB_CONFIG�h1]�h@��>>> from Configurations import Configurations
>>> pricing_parameters = Configurations.Pricing.DEFAULT_PRICING_PARAMETERS
>>> database_config = Configurations.DataBase.DEFAULT_DB_CONFIG�����}�(hhh#j�  ubaubh`)��}�(h}�(h]�h]�h]�h]�h]�uh"KXh#hhhh.h/h0h_hX{  The object Configurations.Pricing.DEFAULT_PRICING_PARAMETERS contains the numerical paramters to be passed to the pricer.
Those parameters determine the accuracy and the performance of the pricing algorithm. The object Configurations.Greeks.DEFAULT_DB_CONFIG
contains the database connexion parameters such as the user name, the port on which the connexion should be done, etc...�h1]�h@X{  The object Configurations.Pricing.DEFAULT_PRICING_PARAMETERS contains the numerical paramters to be passed to the pricer.
Those parameters determine the accuracy and the performance of the pricing algorithm. The object Configurations.Greeks.DEFAULT_DB_CONFIG
contains the database connexion parameters such as the user name, the port on which the connexion should be done, etc...�����}�(hj�  h#j�  ubaubeubh)h$jJ  jE  j�  j�  hthoj  j  j�  j�  u�autofootnote_refs�]��	footnotes�]��	nametypes�}�(h-Nj  Nh!Nj�  NjN  Nj�  NhxNu�symbol_footnote_refs�]��current_line�N�	citations�]��citation_refs�}��footnote_refs�}��settings��docutils.frontend��Values���)��}�(�syntax_highlight��long��
source_url�N�_config_files�]��_source�h/�error_encoding��cp437��
halt_level�K�dump_settings�N�expose_internals�N�strip_classes�N�	tab_width�K�pep_base_url�� https://www.python.org/dev/peps/��dump_internals�N�source_link�N�pep_file_url_template��pep-%04d��	datestamp�N�trim_footnote_reference_space���file_insertion_enabled���gettext_compact���raw_enabled�K�cloak_email_addresses���doctitle_xform���output_encoding_error_handler��strict��pep_references�Nh3N�exit_status_level�K�output_encoding��utf-8��config�N�sectsubtitle_xform���env�N�error_encoding_error_handler��backslashreplace��strict_visitor�N�_disable_config�N�debug�N�strip_comments�N�strip_elements_with_classes�N�sectnum_xform�K�dump_transforms�N�dump_pseudo_xml�N�input_encoding_error_handler�j�  �_destination�N�	generator�N�smart_quotes���language_code��en��embed_stylesheet���footnote_backlinks�K�input_encoding��	utf-8-sig��rfc_base_url��https://tools.ietf.org/html/��report_level�K�toc_backlinks��entry��rfc_references�N�	traceback���record_dependencies�N�warning_stream�N�docinfo_xform�K�auto_id_prefix��id��	id_prefix�hub�autofootnotes�]��refids�}��substitution_defs�}��symbol_footnote_start�K �transformer�N�reporter�N�id_start�Kh}�(h]��source�h/h]�h]�h]�h]�u�symbol_footnotes�]��current_source�N�parse_messages�]��autofootnote_start�Kh0h�transform_messages�]��nameids�}�(h-h)j  j  h!hj�  j�  jN  jJ  j�  j�  hxhtuh1]�h$aub.