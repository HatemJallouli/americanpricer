Pricing.PV.AmericanPricer
=========================

.. automodule:: Pricing.PV.AmericanPricer

   
   
   .. rubric:: Functions

   .. autosummary::
   
      Chebychev_Nodes
      Gauss_Legendre
      Tau_From_Z
      X_function
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      AmericanPricer
      Black_Schole_Formulae_Provider
      Chebychev_Interpolator
      Chebychev_Interpolator_Factory
      First_Guess_Finder
      IterationType
   
   

   
   
   