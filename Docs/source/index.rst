.. American Pricer documentation master file, created by
   sphinx-quickstart on Sat Apr 29 15:24:34 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to American Pricer's documentation!
===========================================


.. raw:: html

        <!-- Block section -->

          <!-- row -->
            <div class="row-fluid">
                <div class="span4 box">
                    <h2><a href="tutorials/index.html">Tutorials</a></h2>
                    <blockquote>Useful tutorials for developing a feel
                    for some of the library's key classes and functions.
                    </blockquote>
                </div>
                <div class="span4 box">
                    <h2><a href="modules/index.html">API</a></h2>
                            <blockquote>The exact API of all functions and classes, as given by the docstrings.
                            The API documents expected types and allowed features for all functions in the library.
                            </blockquote>
                </div>
            </div>


.. toctree::
    :maxdepth: 2
    :hidden:

    tutorials/index
    modules/index


