========================
Volatility Implicitation
========================

.. topic:: Section contents

    In this section, we will introduce the different steps in order to get a volatility smile from the market
    quotations of american options.

Specifying the underlying
=========================

First, one needs to specify the futures contract on which the option is written. This is defined by an underlying name
such as GOLD or WTI and a futures maturity.

The classes relevant to contract creation are located in the module Backends. We will first import the needed
classes from there. We will also need to import :class:`~datetime.date` in order to specify the maturity.

 >>> from Backends.Contract import Contract
 >>> from datetime import date

Then we specify the different elements of the contract

 >>> from Backends.Contract import Contract
 >>> from datetime import date
 >>> contract_maturity = date(2017,12,1)
 >>> index_name = "WTI"
 >>> contract = Contract(index_name, contract_maturity)

Specifying quotation dates
==========================

Once we define the contract, we need to define the different dates for which the smile needs to be calculated. The only
quantities needed for that are a start date and an end date that will be included in the time interval. All the dates in
between and for which option quotations are available in the database will be effectively taken into account.

The definition of these dates is simply done by adding the two lines

 >>> from Backends.Contract import Contract
 >>> from datetime import date
 >>> contract_maturity = date(2017,12,1)
 >>> index_name = "WTI"
 >>> contract = Contract(index_name, contract_maturity)
 >>> start_value_date = date(2017, 1, 1)
 >>> end_value_date = date(2017,1,4)


Choosing the smile's granualarity
=================================

For a given contract, many options with many strikes are quoted in the market. The smile is parametrized using a sub set
set of these options in order to have a reduced number of parameters. The user can choose to keep 5 or 7 strikes that
would be sufficient to reproduce the entire strike.

To specify the strikes for which the volatility will be computed, one should specify the different moneynesses represented in
deltas. This is illustrated in the added line below:

 >>> from Backends.Contract import Contract
 >>> from datetime import date
 >>> contract_maturity = date(2017,12,1)
 >>> index_name = "WTI"
 >>> contract = Contract(index_name, contract_maturity)
 >>> start_value_date = date(2017, 1, 1)
 >>> end_value_date = date(2017,1,4)
 >>> import numpy as np
 >>> moneynesses = np.array([0.05, 0.25, 0.5, 0.75, 0.95])

For example, the moneyness 0.5 corresponds to that At-The-Money strike (equal to the underlying price), whereas the
moneyness 0.95 corresponds to a strike lower that the underlying level and 0.05 correponds to a strike higher than the
underlying level.

Creating the user configuration
===============================

Since the implied volatility is obtained through numerical solving, some parameters need to be passed to the pricer such
the numerical parameters of the pricing algorithm. Moreover, in order to retrieve the option quotations, we need to specify
the configuration relative to the database connexion. The classes relevant to user configuration are
located in the module Configurations.Configurations. First, we will import the needed classes from there:

 >>> from Configurations import Configurations

For ease of use, default parameters are defined in the Configurations module, which we can call using these lines of code:

 >>> from Configurations import Configurations
 >>> pricing_parameters = Configurations.Pricing.DEFAULT_PRICING_PARAMETERS
 >>> database_config = Configurations.DataBase.DEFAULT_DB_CONFIG

The object Configurations.Pricing.DEFAULT_PRICING_PARAMETERS contains the numerical paramters to be passed to the pricer.
Those parameters determine the accuracy and the performance of the pricing algorithm. The object Configurations.Greeks.DEFAULT_DB_CONFIG
contains the database connexion parameters such as the user name, the port on which the connexion should be done, etc...

Building the volatility smile
=============================

In order to build the smile, the four previously presented elements (underlying, dates, moneynesses and configuration)
should be passed to a builder situated in the module Smile.Implicitation. To import it, one should add the following lines

 >>> from Smile.Implicitation.MoneynessSmileRepresentationBuilder import MoneynessSmileRepresentationBuilder

Utilizing the different steps presented previously, one gets the following script

 >>> from Backends.Contract import Contract
 >>> from datetime import date
 >>> contract_maturity = date(2017,12,1)
 >>> index_name = "WTI"
 >>> contract = Contract(index_name, contract_maturity)
 >>> start_value_date = date(2017, 1, 1)
 >>> end_value_date = date(2017,1,4)
 >>> import numpy as np
 >>> moneynesses = np.array([0.05, 0.25, 0.5, 0.75, 0.95])
 >>> from Configurations import Configurations
 >>> pricing_parameters = Configurations.Pricing.DEFAULT_PRICING_PARAMETERS
 >>> database_config = Configurations.DataBase.DEFAULT_DB_CONFIG
 >>> from Smile.Implicitation.MoneynessSmileRepresentationBuilder import MoneynessSmileRepresentationBuilder
 >>> smile_builder = MoneynessSmileRepresentationBuilder(database_config, pricing_parameters, moneynesses)
 >>> smiles = smile_builder.build_for_dates(start_value_date,end_value_date, contract)

The object smiles is a dictionary mapping each value date into a volatility smile corresponding to that value date.
In order to display the results, move on to the next section.

Formatting Results
==================

In order to format smile results, a str method was implemented for the class :class:`~Smile.Representations.MoneynessSmileRepresentation`.
It is therefore sufficient to print the smile for each value date using these few lines

 >>> from Backends.Contract import Contract
 >>> from datetime import date
 >>> contract_maturity = date(2017,12,1)
 >>> index_name = "WTI"
 >>> contract = Contract(index_name, contract_maturity)
 >>> start_value_date = date(2017, 1, 1)
 >>> end_value_date = date(2017,1,4)
 >>> import numpy as np
 >>> moneynesses = np.array([0.05, 0.25, 0.5, 0.75, 0.95])
 >>> from Configurations import Configurations
 >>> pricing_parameters = Configurations.Pricing.DEFAULT_PRICING_PARAMETERS
 >>> database_config = Configurations.DataBase.DEFAULT_DB_CONFIG
 >>> from Smile.Implicitation.MoneynessSmileRepresentationBuilder import MoneynessSmileRepresentationBuilder
 >>> smile_builder = MoneynessSmileRepresentationBuilder(database_config, pricing_parameters, moneynesses)
 >>> smiles = smile_builder.build_for_dates(start_value_date, end_value_date, contract)
 >>> print(smiles[end_value_date])
 Moneyness |0.050|0.250|0.500|0.750|0.950|
 Vols      |0.382|0.330|0.279|0.244|0.252|
 Skew      |0.370|0.185|0.000|-0.123|-0.096|
 <BLANKLINE>
