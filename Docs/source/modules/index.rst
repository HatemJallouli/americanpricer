=============
API Reference
=============

This is the class and function reference of American Pricer.


Backends
========

This module contains classes that interact with databases (data retrieval and transport format)

.. autosummary::
   :toctree: generated

   Backends.Contract
   Backends.ContractMaturity
   Backends.DatabaseConfig
   Backends.MarketDataRetriever
   Backends.OptionId
   Backends.OptionQuotationsRetriever
   Backends.RawDiscountRateCurveRetriever

Calendars
=========

This module contains classes that handle calendar conventions and dates conversions.

.. autosummary::
   :toctree: generated

   Calendars.CalendarTimeMeasure
   Calendars.ContractMonthCodes
   Calendars.ITimeMeasure

Common
======

This module contains common classes used by the different other modules.

.. autosummary::
   :toctree: generated

   Common.MarketDataKey
   Common.MarketStateKey
   Common.MarketStateKeys
   Common.PricingResult
   Common.ResultKey
   Common.ResultKeys

Greeks
======

This module contains classes that are used for Greeks computation.

Common
------

This module contains classes that are common to all the sub-modules under Greeks module.

.. autosummary::
   :toctree: generated

   Greeks.Common.GreekKey
   Greeks.Common.GreeksDefinition
   Greeks.Common.ShockUtilities

Formatters
----------

This module contains classes that are useful for formatting and printing results.

.. autosummary::
   :toctree: generated

   Greeks.Formatters.ResultsFormatter

GreekFillers
------------

This module contains classes that are used to build Greeks metrics.

.. autosummary::
   :toctree: generated

   Greeks.GreekFillers.IGreekFiller
   Greeks.GreekFillers.DeltaGreekFiller
   Greeks.GreekFillers.GammaGreekFiller
   Greeks.GreekFillers.RhoGreekFiller
   Greeks.GreekFillers.SmileRiskGreekFiller
   Greeks.GreekFillers.ThetaGreekFiller
   Greeks.GreekFillers.VannaGreekFiller
   Greeks.GreekFillers.VegaGreekFiller
   Greeks.GreekFillers.VolgaGreekFiller
   Greeks.GreekFillers.GreekFillersFactory

Runners
-------

This module contains classes that are used to run Greeks computations.

.. autosummary::
   :toctree: generated

   Greeks.Runners.GreeksRunner

StateFillers
------------

This module contains classes that are used to build market states.

.. autosummary::
   :toctree: generated

   Greeks.StateFillers.IMarketStateFiller
   Greeks.StateFillers.PriceMarketStateFiller
   Greeks.StateFillers.DeltaMarketStateFiller
   Greeks.StateFillers.GammaMarketStateFiller
   Greeks.StateFillers.RhoMarketStateFiller
   Greeks.StateFillers.SmileRiskMarketStateFiller
   Greeks.StateFillers.ThetaMarketStateFiller
   Greeks.StateFillers.VannaMarketStateFiller
   Greeks.StateFillers.VegaMarketStateFiller
   Greeks.StateFillers.VolgaMarketStateFiller
   Greeks.StateFillers.MarketStateFillersFactory

MarketQuotations
================

This module contains classes that are used to describe the market data.

.. autosummary::
   :toctree: generated

   MarketQuotations.Market
   MarketQuotations.MarketData


Parsing
=======

This module contains classes that are useful for parsing strings into python objects.

.. autosummary::
   :toctree: generated

   Parsing.ContractIdParser
   Parsing.OptionIdParser

Pricing
=======

This module contains classes that are used to compute instruments prices.

Common
------

This module contains common classes used by the different other modules.

.. autosummary::
   :toctree: generated

   Pricing.Common.PriceKey

PriceFillers
------------

This module contains classes that are used to build the price.

.. autosummary::
   :toctree: generated

   Pricing.PriceFillers.IPriceFiller
   Pricing.PriceFillers.PriceFiller

PV
--

This module contains the low-level classes and algorithms to compute the price of instruments.

.. autosummary::
   :toctree: generated

   Pricing.PV.IAmericanOptionPricer
   Pricing.PV.AmericanOptionPricer
   Pricing.PV.PricingParameters

Runners
-------

This module contains classes that are used to run price computations.

.. autosummary::
   :toctree: generated

   Pricing.Runners.IPvRunner
   Pricing.Runners.ParallelPvRunner
   Pricing.Runners.SequentialPvRunner


Products
========

This module contains classes to describe financial instruments.

.. autosummary::
   :toctree: generated

   Products.AmericanOption
   Products.OptionType

Rates
=====

This module contains classes to describe the rate curve.

.. autosummary::
   :toctree: generated

   Rates.ConstantDiscountRateCurveFactory
   Rates.DiscountRateCurveInterpolationFactory
   Rates.IDiscountRateCurveInterpolator
   Rates.LogCubicSplineDiscountRateCurveInterpolator
   Rates.RateCurveId
   Rates.RawDiscountRateCurve

Smile
=====

This module contains classes to describe the volatility smile.

Dynamics
--------

This module contains classes to model the dynamics of the volatility smile.

.. autosummary::
   :toctree: generated

   Smile.Dynamics.IVolatilityDynamicsModel
   Smile.Dynamics.LogLinearVolatilityDynamicsModel

Implicitation
-------------

This module contains classes to imply the volatility smile from market quotations.

.. autosummary::
   :toctree: generated

   Smile.Implicitation.AmericanImpliedVolatilitySolver
   Smile.Implicitation.BlackScholesMoneynessToStrikeSolver
   Smile.Implicitation.IImpliedVolatilitySolver
   Smile.Implicitation.IMoneynessSmileRepresentationBuilder
   Smile.Implicitation.IMoneynessToStrikeSolver
   Smile.Implicitation.MoneynessSmileRepresentationBuilder
   Smile.Implicitation.OptionQuotation

Interpolation
-------------

This module contains classes to interpolate the volatility smile.

.. autosummary::
   :toctree: generated

   Smile.Interpolation.CubicSplineImpliedVolatilityInterpolator
   Smile.Interpolation.CubicSplineLinearExtrapol
   Smile.Interpolation.IImpliedVolatilityInterpolator
   Smile.Interpolation.TensionSplineImpliedVolatilityInterpolator

Representation
--------------

This module contains classes to parametrize the volatility smile.

.. autosummary::
   :toctree: generated

   Smile.Representations.MoneynessSmileRepresentation
   Smile.Representations.SmileRepresentationConverter
   Smile.Representations.StrikeSmileRepresentation