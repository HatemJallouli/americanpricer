import re

from Backends.OptionId import OptionId

from Products.OptionType import OptionType


class OptionIdParser:
    """ Class to convert a futures option format into an object of type :class:`~Backends.OptionId`.

    """
    def __init__(self):
        self.__regexp = re.compile("^([0-9]+)((?i)[pc])$")
        self.__year_reference = 2000

    def parse(self, option_id_str : str) -> OptionId:
        """ Convert a futures option format into an object of type :class:`~Backends.OptionId`.

            Parameters
            ----------
            option_id_str : str
                Futures option description in the format strike + option type

            Returns
            -------
            option_id : :class:`~Backends.OptionId`
                The representation of an option in a :class:`~Backends.OptionId` format

            Examples
            --------
            >>> from Parsing.OptionIdParser import OptionIdParser
            >>> option_id_parser = OptionIdParser()
            >>> option_format = "40p"
            >>> option_id = option_id_parser.parse(option_format)
            >>> print(option_id)
            Option Id: 40.0-OptionType.PUT

        """
        match = self.__regexp.match(option_id_str)
        strike_str = match.groups()[0]
        option_type_str = match.groups()[1]
        option_id_str_upper = str.upper(option_type_str)
        strike_float = float(strike_str)

        if option_id_str_upper == "P":
            option_type = OptionType.PUT

        if option_id_str_upper == "C":
            option_type = OptionType.CALL

        return OptionId(strike_float, option_type)

if __name__ == "__main__":
    import doctest
    doctest.testmod()