import re

from Backends.ContractMaturity import ContractMaturity

from Calendars.ContractMonthCodes import ContractMonthCodes


class ContractIdParser:
    """ Class to convert a futures contract format into an object of type :class:`~Backends.ContractMaturity`.

    """
    def __init__(self):
        self.__regexp = re.compile("^([a-zA-z])([0-9]?[0-9])$")
        self.__year_reference = 2000

    def parse(self, contract_id_str : str) -> ContractMaturity:
        """ Convert a futures contract format into an object of type :class:`~Backends.ContractMaturity`.
            
            Parameters
            ----------
            contract_id_str : str
                Futures contract description in the format contract month code + year
            
            Returns
            -------
            contract_maturity : :class:`~Backends.ContractMaturity`
                The representation of the futures maturity in a :class:`~Backends.ContractMaturity` format
            
            Examples
            --------
            >>> from Parsing.ContractIdParser import ContractIdParser
            >>> contract_id_parser = ContractIdParser()
            >>> futures_format = "m17"
            >>> contract_maturity = contract_id_parser.parse(futures_format)
            >>> print(contract_maturity)
            Contract Maturity: Jun-17
            
        """

        match = self.__regexp.match(contract_id_str)
        contract_month_code_str = match.groups()[0]
        contract_year_str = match.groups()[1]
        contract_month_code_str_upper = str.upper(contract_month_code_str)
        contract_month = ContractMonthCodes[contract_month_code_str_upper].value
        contract_year = int(contract_year_str) + self.__year_reference
        return ContractMaturity(contract_month, contract_year)

if __name__ == "__main__":
    import doctest
    doctest.testmod()
