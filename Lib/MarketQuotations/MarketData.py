from typing import List

from Rates.RawDiscountRateCurve import RawDiscountRateCurve


class MarketData:
    """ Container class to encapsulate market data.

        Parameters
        ----------
        underlying_level : float
            The quotation of the option underlying value.
        atm_vol : float
            The quotation of the At-The-Money volatility of an option.
        moneyness : List[float]
            A list specifying the deltas used to represent the smile.
        skew_values : List[float]
            A list where the ith element represents the relative difference between the volatility of the option having
            a moneyness corresponding to moneyness[i] and the atm volatility.
        raw_discount_rate_curve : :class:`~Rates.RawDiscountRateCurve`
            A raw representation (without interpolation) of the discount curve term structure used to discount future flows.
        
        Examples
        --------
        >>> from MarketQuotations.MarketData import MarketData
        >>> from Rates.RawDiscountRateCurve import RawDiscountRateCurve
        >>> import numpy as np
        >>> underlying_level = 100.0
        >>> atm_vol = 0.3
        >>> moneyness = np.array([0.05,0.25,0.5,0.75,0.95])
        >>> skew_values = np.array([0.238, 0.117, 0, -0.110, -0.110])
        >>> rate_durations = np.array([0.0,1.0,2.0,3.0])
        >>> discounts = np.array([1.0,0.98,0.83,0.75])
        >>> raw_discount_rate_curve = RawDiscountRateCurve("USD-LIBOR", rate_durations, discounts)
        >>> market_data = MarketData(underlying_level, atm_vol, moneyness, skew_values, raw_discount_rate_curve)
        >>> print(market_data.underlying_level)
        100.0
        >>> print(market_data.atm_vol)
        0.3

    """
    def __init__(self,
                 underlying_level : float,
                 atm_vol : float,
                 moneyness : List[float],
                 skew_values : List[float],
                 raw_discount_rate_curve : RawDiscountRateCurve):

        self.__underlying_level = underlying_level
        self.__atm_vol = atm_vol
        self.__moneyness = moneyness
        self.__skew_values = skew_values
        self.__raw_discount_rate_curve = raw_discount_rate_curve


    @property
    def underlying_level(self):
        return self.__underlying_level

    @property
    def atm_vol(self):
        return self.__atm_vol


    @property
    def moneyness(self):
        return self.__moneyness

    @property
    def skew_values(self):
        return self.__skew_values

    @property
    def raw_discount_rate_curve(self):
        return self.__raw_discount_rate_curve

if __name__ == "__main__":
    import doctest
    doctest.testmod()