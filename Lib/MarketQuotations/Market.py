from datetime import date

from MarketQuotations.MarketData import MarketData


class Market :
    """ Container class to encapsulate dated market data.

        Parameters
        ----------
        ref_date : date
            Date of the quotations of the market data in the market.
        market_data : :class:`~MarketQuotations.MarketData`
            The quotations of the market data.

    """

    def __init__(self, ref_date : date, market_data : MarketData):
        self.__ref_date = ref_date
        self.__market_data = market_data

    @property
    def ref_date(self):
        return self.__ref_date

    @property
    def market_data(self):
        return self.__market_data