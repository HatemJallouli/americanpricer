import collections
from datetime import date
from typing import List, Dict

import numpy as np
from Backends.Contract import Contract
from Backends.DatabaseConfig import DatabaseConfig
from Backends.RawDiscountRateCurveRetriever import RawDiscountRateCurveRetriever
from Calendars.CalendarTimeMeasure import CalendarTimeMeasure
from Pricing.PV.PricingParameters import PricingParameters
from Products.OptionType import OptionType
from Rates.DiscountRateCurveInterpolationFactory import DiscountRateCurveInterpolationFactory
from Smile.Implicitation.AmericanImpliedVolatilitySolver import AmericanImpliedVolatilitySolver
from Smile.Implicitation.BlackScholesMoneynessToStrikeSolver import BlackScholesMoneynessToStrikeSolver
from Smile.Implicitation.IMoneynessSmileRepresentationBuilder import IMoneynessSmileRepresentationBuilder
from Smile.Representations.MoneynessSmileRepresentation import MoneynessSmileRepresentation
from scipy.interpolate import CubicSpline

from Backends.OptionQuotationsRetriever import OptionQuotationsRetriever


class MoneynessSmileRepresentationBuilder(IMoneynessSmileRepresentationBuilder):
    """ Class to build the volatility surface associated to american options of a given contract.

        Parameters
        ----------
        db_config : :class:`~Backends.DatabaseConfig`
            The configuration for the database connexion.
        pricing_parameters : :class:`~Pricing.PV.PricingParameters`
            The numerical parameters for the american option pricer.
        moneynesses : List[float]
            The list of deltas used to represent the smile.

    """
    def __init__(self, db_config : DatabaseConfig, pricing_parameters : PricingParameters, moneynesses : List[float], dump_additional_data = False, log = False):
        self.__db_config = db_config
        self.__pricing_parameters = pricing_parameters
        self.__moneynesses = moneynesses
        self.__dump_additional_data = dump_additional_data
        self.__log = log

    def build_for_date(self, value_date : date, contract : Contract) -> MoneynessSmileRepresentation:
        """ Build the volatility surface at a given date, for a given contract.

            Parameters
            ----------
            value_date : date
                The quotation date of the options.
            contract : :class:`~Backends.Contract`
                The contract for which the volatility will be built.

            Returns
            -------
            moneyness_smile_representation : :class:`~Smile.Representations.MoneynessSmileRepresentation`
                The implied volatility smile represented as a function of the deltas.
        """
        return self.build_for_dates(value_date,value_date,contract)[value_date]

    def build_for_dates(self, start_value_date : date, end_value_date, contract : Contract) -> Dict[date,MoneynessSmileRepresentation]:
        """ Build the volatility surface for a range of dates, for a given contract.

            Parameters
            ----------
            start_value_date : date
                The first quotation date of the options (included).
            end_value_date : date
                The last quotation date of the options (included).
            contract : :class:`~Backends.Contract`
                The contract for which the volatility will be built.

            Returns
            -------
            moneyness_smile_representation : Dict[date,:class:`~Smile.Representations.MoneynessSmileRepresentation`]
                Dictionary mapping each value date to the implied volatility smile represented as a function of the deltas.
        """
        db_config = self.__db_config
        pricing_parameters = self.__pricing_parameters
        option_quotations_retriever = OptionQuotationsRetriever(db_config)
        raw_discount_rate_curve_retriever = RawDiscountRateCurveRetriever(db_config)

        index_name = contract.index_name
        contract_date = contract.contract_date

        if (self.__log):
            print ("Retrieving quotations between dates {0} and {1} for {2}-{3}...".format(start_value_date, end_value_date, index_name, contract_date))

        quotations_by_date = option_quotations_retriever.retrieve_for_dates(start_value_date, end_value_date, contract)

        if (self.__log):
            print("Finished quotations retrieval...")
            print("")

        result = dict()

        for (value_date, tuple) in quotations_by_date.items():

            if (self.__log):
                print("Starting Implicitation for date {0}...".format(value_date))

            underlying_level, option_quotations = tuple
            time_measure = CalendarTimeMeasure(value_date)
            raw_discount_rate_curve = raw_discount_rate_curve_retriever.retrieve(value_date)
            discount_rate_curve_interpolator = DiscountRateCurveInterpolationFactory.log_cubic_interpolation(value_date, raw_discount_rate_curve)
            strikes = []
            prices = []
            vols = []

            vol_solver = AmericanImpliedVolatilitySolver(underlying_level,discount_rate_curve_interpolator, pricing_parameters)

            expiry_date = option_quotations[0].american_option.expiry_date

            for option_quotation in option_quotations:
                if (
                                option_quotation.american_option.option_type == OptionType.PUT and option_quotation.american_option.strike > underlying_level):
                    continue

                if (
                                option_quotation.american_option.option_type == OptionType.CALL and option_quotation.american_option.strike <= underlying_level):
                    continue

                if (option_quotation.american_option.expiry_date != expiry_date):
                    raise ValueError(
                        "Inconsistent expiries in option quotations {0} is not equal to {1}".format(expiry_date,
                                                                                                    option_quotation.american_option.expiry_date))

                strikes.append(option_quotation.american_option.strike)
                prices.append(option_quotation.quote_value)
                implied_vol = vol_solver.solve(option_quotation)
                vols.append(implied_vol)

            duration = time_measure.to_duration(expiry_date)

            strikes = np.array(strikes)
            vols = np.array(vols)

            cs = CubicSpline(strikes, vols, bc_type="natural")

            strike_solver = BlackScholesMoneynessToStrikeSolver(cs, underlying_level,
                                                                duration)

            moneynesses =self.__moneynesses

            strikes_for_moneynesses = np.zeros(len(moneynesses))

            for i in range(0, len(moneynesses)):
                strikes_for_moneynesses[i] = strike_solver.solve(moneynesses[-1 - i])

            atm_vol = cs(underlying_level)
            skew_values = (cs(strikes_for_moneynesses) / atm_vol - 1)


            moneyness_smile_representation = MoneynessSmileRepresentation(atm_vol, moneynesses, skew_values)

            if (self.__log):
                print("Smile:\n{0}".format(moneyness_smile_representation))

            if self.__dump_additional_data:

                result[value_date] = moneyness_smile_representation, strikes, vols, underlying_level, duration
            else:

                result[value_date] = moneyness_smile_representation

            if (self.__log):
                print("Finished Implicitation for date {0}".format(value_date))
                print("")

        return collections.OrderedDict(sorted(result.items()))