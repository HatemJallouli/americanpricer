from Smile.Implicitation.OptionQuotation import OptionQuotation


class IImpliedVolatilitySolver:
    """ General interface to compute the implied volatility associated to an option quotation.

        See Also
        --------
        AmericanImpliedVolatilitySolver: :class:`~Smile.Implicitation.AmericanImpliedVolatilitySolver`

    """
    def solve(self, option_quotation : OptionQuotation) -> float:
        """ Solve for the implied volatility given an option quotation.

            Parameters
            ----------
            option_quotation : :class:`~Smile.Implicitation.OptionQuotation`
                Characteristics and quotation of an option.

            Returns
            -------
            implied_volatility : float
                The implied volatility of the option
        """
        raise NotImplementedError("Method {0} is an abstract method of interface {1}".format(self.solve.__name__,
                                                                                             self.__class__.__name__))


