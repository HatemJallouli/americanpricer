from datetime import date

from Products.AmericanOption import AmericanOption


class OptionQuotation:
    """ Container class to represent the notion of an option quotation
        
        Parameters
        ----------
        quotation_date : date
            The date for which the quotation was observed.
        american_option : :class:`~Products.AmericanOption`
            The american option for which the quotation is provided.
        quote_value : float
            The quoted value of the option
        
        Examples
        --------
        >>> from Products.AmericanOption import AmericanOption
        >>> from Products.OptionType import OptionType
        >>> from datetime import date
        >>> from Smile.Implicitation.OptionQuotation import OptionQuotation
        >>> strike = 50.0
        >>> option_type = OptionType.CALL
        >>> expiry_date = date(2018,12,1)
        >>> value_date = date(2017,5,15)
        >>> american_option = AmericanOption(strike, option_type, expiry_date)
        >>> option_quotation = OptionQuotation(value_date, american_option, 1.35)
        >>> print(option_quotation)
        OptionType.CALL : Strike = 50.0, Maturity = 2018-12-01, Quote = 1.35 @ 2017-05-15
    
    """
    def __init__(self, quotation_date : date, american_option : AmericanOption, quote_value : float):
        self.__quotation_date = quotation_date
        self.__american_option = american_option
        self.__quote_value = quote_value

    @property
    def quotation_date(self):
        return self.__quotation_date

    @property
    def american_option(self):
        return self.__american_option

    @property
    def quote_value(self):
        return self.__quote_value

    def __str__(self):
        return "{0} : Strike = {1}, Maturity = {2}, Quote = {3} @ {4}".format(
            self.american_option.option_type,
            self.american_option.strike,
            self.american_option.expiry_date,
            self.quote_value,
            self.quotation_date)

if __name__ == "__main__":
    import doctest
    doctest.testmod()