import matplotlib.pyplot as plt
import numpy as np
from Smile.Representations.SmileRepresentationConverter import SmileRepresentationConverter
from scipy import optimize

from Smile.Implicitation.IMoneynessToStrikeSolver import IMoneynessToStrikeSolver


class BlackScholesMoneynessToStrikeSolver (IMoneynessToStrikeSolver):
    """ Class to compute the strike associated to a given moneyness in a Black-Scholes model.

        Parameters
        ----------
        vol_interpolation_in_strike : callable(float)
            An interpolation in strike of the volatility smile
        underlying_level : float
            The market value of the option underlying.
        duration : float
            The duration of the american option.
    """
    def __init__(self, vol_interpolation_in_strike : callable(float), underlying_level : float, duration : float):
        self.__vol_interpolation_in_strike = vol_interpolation_in_strike
        self.__underlying_level = underlying_level
        self.__duration = duration

    def solve(self, moneyness: float) -> float:
        """ Solve for the strike associated to a given moneyness.

            Parameters
            ----------
            moneyness : float
                The delta representation of the option's strike.

            Returns
            -------
            strike : float
                The absolute strike equivalent to the inputted moneyness
        """
        func = lambda k : SmileRepresentationConverter.strike_to_moneyness(self.__underlying_level, k, self.__vol_interpolation_in_strike(k), self.__duration) - moneyness
        first_guess = SmileRepresentationConverter.moneyness_to_strike(self.__underlying_level, moneyness, self.__vol_interpolation_in_strike(self.__underlying_level), self.__duration)
        try :
            strike = optimize.newton(func, first_guess)
        except:
            raise ValueError("Strike solving failed for moneyness: {0}, underlying_level: {1}, duration: {2}. First guess was : {3}".format(moneyness, self.__underlying_level, self.__duration, first_guess))
        return strike