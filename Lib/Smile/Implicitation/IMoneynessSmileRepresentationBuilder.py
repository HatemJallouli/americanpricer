from datetime import date
from typing import Dict

from Smile.Representations.MoneynessSmileRepresentation import MoneynessSmileRepresentation

from Backends.Contract import Contract


class IMoneynessSmileRepresentationBuilder:
    """ General interface to build the volatility surface associated to american options of a given contract.

        See Also
        --------
        MoneynessSmileRepresentationBuilder: :class:`~Smile.Implicitation.MoneynessSmileRepresentationBuilder`

    """
    def build_for_date(self, value_date: date, contract: Contract) -> MoneynessSmileRepresentation:
        """ Build the volatility surface at a given date, for a given contract.

            Parameters
            ----------
            value_date : date
                The quotation date of the options.
            contract : :class:`~Backends.Contract`
                The contract for which the volatility will be built.
            
            Returns
            -------
            moneyness_smile_representation : :class:`~Smile.Representations.MoneynessSmileRepresentation`
                The implied volatility smile represented as a function of the deltas.
        """
        raise NotImplementedError("Method {0} is an abstract method of interface {1}".format(self.build_for_date.__name__,
                                                                                             self.__class__.__name__))

    def build_for_dates(self, start_value_date: date, end_value_date: date, contract: Contract) -> Dict[date,MoneynessSmileRepresentation]:
        """ Build the volatility surface for a range of dates, for a given contract.

            Parameters
            ----------
            start_value_date : date
                The first quotation date of the options (included).
            end_value_date : date
                The last quotation date of the options (included).
            contract : :class:`~Backends.Contract`
                The contract for which the volatility will be built.

            Returns
            -------
            moneyness_smile_representation : Dict[date,:class:`~Smile.Representations.MoneynessSmileRepresentation`]
                Dictionary mapping each value date to the implied volatility smile represented as a function of the deltas.
        """
        raise NotImplementedError("Method {0} is an abstract method of interface {1}".format(self.build_for_dates.__name__,
                                                                                             self.__class__.__name__))