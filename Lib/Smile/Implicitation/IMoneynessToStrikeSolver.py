class IMoneynessToStrikeSolver:
    """ General interface to compute the strike associated to a given moneyness.

        See Also
        --------
        BlackScholesMoneynessToStrikeSolver: :class:`~Smile.Implicitation.BlackScholesMoneynessToStrikeSolver`

    """
    def solve(self, moneyness: float) -> float:
        """ Solve for the strike associated to a given moneyness.

            Parameters
            ----------
            moneyness : float
                The delta representation of the option's strike.

            Returns
            -------
            strike : float
                The absolute strike equivalent to the inputted moneyness
        """
        raise NotImplementedError("Method {0} is an abstract method of interface {1}".format(self.solve.__name__,
                                                                                             self.__class__.__name__))