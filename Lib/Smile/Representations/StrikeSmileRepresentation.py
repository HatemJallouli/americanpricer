from typing import List


class StrikeSmileRepresentation:

    """ Container class that describes a smile as a vector of strikes, the atm volatility, and the shifts in order to recover non-atm vols.
    We choose to represent the values as the At-The-Money volatility and shifts in order to do parallel smile deformation without copying
    the entire smile.
    
    Parameters
    ----------
    atm_vol : float
        At-The-Money volatility value
    strikes : List[float]
        Vector of strikes representing the abscissae of the smile.
    vol_shifts: List[float]
        Vector of relative shifts (relatively to the At-The-Money volatility) in order to describe the skew.
    """

    def __init__(self, atm_vol : float,  strikes : List[float], vol_shifts : List[float]):
        self.__atm_vol = atm_vol
        self.__strikes = strikes
        self.__vol_shifts = vol_shifts

    @property
    def volatilities(self):
        return (self.__vol_shifts + 1) * self.__atm_vol

    @property
    def atm_vol(self):
        return self.__atm_vol

    @property
    def strikes(self):
        return self.__strikes

    @property
    def vol_shifts(self):
        return self.__vol_shifts

    def __str__(self):
        format  = '{:.3f}'
        strikes_str = [format.format(x) for x in self.strikes]
        vol_str = [format.format(x) for x in self.volatilities]
        vol_shifts_str = [format.format(x) for x in self.vol_shifts]

        s = "|"
        strikes = "Strikes   " + s + s.join(strikes_str) + s + '\n'
        vol =       "Vols      " + s + s.join(vol_str) + s + '\n'
        vol_shifts ="Skew      " + s + s.join(vol_shifts_str) + s + '\n'
        return strikes + vol + vol_shifts