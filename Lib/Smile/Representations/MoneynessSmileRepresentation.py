from typing import List


class MoneynessSmileRepresentation:

    """" Container Class that describes a smile as a vector of deltas for moneyness, the atm volatility, and the shifts in
    order to recover non-atm vols. We choose to represent the values as the At-The-Money volatility and shifts in
    order to do parallel smile deformation without copying the entire smile.
    
    
    Parameters
    ----------
    atm_vol : float
        At-The-Money volatility value
    moneyness : List[float]
        Vector of deltas representing the abscissae of the smile.
    vol_shifts: List[float]
        Vector of relative shifts (relatively to the At-The-Money volatility) in order to describe the skew.
    
    """

    def __init__(self, atm_vol : float, moneyness : List[float], vol_shifts : List[float]):
        self.__atm_vol = atm_vol
        self.__moneyness = moneyness
        self.__vol_shifts = vol_shifts

    @property
    def volatilities(self):
        return (self.__vol_shifts + 1) * self.__atm_vol

    @property
    def atm_vol(self):
        return self.__atm_vol

    @property
    def moneyness(self):
        return self.__moneyness

    @property
    def vol_shifts(self):
        return self.__vol_shifts

    def __str__(self):
        format  = '{:.3f}'
        moneyness_str = [format.format(x) for x in self.moneyness]
        vol_str = [format.format(x) for x in self.volatilities]
        vol_shifts_str = [format.format(x) for x in self.vol_shifts]

        s = "|"
        moneyness = "Moneyness " + s + s.join(moneyness_str) + s + '\n'
        vol =       "Vols      " + s + s.join(vol_str) + s + '\n'
        vol_shifts ="Skew      " + s + s.join(vol_shifts_str) + s + '\n'
        return moneyness + vol + vol_shifts