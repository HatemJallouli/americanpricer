from Smile.Representations.MoneynessSmileRepresentation import MoneynessSmileRepresentation

from MarketQuotations.Market import Market


class IVolatilityDynamicsModel:
    """ General interface to describe how implied volatility evolves according to market evolution.
        
        See Also
        --------
        LogLinearVolatilityDynamicsModel: :class:`~Smile.Dynamics.LogLinearVolatilityDynamicsModel`
        
    """
    def smile(self, market : Market) -> MoneynessSmileRepresentation:
        """ Describe the moneyness smile representation for a given market state.

            Parameters
            ----------
            market : date
                Maturity of the linear rate.
            
            Returns
            -------
            moneyness_smile : :class:`~Smile.Representations.MoneynessSmileRepresentation`
                A representation of the smile parametrized by deltas
        """
        raise NotImplementedError("Method {0} is an abstract method of interface {1}".format(self.smile.__name__,
                                                                                             self.__class__.__name__))


