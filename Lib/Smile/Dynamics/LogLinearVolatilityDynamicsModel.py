import numpy as np
from MarketQuotations.Market import Market
from Smile.Representations.MoneynessSmileRepresentation import MoneynessSmileRepresentation

from Smile.Dynamics.IVolatilityDynamicsModel import IVolatilityDynamicsModel


class LogLinearVolatilityDynamicsModel(IVolatilityDynamicsModel):
    """ Class to describe the following relationship between the implied volatility dynamics.
        :math:`d\sigma = \kappa d\ln(F)` where
            * :math:`\sigma`: the At-The-Money volatility
            * :math:`\kappa` : the steepness parameter
            * :math:`F` : the underlying level

        Parameters
        ----------
        ref_underlying_level : float
            The reference underlying level for which the volatility smile is observed.
        steepness : float
            The parameter that links the underlying level dynamics to the volatility smile dynamics
    """
    def __init__(self, ref_underlying_level : float, steepness : float):
        self.__ref_underlying_level = ref_underlying_level
        self.__steepness = steepness

    def smile(self, market : Market) -> MoneynessSmileRepresentation:
        """ Describe the moneyness smile representation for a given market state.

            Parameters
            ----------
            market : date
                Maturity of the linear rate.

            Returns
            -------
            moneyness_smile : :class:`~Smile.Representations.MoneynessSmileRepresentation`
                A representation of the smile parametrized by deltas
        """
        ref_underlying_level = self.__ref_underlying_level
        steepness = self.__steepness
        ref_atm_vol = market.market_data.atm_vol
        new_underlying_level = market.market_data.underlying_level
        new_atm_vol = ref_atm_vol + steepness * np.log(new_underlying_level / ref_underlying_level)
        return MoneynessSmileRepresentation(new_atm_vol, market.market_data.moneyness, market.market_data.skew_values)