from typing import List

from scipy.interpolate import CubicSpline


class CubicSplineLinearExtrapol:
    """ Class that interpolates data between :math:`[x_{inf}, x_{sup}]` using a Cubic Spline with boundary condition being 0 second
    derivative outside the interval :math:`[x_{inf}, x_{sup}]` the extrapolation is linear

    Parameters
    ----------
    x : List[float]
        vector of abscissae. Should be sorted in the increasing order
    y : List[float]
        vector of ordinates.

    """

    def __init__(self, x : List[float], y : List[float]):
        cs = CubicSpline(x, y, bc_type="natural")
        self.__cs = cs
        self.__fst_abs = x[0]
        self.__last_abs = x[-1]
        self.__fst_ord = y[0]
        self.__last_ord = y[-1]
        self.__fst_der = cs(x[0], 1)
        self.__last_der = cs(x[-1], 1)

    def value_at(self, x):
        """ Compute the interpolation at abscissa x

            Parameters
            ----------
            x : float
                The abscissa of the point.
                
            Returns
            -------
            y : float
                The interpolated-extrapolated spline value at the inputted abscissa.

        """
        region_inf = (x < self.__fst_abs)
        region_sup = (x > self.__last_abs)
        region_middle = 1 - region_sup - region_inf
        value_region_inf = self.__fst_ord + self.__fst_der * (x - self.__fst_abs)
        value_region_sup = self.__last_ord + self.__last_der * (x - self.__last_abs)
        value_region_middle = self.__cs(x)
        return region_inf * value_region_inf + region_middle * value_region_middle + region_sup * value_region_sup

    def slope_at(self, x):
        """ Compute the slope of the interpolated curve at abscissa x

            Parameters
            ----------
            x : float
                The abscissa of the point.

            Returns
            -------
            y' : float
                The first derivative of the interpolated-extrapolated spline value at the inputted abscissa.

        """
        region_inf = (x < self.__fst_abs)
        region_sup = (x > self.__last_abs)
        region_middle = 1 - region_sup - region_inf
        value_region_inf = self.__fst_der
        value_region_sup = self.__last_der
        value_region_middle = self.__cs(x, 1)
        return region_inf * value_region_inf + region_middle * value_region_middle + region_sup * value_region_sup
