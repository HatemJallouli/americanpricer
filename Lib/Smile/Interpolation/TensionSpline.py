import math
import numpy as np
import scipy.optimize
import scipy.stats

class Spline:

    def __init__(self):
        pass


class TensionSpline(Spline):

    def __init__(self, calldeltavec, volvec, F, T, \
        wghtvec, sm, smtol):

        numdeltas = len(calldeltavec)
        if len(volvec) != numdeltas:
            raise ValueError('calldeltavec len ' + \
                str(numdeltas) + ' != volvec len ' + \
                str(len(volvec)))

        if np.max(calldeltavec) >= 1.0 or np.min(calldeltavec) <= 0.0:
            raise ValueError('call deltas must be between 0 and 1: ' + \
                str(calldeltavec))

        if np.any(calldeltavec != sorted(calldeltavec)):
            raise ValueError('calldeltavec not sorted: ' + \
                str(calldeltavec))

        if np.min(volvec) <= 0.0:
            raise ValueError('all vols must be > 0: ' + \
                str(volvec))

        if F <= 0:
            raise ValueError('F must be > 0: ' + str(F))

        if T <= 0:
            raise ValueError('T must be > 0: ' + str(T))

        if not type(volvec) is np.ndarray:
            self.__y = np.array(volvec)
        else:
            self.__y = volvec.copy()

        if not type(calldeltavec) is np.ndarray:
            calldeltavec = np.array(calldeltavec)

        if not type(wghtvec) is np.ndarray:
            wghtvec = np.array(wghtvec)

        self.__x = self.strikeForReducedCalldelta(F, self.__y, T, \
            calldeltavec[::-1])

        #% Convergence parameters:
        #%   STOL = Absolute tolerance for SIGS.
        #%   MAXIT = Maximum number of SMCRV/SIGS iterations.
        #%   DYSTOL = Bound on the maximum relative change in a
        #%            component of YS defining convergence of
        #%            the SMCRV/SIGS iteration when SIG1 >= 0.

        self.__per = False
        self.__sig1 = -1.0
        self.__stol = 0.0
        self.__maxit = 99
        self.__dystol = 0.01
        self.__dbltol = 2.2e-14
        self.__sbig = 100.0

        self.__sigma = np.array([0.0] * (numdeltas-1))
        self.__ys = np.array([0.0] * numdeltas)
        self.__yp = np.array([0.0] * numdeltas)

        self.tspss(wghtvec, sm, smtol)

    @property
    def x(self):
        return self.__x

    @property
    def y(self):
        return self.__y

    @property
    def ys(self):
        return self.__ys

    @property
    def yp(self):
        return self.__yp

    @property
    def sigma(self):
        return self.__sigma

    def strikeForReducedCalldelta(self, F, volvec, T, \
        calldeltavec):

        n1dvec = scipy.stats.norm.ppf(calldeltavec)

        retk = F / np.exp(volvec * math.sqrt(T) * n1dvec)
        return(retk)

    #comments from the matlab function
    #
    #% tspss:  Parameters defining shape-preserving smoothing curve
    #%
    #% USAGE:  [sigma,ys,yp,nit,ier,dys,dsmax] = tspss(x,y,per, ...
    #%         sig1,w,sm,smtol)
    #%
    #%   This function computes a set of parameter values that
    #% define a smoothing tension spline H(x).  The parameters
    #% consist of knot values YS and derivatives YP computed
    #% by Function SMCRV, and tension factors SIGMA computed by
    #% Function SIGS (unless SIG1 >= 0, indicating uniform
    #% tension).  The Hermite interpolatory tension spline H(x)
    #% defined by the knot values and derivatives has two contin-
    #% uous derivatives and satisfies either natural or periodic
    #% end conditions.
    #%
    #%   The tension spline may be evaluated by Function TSVAL1
    #% or Functions HVAL (values), HPVAL (first derivatives),
    #% HPPVAL (second derivatives), HPPPVAL (third derivatives),
    #% and TSINTL (integrals).
    #%
    #% On input:
    #%
    #%       X = Vector of length N containing a strictly in-
    #%           creasing sequence of abscissae:  X(I) < X(I+1)
    #%           for I = 1 to N-1.  N >= 2 and N >= 3 if PER =
    #%           TRUE.
    #%
    #%       Y = Vector of length N containing data values asso-
    #%           ciated with the abscissae.  If PER = TRUE, it is
    #%           assumed that Y(N) = Y(1).
    #%
    #%       PER = Logical variable with value TRUE if and only
    #%             H(x) is to be a periodic function with period
    #%             X(N)-X(1).  It is assumed without a test that
    #%             Y(N) = Y(1) in this case.  On output, YP(N) =
    #%             YP(1) and, more generally, the values and
    #%             first two derivatives of H at X(1) agree with
    #%             those at X(N).  If H(x) is one of the compo-
    #%             nents of a parametric curve, this option may
    #%             be used to obtained a closed curve.  If PER =
    #%             FALSE, H satisfies natural end conditions:
    #%             zero second derivatives at X(1) and X(N).
    #%
    #%       SIG1 = Constant (uniform) tension factor in the
    #%              range 0 to SBIG, or negative value if
    #%              variable tension is to be used.  If SIG1 = 0,
    #%              H(x) is a cubic spline, and as SIG1
    #%              increases, H(x) approaches piecewise linear.
    #%              If SIG1 < 0, tension factors are chosen (by
    #%              SIGS) to preserve local monotonicity and
    #%              convexity of the data.  This may result in a
    #%              better fit than the case of uniform tension,
    #%              but requires an iteration on calls to SMCRV
    #%              and SIGS.
    #%
    #%       W = Vector of length N containing positive weights
    #%           associated with the data values.  The recommend-
    #%           ed value of W(I) is 1/DY^2, where DY is the
    #%           standard deviation associated with Y(I).  If
    #%           nothing is known about the errors in Y, a con-
    #%           stant (estimated value) should be used for DY.
    #%           If PER = TRUE, it is assumed that W(N) = W(1).
    #%
    #%       SM = Positive parameter specifying an upper bound on
    #%            Q2(YS), where Q2(YS) is the weighted sum of
    #%            squares of deviations from the data (differ-
    #%            ences between YS and Y).  H(x) is linear (and
    #%            Q2 is minimized) if SM is sufficiently large
    #%            that the constraint is not active.  It is
    #%            recommended that SM satisfy N-SQRT(2N) <= SM
    #%            <= N+SQRT(2N) and SM = N is reasonable if
    #%            W(I) = 1/DY^2.
    #%
    #%       SMTOL = Parameter in the range (0,1) specifying the
    #%               relative error allowed in satisfying the
    #%               constraint:  the constraint is assumed to
    #%               be satisfied if SM*(1-SMTOL) <= Q2 <=
    #%               SM*(1+SMTOL).  A reasonable value for SMTOL
    #%               is SQRT(2/N) for N > 2.
    #%
    #% On output:
    #%
    #%       SIGMA = Array of size 1 by n-1 containing tension
    #%               factors.  SIGMA(I) is associated with inter-
    #%               val (X(I),X(I+1)) for I = 1 to N-1.  SIGMA
    #%               is zeros if N is invalid or -4 < IER < -1,
    #%               and SIGMA is constant if IER = -1 (and N
    #%               is valid) or IER = -4.
    #%
    #%       YS = Vector of size(X) containing values of H at the
    #%            abscissae.  YS(N) = YS(1) if PER = TRUE.  YS is
    #%            zeros if IER < 0.
    #%
    #%       YP = Vector of size(X) containing first derivative
    #%            values of H at the abscissae.  YP(N) = YP(1)
    #%            if PER = TRUE.  YP is zeros if IER < 0.
    #%
    #%       NIT = Number of iterations (calls to SIGS).  NIT = 0
    #%             if IER < 0 or SIG1 >= 0.  If NIT > 0, NIT+1
    #%             calls to SMCRV were employed.
    #%
    #%       IER = Error indicator:
    #%             IER = 0 if no errors were encountered and the
    #%                     constraint is active:  Q2(YS) is ap-
    #%                     proximately equal to SM.
    #%             IER = 1 if no errors were encountered but the
    #%                     constraint is not active:  YS and YP
    #%                     are the values and derivatives of the
    #%                     linear function (constant function if
    #%                     PERIOD = TRUE) that minimizes Q2, and
    #%                     Q1 = 0 (refer to SMCRV).
    #%             IER = -1 if N, W, SM, or SMTOL is outside its
    #%                      valid range.
    #%             IER = -2 if the number of input arguments is
    #%                      not valid.
    #%             IER = -3 if SIG1 > SBIG.
    #%             IER = -4 if the abscissae X are not strictly
    #%                      increasing.
    #%
    #%       DYS = Maximum relative change in a component of YS
    #%             on the last iteration if NIT > 0.
    #%
    #%       DSMAX = Maximum relative change in a component of
    #%               SIGMA on the last iteration if NIT > 0.
    #%
    #% Modules required by TSPSS:  B2TRI or B2TRIP, SIGS, SMCRV,
    #%                               SNHCSH, YPCOEF
    #%
    #%***********************************************************

    def tspss(self, w, sm, smtol):

        m = len(self.__x)
        n = len(self.__x)

        self.__sigma = np.array([0.0] * (n-1))

        nit = 0
        dys = 0.0
        dsmax = 0.0

        #% Store uniform tension factors, or initialize SIGMA to zeros.

        sig = -1.0
        if self.__sig1 >= 0.0:
            raise ValueError('uniform tension case not implemented: ' + \
                'sig1 = ' + str(self.__sig1))
        else:
            sig = 0.0

        self.__sigma = np.array([sig] * (n-1))

        self.smcrv(sm, w, smtol)

        #%   Iterate on calls to SIGS and SMCRV.  The function
        #%     values YS from the previous iteration are stored
        #%     in WK.
        #%
        #%   DYS is the maximum relative change in a component of YS.
        #%   ICNT is the number of tension factors that were
        #%        increased by SIGS.
        #%   DSMAX is the maximum relative change in a component of
        #%         SIGMA.

        wk = np.array([0.0] * m)
        e = np.array([0.0] * m)
        icnt = 0

        for iter in range(self.__maxit):

            wk[1:-1] = self.__y[1:-1]

            (icnt,dsmax) = self.sigs()
            self.smcrv(sm, w, smtol)

            e[1:-1] = np.abs(self.__ys[1:-1] - wk[1:-1])

            wksub = np.where(wk != 0.0)
            e[wksub] = e[wksub] / wk[wksub]

            dys = np.max(e)

            if 0 == icnt or dys < self.__dystol:
                break

    #comments from matlab
    #
    #% smcrv:  C^2 smoothing curve
    #%
    #% USAGE:  [ys,yp,ier] = smcrv(x,y,sigma,period,w,sm,smtol)
    #%
    #%   Given a sequence of abscissae X with associated data
    #% values Y and tension factors SIGMA, this function deter-
    #% mines a set of function values YS and first derivatives YP
    #% associated with a Hermite interpolatory tension spline
    #% H that smoothes the data.  H has two continuous deriva-
    #% tives at every point, and satisfies either natural or
    #% periodic end conditions.  The values and derivatives are
    #% chosen to minimize a quadratic functional Q1(YS,YP)
    #% subject to the constraint Q2(YS) <= SM for Q2(YS) =
    #% (Y-YS)^T*W*(Y-YS), where ^T denotes transpose, and W is
    #% a diagonal matrix of positive weights.
    #%
    #%   Functions HVAL, HPVAL, HPPVAL, HPPVAL, and TSINTL may be
    #% called to compute values, derivatives, and integrals of H.
    #% The function values YS must be used as data values in those
    #% functions.
    #%
    #%   The smoothing procedure is an extension of the method
    #% for cubic spline smoothing due to C. Reinsch:  Numer.
    #% Math., 10 (1967) and 16 (1971).  Q1 is defined as the sum
    #% of integrals over the intervals (X(I),X(I+1)) of HPP^2 +
    #% (SIGMA(I)/DX)^2*(HP-S)^2, where DX = X(I+1)-X(I), HP and
    #% HPP denote first and second derivatives of H, and S =
    #% (YS(I+1)-YS(I))/DX.  Introducing a smoothing parameter P,
    #% and assuming the constraint is active, the problem is
    #% equivalent to minimizing Q(P,YS,YP) = Q1(YS,YP) +
    #% P*(Q2(YS)-SM).  The procedure consists of finding a zero
    #% of G(P) = 1/SQRT(Q2) - 1/SQRT(SM), where YS and YP satisfy
    #% the order 2N symmetric positive-definite linear system
    #% obtained by setting the gradient of Q (treated as a func-
    #% tion of YS and YP) to zero.
    #%
    #%   Note that the interpolation problem corresponding to
    #% YS = Y, SM = 0, and P infinite is solved by Function
    #% YPC2 or YPC2P.
    #%
    #% On input:
    #%
    #%       X = Vector of length N containing a strictly in-
    #%           creasing sequence of abscissae:  X(I) < X(I+1)
    #%           for I = 1 to N-1.  N >= 2 if PERIOD = FALSE,
    #%           and N >= 3 if PERIOD = TRUE.
    #%
    #%       Y = Vector of length N containing data values assoc-
    #%           iated with the abscissae.  If PERIOD = TRUE, it
    #%           is assumed that Y(N) = Y(1).
    #%
    #%       SIGMA = Vector of length N-1 containing tension
    #%               factors.  SIGMA(I) is associated with inter-
    #%               val (X(I),X(I+1)) for I = 1 to N-1.  If
    #%               SIGMA(I) = 0, H is cubic, and as SIGMA in-
    #%               creases, H approaches linear in the inter-
    #%               val.
    #%
    #%       PERIOD = Periodic end condition flag:
    #%                PERIOD = 0 if H is to satisfy natural end
    #%                           conditions:  zero second der-
    #%                           ivatives at X(1) and X(N).
    #%                PERIOD = 1 if H is to satisfy periodic
    #%                           end conditions:  the values
    #%                           and first two derivatives at
    #%                           X(1) agree with those at X(N),
    #%                           and a period thus has length
    #%                           X(N)-X(1).
    #%
    #%       W = Vector of length N containing positive weights
    #%           associated with the data values.  The recommend-
    #%           ed value of W(I) is 1/DY^2, where DY is the
    #%           standard deviation associated with Y(I).  If
    #%           nothing is known about the errors in Y, a con-
    #%           stant (estimated value) should be used for DY.
    #%           If PERIOD = TRUE, it is assumed that W(N) =
    #%           W(1).
    #%
    #%       SM = Positive parameter specifying an upper bound on
    #%            Q2(YS).  H(x) is linear (and Q2 is minimized)
    #%            if SM is sufficiently large that the constraint
    #%            is not active.  It is recommended that SM sat-
    #%            isfy N-SQRT(2N) <= SM <= N+SQRT(2N) and
    #%            SM = N is reasonable if W(I) = 1/DY^2.
    #%
    #%       SMTOL = Parameter in the range (0,1) specifying the
    #%               relative error allowed in satisfying the
    #%               constraint:  the constraint is assumed to
    #%               be satisfied if SM*(1-SMTOL) <= Q2 <=
    #%               SM*(1+SMTOL).  A reasonable value for SMTOL
    #%               is SQRT(2/N) for N > 2.
    #%
    #% On output:
    #%
    #%       YS = Vector of size(X) containing values of H at the
    #%            abscissae unless IER < 0.  YS(N) = YS(1) if
    #%            PERIOD = TRUE.
    #%
    #%       YP = Vector of size(X) containing first derivative
    #%            values of H at the abscissae unless IER < 0.
    #%            YP(N) = YP(1) if PERIOD = TRUE.
    #%
    #%       IER = Error indicator:
    #%             IER = 0 if no errors were encountered and the
    #%                     constraint is active:  Q2(YS) is ap-
    #%                     proximately equal to SM.
    #%             IER = 1 if no errors were encountered but the
    #%                     constraint is not active:  YS and YP
    #%                     are the values and derivatives of the
    #%                     linear function (constant function if
    #%                     PERIOD = TRUE) that minimizes Q2, and
    #%                     Q1 = 0.
    #%             IER = -1 if N, W, SM, or SMTOL is outside its
    #%                      valid range.  YS and YP are zeros
    #%                      in this case.
    #%             IER = -I if X(I) <= X(I-1) for some I in the
    #%                      range 2 to N.  YS and YP are zeros
    #%                      in this case.
    #%
    #% Modules required by SMCRV:  B2TRI or B2TRIP, SNHCSH,
    #%                                    YPCOEF
    #%
    #%***********************************************************

    def smcrv(self, sm, w, smtol):

        m = len(self.__x)
        n = len(self.__x)

        if sm <= 0.0:
            raise ValueError('sm should be > 0: ' + str(sm))

        if smtol <= 0.0 or smtol >= 1.0:
            raise ValueError('smtol must be in (0,1): ' + str(smtol))

        if np.min(w) <= 0.0:
            raise ValueError('all weights must be > 0: ' + str(w))

        if np.any(self.__x != sorted(self.__x)):
            raise ValueError('self.__x must be increasing: ' + str(self.__x))

        if len(self.__sigma) != m - 1:
            raise ValueError('self.__sigma should have size ' + str(m-1) + \
                ', found: ' + str(len(self.__sigma)))

        if len(w) != m:
            raise ValueError('w should have size ' + str(m) + ', found: ' + \
                str(len(w)))

        if self.__per:
            raise ValueError('periodic end conditions not implemented')

        t = self.__x * w

        #% Sum(w(i)*x(i)^2)

        c11 = np.sum(t * self.__x)

        #% Sum(w(i)*x(i))

        c12 = np.sum(t)

        #% Sum(w(i)*x(i)*y(i))

        r1 = np.sum(t * self.__y)

        #% Sum(w(i)*y(i))

        c22 = np.sum(w)

        #% Sum(w(i))

        r2 = np.sum(w * self.__y)

        #% Solve the system for (HP,H0), where HP is the derivative
        #%   (constant) and H0 = H(0).

        h0 = -1.0
        hp = -1.0
        if self.__per:
            raise ValueError('periodic end conditions not implemented')
        else:
            h0 = (c11 * r2 - c12 * r1) / (c11 * c22 - c12 * c12)
            hp = (r1 - c12 * h0) / c11

        #% Store function values and derivatives, and accumulate
        #%   Q2 = (Y-YS)^T*W*(Y-YS).

        self.__ys = hp * self.__x + h0
        self.__yp = np.array([hp] * m)
        t = self.__y - self.__ys

        q2 = np.sum(t * w * t)

        #% Test for the constraint satisfied by the linear fit.

        if q2 <= sm * (1.0 + smtol):
            raise ValueError('The constraint is not active, and ' + \
                'the fit is linear.')

        #% Compute the matrix components for the linear system.

        dx = np.diff(self.__x)
        sig = np.abs(self.__sigma)

        (d, sd) = self.ypcoef(sig, dx)

        #% Compute G0 = G(0), and print a heading.

        s = 1.0 / math.sqrt(sm)
        g0 = 1.0 / math.sqrt(q2) - s

        #% G(P) is strictly increasing and concave, and G(0) < 0.
        #%
        #% Initialize parameters for the zero finder.

        tol1 = (1.0 - 1.0 / math.sqrt(1.0 + smtol)) * s
        tol2 = (-1.0 + 1.0 / math.sqrt(1.0 - smtol)) * s
        tol = np.min([tol1, tol2])

        #% Find a bracketing interval

        g = 0.0
        p = sm
        while g <= 0.0:
            p = 10.0 * p
            g = self.fsmcrv(p, w, d, sd, g0, s)

        p = scipy.optimize.brenth(self.fsmcrv, 0.0, p, \
            args=(w, d, sd, g0, s), \
            maxiter=self.__maxit)

        #call once more to make sure ys and yp set to final values

        self.fsmcrv(p, w, d, sd, g0, s)

    #comments from matlab
    #
    #% ypcoef:  Coefficients for ypc2p and smcrv
    #%
    #% USAGE:  [d,sd] = ypcoef(sigma,dx)
    #%
    #%   This function computes the coefficients of the deriva-
    #% tives in the symmetric diagonally dominant tridiagonal
    #% system associated with the C-2 derivative estimation pro-
    #% cedure for a Hermite interpolatory tension spline.
    #%
    #% On input:
    #%
    #%       SIGMA = Nonnegative tension factor (or vector of
    #%               factors) associated with intervals.
    #%
    #%       DX = Positive interval widths in one-to-one corres-
    #%            pondence with SIGMA.
    #%
    #% On output:
    #%
    #%       D = Components of the diagonal terms associated with
    #%           the intervals.  D = SIGMA.*(SIGMA.*COSHM(SIGMA)-
    #%           SINHM(SIGMA))./(DX.*E), where E = SIGMA.*
    #%           SINH(SIGMA) - 2*COSHM(SIGMA).
    #%
    #%       SD = Subdiagonal (superdiagonal) terms.  SD =
    #%            SIGMA.*SINHM(SIGMA)./(DX.*E).
    #%
    #% Module required by YPCOEF:  SNHCSH
    #%
    #%***********************************************************

    def ypcoef(self, sig, dx):

        d = np.zeros(len(sig))
        sd = np.zeros(len(sig))

        #% k = indices for which SIGMA = 0:  cubic interpolant.

        sigsub = np.where(sig < self.__dbltol)
        d[sigsub] = 4.0 / dx[sigsub]
        sd[sigsub] = 2.0 / dx[sigsub]

        #% For 0 < SIGMA <= .5, use approximations designed to avoid
        #%   cancellation error in the hyperbolic functions.

        sigsub = np.intersect1d(np.where(sig >= self.__dbltol), \
            np.where(sig <= 0.5))
        (sinhm,coshm,coshmm) = self.snhcsh(sig[sigsub])
        e = (sig[sigsub] * sinhm - coshmm - coshmm) * dx[sigsub]
        d[sigsub] = sig[sigsub] * (sig[sigsub] * coshm - sinhm) / e
        sd[sigsub] = sig[sigsub] * sinhm / e

        #% For SIGMA > .5, scale SINHM and COSHM by 2*EXP(-SIGMA) in
        #%   order to avoid overflow when SIGMA is large.

        sigsub = np.where(sig > 0.5)
        ems = np.exp(-sig[sigsub])
        ssinh = 1.0 - ems * ems
        ssm = ssinh - 2.0 * sig[sigsub] * ems
        scm = (1.0 - ems) * (1.0 - ems)
        e = (sig[sigsub] * ssinh - scm - scm) * dx[sigsub]
        d[sigsub] = sig[sigsub] * (sig[sigsub] * scm - ssm) / e
        sd[sigsub] = sig[sigsub] * ssm / e

        return (d,sd)

    def fsmcrv(self, p, w, d, sd, g0, s):

        if 0.0 == p:
            return g0

        self.b2tri(w, p, d, sd)

        t = self.__y - self.__ys

        q2 = np.sum(t * w * t)

        g = 1.0 / math.sqrt(q2) - s

        return g

    def snhcsh(self, inx):

        bconvert = False
        if not type(inx) is np.ndarray:
            bconvert = True
            inx = np.array([inx])

        #% Coefficients defining rational approximations for small x.

        p1 = -3.51754964808151394800e5
        p2 = -1.15614435765005216044e4
        p3 = -1.63725857525983828727e2
        p4 = -7.89474443963537015605e-1
        q1 = -2.11052978884890840399e6
        q2 = 3.61578279834431989373e4
        q3 = -2.77711081420602794433e2
        q4 = 1.0

        ax = np.abs(inx)
        xs = inx * inx
        sinhm = np.zeros(np.shape(inx))
        coshm = np.zeros(np.shape(inx))
        coshmm = np.zeros(np.shape(inx))

        #% Approximations for small X:

        xsub = np.where(ax <= 0.5)
        xc = inx[xsub] * xs[xsub]
        p = ((p4 * xs[xsub] + p3) * xs[xsub] + p2) * xs[xsub] + p1
        q = ((q4 * xs[xsub] + q3) * xs[xsub] + q2) * xs[xsub] + q1
        sinhm[xsub] = xc * (p / q)
        xsd4 = 0.25 * xs[xsub]
        xsd2 = xsd4 + xsd4
        p = ((p4 * xsd4 + p3) * xsd4 + p2) * xsd4 + p1
        q = ((q4 * xsd4 + q3) * xsd4 + q2) * xsd4 + q1
        f = xsd4 * (p / q)
        coshmm[xsub] = xsd2 * f * (f + 2.0)
        coshm[xsub] = coshmm[xsub] + xsd2

        #% Approximations for large X:

        xsub = np.where(ax > 0.5)
        expx = np.exp(ax[xsub])
        sinhm[xsub] = -(((1.0 / expx + ax[xsub]) + ax[xsub]) - expx) / 2.0
        xsub_neg = np.intersect1d(xsub, np.where(inx < 0.0))
        sinhm[xsub_neg] = -sinhm[xsub_neg]
        coshm[xsub] = ((1.0 / expx - 2.0) + expx) / 2.0
        coshmm[xsub] = coshm[xsub] - xs[xsub] / 2.0

        if bconvert:
            sinhm = sinhm[0]
            coshm = coshm[0]
            coshmm = coshmm[0]
            inx = inx[0]

        return (sinhm,coshm,coshmm)

    #coments from matlab
    #
    #% b2tri:  SPD block tridiagonal system solver
    #%
    #% USAGE:  [ys,yp] = b2tri(x,y,w,p,d,sd)
    #%
    #%   This function solves the order 2N symmetric positive-
    #% definite block tridiagonal linear system associated with
    #% minimizing the quadratic functional Q(YS,YP) described in
    #% Function SMCRV.
    #%
    #% On input:
    #%
    #%       X,Y,W = Vectors of length N containing abscissae,
    #%               data values, and positive weights, respect-
    #%               ively.  The abscissae must be strictly in-
    #%               creasing.
    #%
    #%       P = Positive smoothing parameter defining Q.
    #%
    #%       D,SD = Vectors of length N-1 containing positive ma-
    #%              trix entries.  Letting DX and SIG denote the
    #%              width and tension factor associated with the
    #%              interval (X(I),X(I+1)), D(I) = SIG*(SIG*
    #%              COSHM(SIG) - SINHM(SIG))/(DX*E) and SD(I) =
    #%              SIG*SINHM(SIG)/(DX*E) where E = SIG*SINH(SIG)
    #%              - 2*COSHM(SIG).
    #%
    #%   Note that no test is made for a nonpositive value of
    #% X(I+1)-X(I), W(I), D(I), or SD(I).
    #%
    #% On output:
    #%
    #%       YS,YP = Vectors of size(X) containing solution com-
    #%               ponents:  function and derivative values,
    #%               respectively, at the abscissae.
    #%
    #% Modules required by B2TRI:  None
    #%
    #%***********************************************************

    def b2tri(self, w, p, d, sd):

        m = len(self.__x)
        n = len(self.__x)
        nm1 = n - 1

        #% Initialize output arrays.

        self.__ys = np.zeros(m)
        self.__yp = np.zeros(m)

        #% Work space:

        t11 = np.zeros(nm1)
        t12 = np.zeros(nm1)
        t21 = np.zeros(nm1)
        t22 = np.zeros(nm1)

        #% The forward elimination step consists of scaling a row by
        #%   the inverse of its diagonal block and eliminating the
        #%   subdiagonal block.  The superdiagonal is stored in T and
        #%   the right hand side in YS,YP.  For J = 11, 12, and 22,
        #%   SJI and SJIM1 denote the elements in position J of the
        #%   superdiagonal block in rows I and I-1, respectively.
        #%   Similarly, DJI denotes an element in the diagonal block
        #%   of row I.

        #% Initialize for I = 2.

        dx = self.__x[1] - self.__x[0]
        dim1 = d[0]
        s22im1 = sd[0]
        s12im1 = (dim1 + s22im1) / dx
        s11im1 = -2.0 * s12im1 / dx
        r1 = p * w[0]
        d11i = r1 - s11im1
        d12i = s12im1
        d22i = dim1
        den = d11i * d22i - d12i * d12i
        t11[0] = (d22i * s11im1 + d12i * s12im1) / den
        t12[0] = (d22i * s12im1 - d12i * s22im1) / den
        t21[0] = -(d12i * s11im1 + d11i * s12im1) / den
        t22[0] = (d11i * s22im1 - d12i * s12im1) / den
        r1 = r1 * self.__y[0] / den
        self.__ys[0] = d22i * r1
        self.__yp[0] = -d12i * r1

        #% I = 2 to N-1:

        for i in range(1,nm1):
            im1 = i - 1
            dx = self.__x[i + 1] - self.__x[i]
            di = d[i]
            s22i = sd[i]
            s12i = (di + s22i) / dx
            s11i = -2.0 * s12i / dx
            r1 = p * w[i]
            d11i = r1 - s11im1 - s11i - (s11im1 * t11[im1] - s12im1 * t21[im1])
            d12i = s12i - s12im1 - (s11im1 * t12[im1] - s12im1 * t22[im1])
            d22i = dim1 + di - (s12im1 * t12[im1] + s22im1 * t22[im1])
            den = d11i * d22i - d12i * d12i
            t11[i] = (d22i * s11i + d12i * s12i) / den
            t12[i] = (d22i * s12i - d12i * s22i) / den
            t21[i] = -(d12i * s11i + d11i * s12i) / den
            t22[i] = (d11i * s22i - d12i * s12i) / den
            r1 = r1 * self.__y[i] - s11im1 * self.__ys[im1] + s12im1 * self.__yp[im1]
            r2 = -s12im1 * self.__ys[im1] - s22im1 * self.__yp[im1]
            self.__ys[i] = (d22i * r1 - d12i * r2) / den
            self.__yp[i] = (d11i * r2 - d12i * r1) / den
            dim1 = di
            s22im1 = s22i
            s12im1 = s12i
            s11im1 = s11i

        #% I = N:

        r1 = p * w[n - 1]
        d11i = r1 - s11im1 - (s11im1 * t11[nm1 - 1] - s12im1 * t21[nm1 - 1])
        d12i = -s12im1 - (s11im1 * t12[nm1 - 1] - s12im1 * t22[nm1 - 1])
        d22i = dim1 - (s12im1 * t12[nm1 - 1] + s22im1 * t22[nm1 - 1])
        den = d11i * d22i - d12i*d12i
        r1 = r1*self.__y[n - 1] - s11im1 * self.__ys[nm1 - 1] + s12im1 * self.__yp[nm1 - 1]
        r2 = -s12im1 * self.__ys[nm1 - 1] - s22im1 * self.__yp[nm1 - 1]
        self.__ys[n - 1] = (d22i * r1 - d12i * r2) / den
        self.__yp[n - 1] = (d11i * r2 - d12i * r1) / den

        #% Back solve the system.

        for i in range(nm1 - 1, -1, -1):
            self.__ys[i] = self.__ys[i] - (t11[i] * self.__ys[i + 1] + t12[i] * self.__yp[i + 1])
            self.__yp[i] = self.__yp[i] - (t21[i] * self.__ys[i + 1] + t22[i] * self.__yp[i + 1])

    #comments from matlab
    #
    #% sigs:  Minimum tension for monotonicity and convexity
    #%
    #% USAGE:  [sigma,dsmax,ier] = sigs(x,y,yp,tol,sigma)
    #%
    #%   Given a set of abscissae X with associated data values Y
    #% and derivatives YP, this function determines the small-
    #% est (nonnegative) tension factors SIGMA such that the Her-
    #% mite interpolatory tension spline H(x) preserves local
    #% shape properties of the data.  In an interval (X1,X2) with
    #% data values Y1,Y2 and derivatives YP1,YP2, the properties
    #% of the data are
    #%
    #%       Monotonicity:  S, YP1, and YP2 are nonnegative or
    #%                        nonpositive,
    #%  and
    #%       Convexity:     YP1 <= S <= YP2  or  YP1 >= S
    #%                        >= YP2,
    #%
    #% where S = (Y2-Y1)/(X2-X1).  The corresponding properties
    #% of H are constant sign of the first and second deriva-
    #% tives, respectively.  Note that, unless YP1 = S = YP2, in-
    #% finite tension is required (and H is linear on the inter-
    #% val) if S = 0 in the case of monotonicity, or if YP1 = S
    #% or YP2 = S in the case of convexity.
    #%
    #%   SIGS may be used in conjunction with Function YPC2
    #% (or YPC2P) in order to produce a C-2 interpolant that
    #% preserves the shape properties of the data.  This is
    #% achieved by calling YPC2 with SIGMA initialized to the
    #% zero vector, and then alternating calls to SIGS with
    #% calls to YPC2 until the change in SIGMA is small (refer to
    #% the parameter descriptions for SIGMA, DSMAX and IER), or
    #% the maximum relative change in YP is bounded by a toler-
    #% ance (a reasonable value is .01).  A similar procedure may
    #% be used to produce a C-2 shape-preserving smoothing curve
    #% (Function SMCRV).
    #%
    #%   Refer to Function SIGBI for a means of selecting mini-
    #% mum tension factors to satisfy more general constraints.
    #%
    #% On input:
    #%
    #%       X = Vector of length N containing a strictly in-
    #%           creasing sequence of abscissae:  X(I) < X(I+1)
    #%           for I = 1 to N-1.  N >= 2.
    #%
    #%       Y = Vector of length N containing data values (or
    #%           function values computed by SMCRV) associated
    #%           with the abscissae.  H(X(I)) = Y(I) for I =
    #%           1 to N.
    #%
    #%       YP = Vector of length N containing first derivatives
    #%            of H at the abscissae.  Refer to Functions
    #%            YPC1, YPC1P, YPC2, YPC2P, and SMCRV.
    #%
    #%       TOL = Nonnegative tolerance for the zero finder when
    #%             nonzero finite tension is necessary and
    #%             sufficient to satisfy the constraint.  Use
    #%             TOL = 0 for full accuracy.
    #%
    #%       SIGMA = Vector of length N-1 containing minimum val-
    #%               ues of the tension factors.  SIGMA(I) is as-
    #%               sociated with interval (I,I+1) and SIGMA(I)
    #%               >= 0 for I = 1 to N-1.  SIGMA should be
    #%               set to the zero vector if minimal tension
    #%               is desired, and should be unchanged from a
    #%               previous call in order to ensure convergence
    #%               of the C-2 iterative procedure.
    #%
    #% On output:
    #%
    #%       SIGMA = Array containing tension factors for which
    #%               H(x) preserves the properties of the data,
    #%               with the restriction that SIGMA(I) <= SBIG
    #%               for all I (unless the input value is larger).
    #%               The factors are as small as possible (within
    #%               the tolerance), but not less than their
    #%               input values.  If infinite tension is re-
    #%               quired in interval (X(I),X(I+1)), then
    #%               SIGMA(I) = SBIG (and H is an approximation
    #%               to the linear interpolant on the interval),
    #%               and if neither property is satisfied by the
    #%               data, then SIGMA(I) = 0 (unless the input
    #%               value is positive), and thus H is cubic in
    #%               the interval.
    #%
    #%       DSMAX = Maximum increase in a component of SIGMA
    #%               from its input value.  The increase is a
    #%               relative change if the input value is
    #%               nonzero, and an absolute change otherwise.
    #%
    #%       IER = Error indicator and information flag:
    #%             IER = I if no errors were encountered and I
    #%                     components of SIGMA were altered from
    #%                     their input values for 0 <= I <=
    #%                     N-1.
    #%             IER = -1 if N < 2.  SIGMA is not altered in
    #%                      this case.
    #%             IER = -I if X(I) <= X(I-1) for some I in the
    #%                      range 2 to N.  SIGMA(J-1) is unal-
    #%                      tered for J = I to N in this case.
    #%
    #% Module required by SIGS:  SNHCSH
    #%
    #%***********************************************************

    def sigs(self):

        tol = self.__stol
        sbig = self.__sbig

        #% Initialize change counter IER and maximum change DSMAX, and
        #%   test for n < 2.

        ier = 0
        dsmax = 0.0
        n = len(self.__x)
        if n < 2:
            ier = -1
            return

        #% Compute an absolute tolerance FTOL = abs(TOL) and a
        #%   relative tolerance RTOL = 1000*MACHEPS for the
        #%   convexity computation, and compute options for fzero
        #%   (used for the monotonicity computation).

        ftol = np.abs(tol)
        rtol = 10.0 * self.__dbltol
        rootfindtol = np.max([tol, self.__dbltol])

        #% Loop on subintervals.

        for i in range(n - 1):
            ip1 = i + 1
            dx = self.__x[ip1] - self.__x[i]
            if dx <= 0.0:
                ier = -1 * ip1
                return
            sigin = self.__sigma[i]
            if sigin >= sbig:
                continue

            #% Compute first and second differences.

            s1 = self.__yp[i]
            s2 = self.__yp[ip1]
            s = (self.__y[ip1] - self.__y[i]) / dx
            d1 = s - s1
            d2 = s2 - s
            d1d2 = d1 * d2

            while True:

                #% Test for infinite tension required to satisfy either
                #%   property.

                sig = sbig
                if      (0.0 == d1d2 and s1 != s2) \
                    or  (0.0 == s and s1 * s2 > 0.0):
                    break

                #% Test for SIGMA = 0 sufficient.  The data satisfies convex-
                #%   ity iff D1D2 >= 0, and D1D2 = 0 implies S1 = S = S2.

                sig = 0.0
                if d1d2 >= 0.0:
                    if 0.0 == d1d2:
                        break
                    t = np.max([ d1 / d2, d2 / d1 ])
                    if t <= 2.0:
                        break
                    tp1 = t + 1.0

                    #% Convexity:  Find a zero of F(SIG) = SIG*COSHM(SIG)/
                    #%   SINHM(SIG) - TP1.
                    #%
                    #%   F(0) = 2-T < 0, F(TP1) >= 0, the derivative of F
                    #%     vanishes at SIG = 0, and the second derivative of F is
                    #%     .2 at SIG = 0.  A quadratic approximation is used to
                    #%     obtain a starting point for the Newton method.

                    sig = math.sqrt(10.0 * t - 20.0)
                    nit = 0

                    #%   Top of loop:

                    while True:
                        if sig <= 0.5:
                            (sinhm, coshm, coshmm) = self.snhcsh(sig)
                            t1 = coshm / sinhm
                            fp = t1 + sig * (sig / sinhm - t1 * t1 + 1.0)

                        #%   Scale SINHM and COSHM by 2*EXP(-SIG) in order to avoid
                        #%     overflow with large SIG.

                        else:

                            ems = np.exp(-sig)
                            ssm = 1.0 - ems * (ems + sig + sig)
                            t1 = (1.0 - ems) * (1.0 - ems) / ssm
                            fp = t1 + sig * (2.0 * sig * ems / ssm - t1 * t1 + 1.0)

                        f = sig * t1 - tp1
                        nit = nit + 1

                        #%   Test for convergence.

                        if fp <= 0.0:
                            break
                        dsig = -f / fp
                        if      (np.abs(dsig) <= rtol * sig) \
                            or  (f >= 0.0 and f <= ftol) \
                            or  (np.abs(f) <= rtol):
                            break

                        #%   Update SIG.

                        sig = sig + dsig

                    break

                #% Convexity cannot be satisfied.  Monotonicity can be satis-
                #%   fied iff S1*S >= 0 and S2*S >= 0 since S ~= 0.

                if s1 * s < 0.0 or s2 * s < 0.0:
                    break
                t0 = 3.0 * s - s1 - s2
                d0 = t0 * t0 - s1 * s2

                #% SIGMA = 0 is sufficient for monotonicity iff S*T0 >= 0
                #%   or D0 <= 0.

                if d0 <= 0.0 or s * t0 >= 0.0:
                    break

                #% Monotonicity:  find a zero of F(SIG) = SIGN(S)*HP(R),
                #%   where HPP(R) = 0 and HP, HPP denote derivatives of H.
                #%   F has a unique zero, F(0) = F0 < 0, and F approaches
                #%   abs(S) as SIG increases.
                #%
                #% Store shared variables needed by nested function fsigs.

                if s > 0.0:
                    sgn = 1.0
                elif 0.0 == s:
                    sgn = 0.0
                else:
                    sgn = -1.0
                f0 = sgn * d0 / (2 * t0 - s1 - s2)
                sig = sbig
                fmax = sgn * (sig * s - s1 - s2) / (sig - 2.0)
                if fmax <= 0.0:
                    break
                d1pd2 = d1 + d2
                nit = -1

                f = self.fsigs(sbig, d1, d1pd2, d2, f0, fmax, nit, s2, sgn)
                if f <= 0.0:
                    break

                #% [0,SBIG] is a bracketing interval.

                nit = 0
                sig = scipy.optimize.brenth(self.fsigs, 0.0, sbig, \
                    args=(d1, d1pd2, d2, f0, fmax, nit, s2, sgn), \
                    rtol=rootfindtol, maxiter=self.__maxit)

                break

            #%  Update SIGMA(I), IER, and DSMAX if necessary.

            sig = np.min([sig, sbig])
            if sig > sigin:
                self.__sigma[i] = sig
                ier = ier + 1
                dsig = sig - sigin
                if sigin > 0:
                    dsig = dsig / sigin
                dsmax = np.max([dsmax, dsig])


        return (ier,dsmax)

    def fsigs(self, sig, sbig, d1, d1pd2, d2, f0, fmax, nit, s2, sgn):

        if 0.0 == sig:
            return f0

        #%   Use approximations to the hyperbolic functions designed
        #%     to avoid cancellation error with small SIG.

        if sig <= 0.5:
            (sinhm, coshm, coshmm) = self.snhcsh(sig)

            c1 = sig * coshm * d2 - sinhm * d1pd2
            c2 = sig * (sinhm + sig) * d2 - coshm * d1pd2
            a = c2 - c1
            e = sig * sinhm - coshmm - coshmm
            f = (sgn * (e * s2 - c2) + math.sqrt(a * (c2 + c1))) / e

        #%   Scale SINHM and COSHM by 2*EXP(-SIG) in order to avoid
        #%     overflow with large SIG.

        else:
            ems = np.exp(-sig)
            ems2 = ems + ems
            tm = 1.0 - ems
            ssinh = tm * (1.0 + ems)
            ssm = ssinh - sig * ems2
            scm = tm*tm
            c1 = sig * scm * d2 - ssm * d1pd2
            c2 = sig * ssinh * d2 - scm * d1pd2

            #%   R is in (0,1) and well-defined iff HPP(X1)*HPP(X2) < 0.

            f = fmax
            if (c1 * (sig * scm * d1 - ssm * d1pd2)) < 0:
                a = ems2 * (sig * tm * d2 + (tm - sig) * d1pd2)
                if a * (c2 + c1) >= 0.0:
                    e = sig * ssinh - scm - scm
                    f = (sgn * (e * s2 - c2) + math.sqrt(a * (c2 + c1))) / e

        #%   Update number of iterations NIT.

        nit = nit + 1
        return f

    def ValueAt(self, xin):

        bconvert = False
        if type(xin) is np.ndarray:
            bconvert = False
            xin_local = xin.copy()
        else:
            bconvert = True
            xin_local = np.array([xin])

        #value of spline, as opposed to one of its derivatives
        iflag = 0
        retval = np.zeros(np.shape(xin_local))

        if 0 == len(self.__x):
            raise ValueError('x empty')
        minx = self.__x[0]
        maxx = self.__x[-1]

        xsub = np.where(xin_local <= minx)
        if type(xsub) is tuple:
            xsub = xsub[0]
        if len(xsub) > 0:
            te = np.array([minx])
            v = self.tsval1(iflag, te)
            minx_tensvalue = v[0]
            minx_slope = self.SlopeAt(minx)
            retval[xsub] = minx_tensvalue + minx_slope * (xin_local[xsub] - minx)

        xsub = np.where(xin_local >= maxx)
        if type(xsub) is tuple:
            xsub = xsub[0]
        if len(xsub) > 0:
            te = np.array([maxx])
            v = self.tsval1(iflag, te)
            maxx_tensvalue = v[0]
            maxx_slope = self.SlopeAt(maxx)
            retval[xsub] = maxx_tensvalue + maxx_slope * (xin_local[xsub] - maxx)

        xsub = np.intersect1d(np.where(xin_local > minx), np.where(xin_local < maxx))
        if type(xsub) is tuple:
            xsub = xsub[0]
        if len(xsub) > 0:
            te = xin_local[xsub]
            v = self.tsval1(iflag, te)
            retval[xsub] = v

        if bconvert:
            retval = retval[0]

        return retval

    def SlopeAt(self, xin):

        bconvert = False
        if type(xin) is np.ndarray:
            bconvert = False
            te = xin.copy()
        else:
            bconvert = True
            te = np.array([xin])

        iflag = 1
        retval = np.zeros(np.shape(te))

        if 0 == len(self.__x):
            raise ValueError('x empty')
        minx = self.__x[0]
        maxx = self.__x[-1]

        te[te < minx] = minx
        te[te > maxx] = maxx

        retval = self.tsval1(iflag, te)
        if bconvert:
            retval = retval[0]

        return retval


    #comments from matlab
    #
    #% tsval1:  Values or derivatives of a tension spline
    #%
    #% USAGE:  [v,ier] = tsval1(x,y,yp,sigma,iflag,te)
    #%
    #%   This function evaluates a Hermite interpolatory ten-
    #% sion spline H or its first, second, or third derivative
    #% at a set of points TE.
    #%
    #% On input:
    #%
    #%       X = Vector of length N containing the abscissae.
    #%           These must be in strictly increasing order:
    #%           X(I) < X(I+1) for I = 1 to N-1.  N >= 2.
    #%
    #%       Y = Vector of length N containing data values or
    #%           function values returned by Function SMCRV.
    #%           Y(I) = H(X(I)) for I = 1 to N.
    #%
    #%       YP = Vector of length N containing first deriva-
    #%            tives.  YP(I) = HP(X(I)) for I = 1 to N, where
    #%            HP denotes the derivative of H.
    #%
    #%       SIGMA = Vector of length N-1 containing tension fac-
    #%               tors whose absolute values determine the
    #%               balance between cubic and linear in each
    #%               interval.  SIGMA(I) is associated with int-
    #%               erval (I,I+1) for I = 1 to N-1.
    #%
    #%       IFLAG = Output option indicator:
    #%               IFLAG = 0 if values of H are to be computed.
    #%               IFLAG = 1 if first derivative values are to
    #%                         be computed.
    #%               IFLAG = 2 if second derivative values are to
    #%                         be computed.
    #%               IFLAG = 3 if third derivative values are to
    #%                         be computed.
    #%
    #%       TE = Vector of length NE containing the evaluation
    #%            points.  The sequence should be strictly in-
    #%            creasing for maximum efficiency.  Extrapolation
    #%            is performed if a point is not in the interval
    #%            [X(1),X(N)].  NE > 0.
    #%
    #% On output:
    #%
    #%       V = Vector of size(TE) containing function, first
    #%           derivative, second derivative, or third deriva-
    #%           tive values at the evaluation points unless IER
    #%           < 0.  If IER = -2, V is zeros.  If IER = -1, V
    #%           may be only partially defined.
    #%
    #%       IER = Error indicator:
    #%             IER = 0  if no errors were encountered and
    #%                      no extrapolation occurred.
    #%             IER > 0  if no errors were encountered but
    #%                      extrapolation was required at IER
    #%                      points.
    #%             IER = -1 if the abscissae are not in strictly
    #%                      increasing order.  (This error will
    #%                      not necessarily be detected.)
    #%             IER = -2 if N < 2, IFLAG < 0, IFLAG > 3, or
    #%                      NE < 1.
    #%
    #% Modules required by TSVAL1:  HPPPVAL, HPPVAL, HPVAL, HVAL,
    #%                                SNHCSH
    #%
    #%***********************************************************

    def tsval1(self, iflag, te):

        if 0 == iflag:
            v = self.hval(te)
        elif 1 == iflag:
            v = self.hpval(te)
        else:
            raise ValueError('iflag ' + str(iflag) + ' not implemented yet')

        return v

    #comments from matlab
    #
    #% hval:  Evaluation of Hermite interpolatory tension spline
    #%
    #% USAGE:  [h,ier] = hval(t,x,y,yp,sigma)
    #%
    #%   This function evaluates a Hermite interpolatory tension
    #% spline H at a set of points T.
    #%
    #% On input:
    #%
    #%       T = Point or vector of points at which H is to be
    #%           evaluated.  Extrapolation is performed if T <
    #%           X(1) or T > X(N).
    #%
    #%       X = Vector of length N containing the abscissae.
    #%           These must be in strictly increasing order:
    #%           X(I) < X(I+1) for I = 1 to N-1.  N >= 2.
    #%
    #%       Y = Vector of length N containing data values.
    #%           H(X(I)) = Y(I) for I = 1 to N.
    #%
    #%       YP = Vector of length N containing first deriva-
    #%            tives.  HP(X(I)) = YP(I) for I = 1 to N, where
    #%            HP denotes the derivative of H.
    #%
    #%       SIGMA = Vector of length N-1 containing tension fac-
    #%               tors whose absolute values determine the
    #%               balance between cubic and linear in each
    #%               interval.  SIGMA(I) is associated with int-
    #%               erval (I,I+1) for I = 1 to N-1.
    #%
    #% On output:
    #%
    #%       H = Column vector of length(T) containing function
    #%           values H(T), or zeros if IER < 0.
    #%
    #%       IER = Optional error indicator:
    #%             IER = 0  if no errors were encountered and
    #%                      X(1) <= T <= X(N) for all components
    #%                      of T.
    #%             IER = 1  if no errors were encountered and
    #%                      extrapolation was necessary.
    #%             IER = -1 if the abscissae are not in strictly
    #%                      increasing order.  (This error will
    #%                      not necessarily be detected.)
    #%
    #% Module required by HVAL:  SNHCSH
    #%
    #%***********************************************************

    def hval(self, t):

        n = len(self.__x)
        m = len(t)

        h = np.zeros(m)

        #% Find the index vector I of the left endpoints of the intervals
        #%   containing the elements of T.  If T < X(1) or T > X(N),
        #%   extrapolation is performed using the leftmost or rightmost
        #%   interval.

        i = np.zeros(m, dtype=int)
        for j in range(n - 1):
            for isub in range(m):
                if self.__x[j] <= t[isub]:
                    i[isub] = j

        #% Compute interval widths DX, local coordinates B1 and B2,
        #%   and second differences D1 and D2.

        ip1 = i + 1

        dx = self.__x[ip1] - self.__x[i]
        if np.min(dx) <= 0.0:
            raise ValueError('x not increasing: ' + str(self.__x))

        u = t - self.__x[i]
        b2 = u / dx
        b1 = 1.0 - b2
        y1 = self.__ys[i]
        s1 = self.__yp[i]
        s = (self.__ys[ip1] - y1) / dx
        d1 = s - s1
        d2 = self.__yp[ip1] - s
        sig = np.abs(self.__sigma[i])

        #% For SIG = 0, H is the Hermite cubic interpolant.

        sigsub = np.where(sig < self.__dbltol)
        h[sigsub] = y1[sigsub] \
            + u[sigsub] * \
                (s1[sigsub] + b2[sigsub] * \
                    (d1[sigsub] + b1[sigsub] * (d1[sigsub] - d2[sigsub])))

        #% For 0 < SIG <= .5, use approximations designed to avoid
        #%   cancellation error in the hyperbolic functions.

        sigsub = np.intersect1d(np.where(sig >= self.__dbltol), \
            np.where(sig <= 0.5))
        sb2 = sig[sigsub] * b2[sigsub]
        (sm,cm,cmm) = self.snhcsh(sig[sigsub])
        (sm2,cm2,cmm2) = self.snhcsh(sb2)
        e = sig[sigsub] * sm - cmm - cmm
        h[sigsub] = y1[sigsub] + s1[sigsub] * u[sigsub] + \
            dx[sigsub] * ((cm * sm2 - sm * cm2) * (d1[sigsub] + d2[sigsub]) + \
            sig[sigsub] * (cm * cm2 - (sm + sig[sigsub]) * sm2) * d1[sigsub]) / (sig[sigsub] * e)

        #% For SIG > .5, use negative exponentials in order to avoid
        #%   overflow.  Note that EMS = EXP(-SIG).  In the case of
        #%   extrapolation (negative B1 or B2), H is approximated by
        #%   a linear function if -SIG*B1 or -SIG*B2 is large.

        sigsub = np.where(sig > 0.5)
        sb1 = sig[sigsub] * b1[sigsub]
        sb2 = sig[sigsub] - sb1

        negsub = np.where(-sb1 > self.__sbig)
        if type(negsub) is tuple:
            negsub = negsub[0]
        if len(negsub) > 0:
            snsub = sigsub[negsub]
            h[snsub] = y1[snsub] + s[snsub] * u[snsub]

        negsub = np.where(-sb2 > self.__sbig)
        if type(negsub) is tuple:
            negsub = negsub[0]
        if len(negsub) > 0:
            snsub = sigsub[negsub]
            h[snsub] = y1[snsub] + s[snsub] * u[snsub]

        possub = np.intersect1d(np.where(-sb1 <= self.__sbig), np.where(-sb2 <= self.__sbig))
        if type(possub) is tuple:
            possub = possub[0]
        if len(possub) > 0:
            e1 = np.exp(-sb1[possub])
            e2 = np.exp(-sb2[possub])
            ems = e1 * e2
            tm = 1.0 - ems
            ts = tm * tm
            tp = 1.0 + ems
            if type(sigsub) is tuple:
                spsub = sigsub[0][possub]
            else:
                spsub = sigsub[possub]
            e = tm * (sig[spsub] * tp - tm - tm)
            h[spsub] = y1[spsub] + s[spsub] * u[spsub] + \
                dx[spsub] * (tm * (tp - e1 - e2) * (d1[spsub] + d2[spsub]) + \
                    sig[spsub] * ((e2 + ems * (e1 - 2.0) - b1[spsub] * ts) * d1[spsub] + \
                        (e1 + ems * (e2 - 2.0) - b2[spsub] * ts) * d2[spsub])) / (sig[spsub] * e)

        return h

    #comments from matlab
    #
    #% hpval:  First derivative of Hermite interpolatory tension spline
    #%
    #% USAGE:  [hp,ier] = hpval(t,x,y,yp,sigma)
    #%
    #%   This function evaluates the first derivative HP of a
    #% Hermite interpolatory tension spline H at one or more
    #% points
    #%
    #% On input:
    #%
    #%       T = Point or vector of points at which HP is to be
    #%           evaluated.  Extrapolation is performed if T <
    #%           X(1) or T > X(N).
    #%
    #%       X = Vector of length N containing the abscissae.
    #%           These must be in strictly increasing order:
    #%           X(I) < X(I+1) for I = 1 to N-1.  N >= 2.
    #%
    #%       Y = Vector of length N containing data values.
    #%           H(X(I)) = Y(I) for I = 1 to N.
    #%
    #%       YP = Vector of length N containing first deriva-
    #%            tives.  HP(X(I)) = YP(I) for I = 1 to N.
    #%
    #%       SIGMA = Vector of length N-1 containing tension fac-
    #%               tors whose absolute values determine the
    #%               balance between cubic and linear in each
    #%               interval.  SIGMA(I) is associated with int-
    #%               erval (I,I+1) for I = 1 to N-1.
    #%
    #% On output:
    #%
    #%       HP = Column vector of length(T) containing deriva-
    #%            tive values HP(T), or zeros if IER < 0.
    #%
    #%       IER = Optional error indicator:
    #%             IER = 0  if no errors were encountered and
    #%                      X(1) <= T <= X(N) for all components
    #%                      of T.
    #%             IER = 1  if no errors were encountered and
    #%                      extrapolation was necessary.
    #%             IER = -1 if the abscissae are not in strictly
    #%                      increasing order.  (This error will
    #%                      not necessarily be detected.)
    #%
    #% Module required by HPVAL:  SNHCSH
    #%
    #%***********************************************************

    def hpval(self, t):

        n = len(self.__x)
        m = len(t)

        hp = np.zeros(m)

        #% Find the index vector I of the left endpoints of the intervals
        #%   containing the elements of T.  If T < X(1) or T > X(N),
        #%   extrapolation is performed using the leftmost or rightmost
        #%   interval.

        i = np.zeros(m, dtype=int)
        for j in range(n - 1):
            for isub in range(m):
                if self.__x[j] <= t[isub]:
                    i[isub] = j

        #% Compute interval widths DX, local coordinates B1 and B2,
        #%   and second differences D1 and D2.

        ip1 = i + 1
        dx = self.__x[ip1] - self.__x[i]
        b1 = (self.__x[ip1] - t) / dx
        b2 = 1.0 - b1
        s1 = self.__yp[i]
        s = (self.__ys[ip1] - self.__ys[i]) / dx
        d1 = s - s1
        d2 = self.__yp[ip1] - s
        sig = np.abs(self.__sigma[i])

        #% For SIG = 0, H is the Hermite cubic interpolant.

        sigsub = np.where(sig < self.__dbltol)
        if type(sigsub) is tuple:
            sigsub = sigsub[0]
        hp[sigsub] = s1[sigsub] \
            + b2[sigsub] * (d1[sigsub] + d2[sigsub] - \
                3.0 * b1[sigsub] * (d2[sigsub] - d1[sigsub]))

        #% For 0 < SIG <= .5, use approximations designed to avoid
        #%   cancellation error in the hyperbolic functions.

        sigsub = np.intersect1d(np.where(sig >= self.__dbltol), \
            np.where(sig <= 0.5))
        sb2 = sig[sigsub] * b2[sigsub]
        (sm,cm,cmm) = self.snhcsh(sig[sigsub])
        (sm2,cm2,cmm2) = self.snhcsh(sb2)
        sinh2 = sm2 + sb2
        e = sig[sigsub] * sm - cmm - cmm
        hp[sigsub] = s1[sigsub] \
            + ((cm * cm2 - sm * sinh2) * (d1[sigsub] + d2[sigsub]) + \
            sig[sigsub] * (cm * sinh2 - (sm + sig[sigsub]) * cm2) * d1[sigsub]) / e

        #% For SIG > .5, use negative exponentials in order to avoid
        #%   overflow.  Note that EMS = EXP(-SIG).  In the case of
        #%   extrapolation (negative B1 or B2), H is approximated by
        #%   a linear function if -SIG*B1 or -SIG*B2 is large.

        sigsub = np.where(sig > 0.5)
        sb1 = sig[sigsub] * b1[sigsub]
        sb2 = sig[sigsub] - sb1

        negsub = np.where(-sb1 > self.__sbig)
        if type(negsub) is tuple:
            negsub = negsub[0]
        if len(negsub) > 0:
            snsub = sigsub[negsub]
            hp[snsub] = s[snsub]

        negsub = np.where(-sb2 > self.__sbig)
        if type(negsub) is tuple:
            negsub = negsub[0]
        if len(negsub) > 0:
            snsub = sigsub[negsub]
            hp[snsub] = s[snsub]

        possub = np.intersect1d(np.where(-sb1 <= self.__sbig), np.where(-sb2 <= self.__sbig))
        if type(possub) is tuple:
            possub = possub[0]
        if len(possub) > 0:
            if type(sigsub) is tuple:
                spsub = sigsub[0][possub]
            else:
                spsub = sigsub[possub]
            e1 = np.exp(-sb1[possub])
            e2 = np.exp(-sb2[possub])
            ems = e1 * e2
            tm = 1.0 - ems
            e = tm * (sig[spsub] * (1.0 + ems) - tm - tm)
            hp[spsub] = s[spsub] \
                + (tm * ((e2 - e1) * (d1[spsub] + d2[spsub]) + tm * (d1[spsub] - d2[spsub])) + \
                sig[spsub] * ((e1 * ems - e2) * d1[spsub] + (e1 - e2 * ems) * d2[spsub])) / e

        return hp




