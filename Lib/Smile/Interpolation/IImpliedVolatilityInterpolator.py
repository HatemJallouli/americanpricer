class IImpliedVolatilityInterpolator:
    """ General interface to interpolate the implied volatility smile in strike.

        See Also
        --------
        CubicSplineImpliedVolatilityInterpolator: :class:`~Smile.Interpolation.CubicSplineImpliedVolatilityInterpolator`
        TensionSplineImpliedVolatilityInterpolator: :class:`~Smile.Interpolation.TensionSplineImpliedVolatilityInterpolator`

    """

    def vol_at_strike(self, strike : float) -> float:
        """ Compute the implied volatility for a given strike.

            Parameters
            ----------
            strike : float 
                The strike for which the implied volatility will be calculated.

            Returns
            -------
            implied_volatility : float
                The implied volatility for the provided strike.
        """
        raise NotImplementedError("Method {0} is an abstract method of interface {1}".format(self.vol_at_strike.__name__,
                                                                                             self.__class__.__name__))

    def slope_at_strike(self, strike : float) -> float:
        """ Compute the implied volatility slope or skew for a given strike.

            Parameters
            ----------
            strike : float 
                The strike for which the implied volatility slope will be calculated.

            Returns
            -------
            implied_volatility_slope : float
                The implied volatility slope for the provided strike.
        """
        raise NotImplementedError("Method {0} is an abstract method of interface {1}".format(self.slope_at_strike.__name__,
                                                                                             self.__class__.__name__))