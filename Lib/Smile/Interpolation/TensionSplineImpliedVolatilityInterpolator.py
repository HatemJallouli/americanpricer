from typing import List

from Smile.Interpolation.TensionSpline import TensionSpline
from Smile.Representations.MoneynessSmileRepresentation import MoneynessSmileRepresentation

from Smile.Interpolation.IImpliedVolatilityInterpolator import IImpliedVolatilityInterpolator


class TensionSplineImpliedVolatilityInterpolator(IImpliedVolatilityInterpolator):
    """ Class to interpolate the volatility smile in strikes using a tension spline.

        Parameters
        ----------
        moneyness_smile_representation : :class:`~Smile.Representations.MoneynessSmileRepresentation`
            The volatility smile represented in terms of deltas.
        underlying_level : float
            The market value of the option underlying.
        duration : float
            The maturity of the option for which the smile is computed.
        weightvec : List[float]
            Vector of length N containing positive weights associated with the data values.  The recommend-
            ed value of W(I) is 1/DY^2, where DY is the  standard deviation associated with Y(I).  If
            nothing is known about the errors in Y, a constant (estimated value) should be used for DY.
            If PER = TRUE, it is assumed that W(N) = W(1).
        sm : float
            Positive parameter specifying an upper bound on Q2(YS), where Q2(YS) is the weighted sum of
            squares of deviations from the data (differences between YS and Y).  H(x) is linear (and
            Q2 is minimized) if SM is sufficiently large that the constraint is not active.  It is
            recommended that SM satisfy N-SQRT(2N) <= SM <= N+SQRT(2N) and SM = N is reasonable if W(I) = 1/DY^2.
        smtol : float
            Parameter in the range (0,1) specifying the relative error allowed in satisfying the
            constraint:  the constraint is assumed to be satisfied if SM*(1-SMTOL) <= Q2 <=
            SM*(1+SMTOL).  A reasonable value for SMTOL is SQRT(2/N) for N > 2.

    """
    def __init__(self, moneyness_smile_representation: MoneynessSmileRepresentation, underlying_level: float,
                 duration: float, weightvec : List[float], sm : float, smtol :float):
        self.__spline = TensionSpline(moneyness_smile_representation.moneyness, moneyness_smile_representation.volatilities, underlying_level, duration, weightvec, sm, smtol)

    def vol_at_strike(self, strike: float) -> float:
        """ Compute the implied volatility for a given strike.

            Parameters
            ----------
            strike : float 
                The strike for which the implied volatility will be calculated.

            Returns
            -------
            implied_volatility : float
                The implied volatility for the provided strike.
        """
        return self.__spline.ValueAt(strike)

    def slope_at_strike(self, strike: float) -> float:
        """ Compute the implied volatility slope or skew for a given strike.

            Parameters
            ----------
            strike : float 
                The strike for which the implied volatility slope will be calculated.

            Returns
            -------
            implied_volatility_slope : float
                The implied volatility slope for the provided strike.
        """
        return self.__spline.SlopeAt(strike)

