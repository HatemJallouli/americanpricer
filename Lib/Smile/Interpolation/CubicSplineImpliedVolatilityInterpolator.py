from Smile.Interpolation.CubicSplineLinearExtrapol import CubicSplineLinearExtrapol
from Smile.Representations.MoneynessSmileRepresentation import MoneynessSmileRepresentation
from Smile.Representations.SmileRepresentationConverter import SmileRepresentationConverter

from Smile.Interpolation.IImpliedVolatilityInterpolator import IImpliedVolatilityInterpolator


class CubicSplineImpliedVolatilityInterpolator(IImpliedVolatilityInterpolator):
    """ Class to interpolate the volatility smile in strikes using a cubic spline.

        Parameters
        ----------
        moneyness_smile_representation : :class:`~Smile.Representations.MoneynessSmileRepresentation`
            The volatility smile represented in terms of deltas.
        underlying_level : float
            The market value of the option underlying.
        duration : float
            The maturity of the option for which the smile is computed.

    """
    def __init__(self, moneyness_smile_representation : MoneynessSmileRepresentation, underlying_level : float, duration : float):

        strike_smile_representation = SmileRepresentationConverter.strike_representation(moneyness_smile_representation, underlying_level, duration)
        self.__spline = CubicSplineLinearExtrapol(strike_smile_representation.strikes,strike_smile_representation.volatilities)

    def vol_at_strike(self, strike: float) -> float:
        """ Compute the implied volatility for a given strike.

            Parameters
            ----------
            strike : float 
                The strike for which the implied volatility will be calculated.

            Returns
            -------
            implied_volatility : float
                The implied volatility for the provided strike.
        """
        return self.__spline.value_at(strike)

    def slope_at_strike(self, strike: float) -> float:
        """ Compute the implied volatility slope or skew for a given strike.

            Parameters
            ----------
            strike : float 
                The strike for which the implied volatility slope will be calculated.

            Returns
            -------
            implied_volatility_slope : float
                The implied volatility slope for the provided strike.
        """
        return self.__spline.slope_at(strike)

