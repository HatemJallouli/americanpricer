import sys
import time
from datetime import date

from Backends.MarketDataRetriever import MarketDataRetriever
from Calendars.CalendarTimeMeasure import CalendarTimeMeasure
from Configurations import Configurations
from Greeks.Formatters.ResultsFormatter import ResultsFormatter
from Greeks.Runners.GreeksRunner import GreeksRunner
from MarketQuotations.Market import Market
from Parsing.ContractIdParser import ContractIdParser
from Parsing.OptionIdParser import OptionIdParser
from Smile.Dynamics.LogLinearVolatilityDynamicsModel import LogLinearVolatilityDynamicsModel
from Smile.Interpolation.CubicSplineImpliedVolatilityInterpolator import CubicSplineImpliedVolatilityInterpolator
from Smile.Representations.MoneynessSmileRepresentation import MoneynessSmileRepresentation
from Smile.Representations.SmileRepresentationConverter import SmileRepresentationConverter

from Products.AmericanOption import AmericanOption

if '__main__' == __name__:

        # ticker = sys.argv[1]
        # contract_id_str = sys.argv[2]
        # option_id_str = sys.argv[3]

        ticker = "lo"
        contract_id_str = "m17"
        option_id_str = "53p"

        # value_ date assumed to be previous business day if today is not a business day
        # value_date = (date.today() + BDay(1) - BDay(1)).date()
        value_date = date(2017,4,12)
        contract_id_parser = ContractIdParser()
        contract_id = contract_id_parser.parse(contract_id_str)
        contract_date = contract_id.to_date()
        option_id_parser = OptionIdParser()
        option_id = option_id_parser.parse(option_id_str)

        time_measure = CalendarTimeMeasure(value_date)
        duration = time_measure.to_duration(contract_date)

        dbConfig = Configurations.DataBase.DEFAULT_DB_CONFIG
        t1 = time.time()
        market_data = MarketDataRetriever(dbConfig).retrieve(value_date, contract_date, ticker)
        t2 = time.time()
        data_retrieval_time = (t2 - t1) * 1000.0
        moneyness_smile_representation = MoneynessSmileRepresentation(market_data.atm_vol, market_data.moneyness, market_data.skew_values)

        underlying_level =  market_data.underlying_level
        smile_interpolator = CubicSplineImpliedVolatilityInterpolator(moneyness_smile_representation, underlying_level, duration)
        atm_strike = SmileRepresentationConverter.moneyness_to_strike(underlying_level, 0.5, market_data.atm_vol, duration)
        option_vol = market_data.atm_vol
        option_strike = option_id.strike
        american_option = AmericanOption(option_strike, option_id.option_type, contract_date)
        steepness = smile_interpolator.slope_at_strike(atm_strike)

        market = Market(value_date, market_data)
        volatility_model = LogLinearVolatilityDynamicsModel(underlying_level, steepness)

        pricing_parameters = Configurations.Pricing.DEFAULT_PRICING_PARAMETERS
        greeks_definition = Configurations.Greeks.DEFAULT_GREEKS_DEFINITION

        greeks_runner = GreeksRunner(pricing_parameters, greeks_definition)
        greeks_result = greeks_runner.run(market, volatility_model, american_option)
        results_formatter = ResultsFormatter()

        t3 = time.time()
        pricing_time = (t3 - t2) * 1000.0
        print("############ PERF METRICS #########")
        print("")
        print("Retrieving data took {0} ms".format(data_retrieval_time))
        print("Pricing took {0} ms".format(pricing_time))
        print("")
        print("############ PRICE & GREEKS #########")
        print("")
        print (results_formatter.format(greeks_result))
        print("")


if '__main__' == __name__:

        # Reading args

        # In order to test
        # python functions.py 2017-03-14 50.0 0.01 0.3055 0.238 0.117 -0.110 -0.110 -0.25 50 P 2018-03-14 52.0

        help_string = []
        if (len(sys.argv) == 1):

            help_string.append("\n")
            help_string.append("1st arg: Value date in format yyyy-mm-dd. ex: 2017-03-14 \n")
            help_string.append("2nd arg: Reference underlying level. ex: 50.0\n")
            help_string.append("3rd arg: Annual interest rates. ex: 0.01 for a 1% annual rate\n")
            help_string.append("4th arg: At-The-Money volatility assuming that the ATM is equal to the underlying level mentioned earlier. ex: 0.3055 for a 30.55% volatility\n")
            help_string.append("5th arg: Skew level at 5P Delta. ex: 0.238 for a volatiliy at (1+0.238) * ATM at strike = 5P delta\n")
            help_string.append("6th arg: Skew level at 25P Delta. ex: 0.117 for a volatiliy at (1+0.117) * ATM at strike = 25P delta\n")
            help_string.append("7th arg: Skew level at 25C Delta. ex: -0.110 for a volatiliy at (1-0.110) * ATM at strike = 25C delta\n")
            help_string.append("8th arg: Skew level at 5C Delta. ex: -0.110 for a volatiliy at (1-0.110) * ATM at strike = 5C delta\n")
            help_string.append("9th arg: Steepness used to defined the dependency of ATM volatility on the underlying level.\n")
            help_string.append("10th arg: Option strike. ex: 50.0\n")
            help_string.append("11th arg: Option type. ex: P or C\n")
            help_string.append("12th arg: Option maturity in format yyyy-mm-dd. ex: 2018-03-14\n")
            help_string.append("13th arg: (Optional) Underlying level used for crossing. If not contributed will be set to the reference underlying level.\n")
            help_string.append("\n")
            help_string.append("Example:  python functions.py 2017-03-14 50.0 0.01 0.3055 0.238 0.117 -0.110 -0.110 -0.25 50 P 2018-03-14 52.0 \n")

            help_message = ''.join(help_string)
            print (help_message)

        else:

            value_date_str = sys.argv[1]
            underlying_level_str = sys.argv[2]
            interest_rate_str = sys.argv[3]

            atm_vol_str = sys.argv[4]
            skew_1_str = sys.argv[5]
            skew_2_str = sys.argv[6]
            skew_3_str = sys.argv[7]
            skew_4_str = sys.argv[8]

            steepness_str = sys.argv[9]

            strike_str = sys.argv[10]
            option_type_str = sys.argv[11]
            maturity_str = sys.argv[12]

            if (len(sys.argv) == 14):
                cross_level_str = sys.argv[13]
            else:
                cross_level_str = underlying_level_str

            # Parsing
            date_parsing_format = "%Y-%m-%d"
            value_date = datetime.datetime.strptime(value_date_str, date_parsing_format)
            underlying_level = float(underlying_level_str)
            interest_rate = float(interest_rate_str)
            atm_vol = float(atm_vol_str)
            skew_1 = float(skew_1_str)
            skew_2 = float(skew_2_str)
            skew_3 = float(skew_3_str)
            skew_4 = float(skew_4_str)
            strike = float(strike_str)

            if (str.upper(option_type_str) == "C"):
                option_type = OptionType.CALL
            elif (str.upper(option_type_str) == "P"):
                option_type = OptionType.PUT
            else:
                raise ValueError("Invalid option type {0}".format(option_type_str))

            maturity = datetime.datetime.strptime(maturity_str, date_parsing_format)
            steepness = float(steepness_str)
            cross_level = float(cross_level_str)

            # value_date = date(2017,3,19)
            # underlying_level = 50.43
            # interest_rate = 0.01
            # atm_vol = 0.2877
            # skew_1 = 0.221
            # skew_2 = 0.113
            # skew_3 = -0.100
            # skew_4 = -0.100
            # strike = 60

            # maturity = date(2017,11,15)
            # steepness = -0.25

            moneyness = np.array([0.05,0.25,0.5,0.75,0.95])
            skew_values = np.array([skew_1, skew_2, 0.0, skew_3, skew_4])
            constant_rate_factory = ConstantDiscountRateCurveFactory(value_date)
            discount_rate_curve = constant_rate_factory.build(datetime.timedelta(days=1), maturity, interest_rate)
            market = Market(value_date, MarketData(cross_level, atm_vol, moneyness, skew_values, discount_rate_curve))
            volatility_model = LogLinearVolatilityDynamicsModel(underlying_level, steepness)
            american_option = AmericanOption(strike, option_type, maturity)

            price(market, volatility_model, american_option)


