from enum import Enum


class OptionType(Enum):
    """ Enum to represent the different types of an option.

    """
    CALL = 1
    PUT = 2