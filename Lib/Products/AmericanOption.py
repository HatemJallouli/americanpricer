from datetime import date

from Products.OptionType import OptionType


class AmericanOption:
    """ Class to represent the notion of an american option.
    
        Parameters
        ----------
        strike : float
            The exercise price of the american option.
        option_type : :class:`~Products.OptionType`
            The type of the option that can be :attr:`~Products.OptionType.CALL` and :attr:`~Products.OptionType.PUT`
        expiry_date : date
            The maturity of the american option
        
        Examples
        --------
        >>> from Products.AmericanOption import AmericanOption
        >>> from Products.OptionType import OptionType
        >>> from datetime import date
        >>> strike = 50.0
        >>> option_type = OptionType.CALL
        >>> expiry_date = date(2018,12,1)
        >>> american_option = AmericanOption(strike, option_type, expiry_date)
        >>> print(american_option)
        American option: OptionType.CALL, strike 50.0, expiry 2018-12-01

    """
    def __init__(self, strike: float, option_type: OptionType, expiry_date: date):
        self.__strike = strike
        self.__option_type = option_type
        self.__expiry_date = expiry_date

    @property
    def strike(self):
        return self.__strike

    @property
    def option_type(self):
        return self.__option_type

    @property
    def expiry_date(self):
        return self.__expiry_date

    def __str__(self):
        return "American option: {0}, strike {1}, expiry {2}".format(self.option_type, self.strike, self.expiry_date)

if __name__ == "__main__":
    import doctest
    doctest.testmod()