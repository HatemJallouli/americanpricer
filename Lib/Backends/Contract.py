from datetime import date


class Contract:
    """ Container class representing the notion of a futures contract.

        Parameters
        ----------
        index_name : str
            Name of the index.
        contract_date : date
            Maturity of the future contract. 
        
        Examples
        --------
        >>> from datetime import date
        >>> from Backends.Contract import Contract
        >>> contract = Contract("WTI", date(2017,12,1))
        >>> print(contract)
        Contract: WTI Dec-17
        
    """

    def __init__(self, index_name : str, contract_date : date):
        self.__index_name = index_name
        self.__contract_date = contract_date

    @property
    def index_name(self):
        return self.__index_name

    @property
    def contract_date(self):
        return self.__contract_date

    def __str__(self):
        return "Contract: {0} {1}".format(self.index_name, self.contract_date.strftime("%b-%y"))

