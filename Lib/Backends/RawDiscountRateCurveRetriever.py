from datetime import date
from typing import Dict

import numpy as np
from Backends import MySqlConnect as mysqlconn
from Backends.DatabaseConfig import DatabaseConfig
from Calendars.CalendarTimeMeasure import CalendarTimeMeasure
from Rates.RawDiscountRateCurve import RawDiscountRateCurve

from Rates.RateCurveId import RateCurveId


class RawDiscountRateCurveRetriever:
    """ Class to retrieve the rate discount curve corresponding to the USD-LIBOR.

        Parameters
        ----------
        db_config : :class:`~Backends.DatabaseConfig`
            Database connexion configuration.
    """

    def __init__(self, db_config: DatabaseConfig):
        self.__db_config = db_config
        self.__date_format = "%Y-%m-%d"

    def retrieve(self, value_date: date) -> RawDiscountRateCurve:
        """ Retrieve american option quotations and underlying level for a given value date.

            Parameters
            ----------
            value_date : date
                Date of the quotation of the rate curve.

            Returns
            -------
            raw_discount_curve : :class:`~Rates.RawDiscountRateCurve`
                The raw discount (without interpolation) corresponding to the contributed pillars in the database.

            Examples
            --------
            >>> from Backends.RawDiscountRateCurveRetriever import RawDiscountRateCurveRetriever
            >>> from Configurations import Configurations
            >>> from datetime import date
            >>> raw_discount_rate_curve_retriever = RawDiscountRateCurveRetriever(Configurations.DataBase.DEFAULT_DB_CONFIG)
            >>> value_date = date(2017,3,20)
            >>> data = raw_discount_rate_curve_retriever.retrieve(value_date)
            >>> print("Rate Curve: {0}, Duration: {1}, Discount: {2}".format(data.rate_curve_id, data.durations[2], data.discount_factors[2]))
            Rate Curve: USD-LIBOR, Duration: 0.2493150684931507, Discount: 0.9969360408558448
        """

        #hardcoded curve for now - next step take it as an input when db is ready
        rate_curve_id = RateCurveId("USD-LIBOR")
        value_date_str = value_date.strftime(self.__date_format)
        query = ("SELECT "
                 "    yc.fwddate, "
                 "    yc.discfac "
                 "FROM"
                 "    marketdata.yieldcurve yc "
                 "WHERE "
                 "    valdate = '{0}' AND fwddate > valdate"
                 .format(value_date_str))

        data = mysqlconn.getDataQuery(query=query, config=self.__db_config.to_dict())
        data = data.sort_values(by="fwddate")
        maturities = data["fwddate"].as_matrix()
        discount_factors = data["discfac"].as_matrix()
        time_measure = CalendarTimeMeasure(value_date)
        durations = np.zeros(len(maturities))
        for i in range (len(maturities)):
            durations[i] = time_measure.to_duration(maturities[i])

        return RawDiscountRateCurve(rate_curve_id, durations, discount_factors)


if __name__ == "__main__":
    import doctest
    doctest.testmod()