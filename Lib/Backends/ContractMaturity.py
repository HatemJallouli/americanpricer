from datetime import date


class ContractMaturity:
    """ Container class representing the notion of a futures contract maturity.

        Parameters
        ----------
        month : int
            Month index in the year.
        year : int
            Year of the maturity of the contract. 

        Examples
        --------
        >>> from Backends.ContractMaturity import ContractMaturity
        >>> contract_maturity = ContractMaturity(6,2018)
        >>> print(contract_maturity)
        Contract Maturity: Jun-18

    """

    def __init__(self, month : int, year : int):
        self.__month = month
        self.__year = year

    @property
    def month(self):
        return self.__month

    @property
    def year(self):
        return self.__year

    def to_date(self) -> date:
        """ Convert the contract maturity to a date.

            Returns
            -------
            d : date
                Date representation of the maturity assuming that it is on the first day of the month.
 
            Examples
            --------
            >>> from Backends.ContractMaturity import ContractMaturity
            >>> contract_maturity = ContractMaturity(6,2018)
            >>> contract_maturity.to_date()
            datetime.date(2018, 6, 1)

        """
        return date(self.__year, self.__month, 1)

    def __str__(self):
        return "Contract Maturity: {0}".format(self.to_date().strftime("%b-%y"))

if __name__ == "__main__":
    import doctest
    doctest.testmod()