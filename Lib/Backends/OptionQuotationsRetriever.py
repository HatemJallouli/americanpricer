import collections
from datetime import date
from typing import Dict, List, Tuple

import numpy as np
from Products.AmericanOption import AmericanOption
from Products.OptionType import OptionType
from Smile.Implicitation.OptionQuotation import OptionQuotation

from Backends import MySqlConnect as mysqlconn
from Backends.Contract import Contract
from Backends.DatabaseConfig import DatabaseConfig


class OptionQuotationsRetriever:
    """ Class to retrieve american option quotations and the underlying level for smile implicitation.

            Parameters
            ----------
            db_config : :class:`~Backends.DatabaseConfig`
                Database connexion configuration.

    """

    def __init__(self, db_config: DatabaseConfig):
        self.__db_config = db_config
        self.__date_format = "%Y-%m-%d"
        self.__price_threshold = 0.01

    def retrieve_for_dates(self, start_value_date: date, end_value_date: date, contract : Contract) -> Dict[date,Tuple[float, List[OptionQuotation]]]:
        """ Retrieve american option quotations and underlying level for a range of value dates.

            Parameters
            ----------
            start_value_date : date
                First value date (inclusive): quotations for dates after this date will be included.
            end_value_date : date
                Last value date (inclusive): quotations for dates before this date will be included.
            contract : :class:`~Backends.Contract`
                Underlying futures contract.
                
            Returns
            -------
            option_quotations : Dict[date,Tuple[float, List[:class:`~Smile.Implicitation.OptionQuotation`]]]
                Mapping of value dates to a tuple (Underlying level, Option quotations)
                    
            Examples
            --------
            >>> from Backends.OptionQuotationsRetriever import OptionQuotationsRetriever
            >>> from Backends.Contract import Contract
            >>> from Configurations import Configurations
            >>> from datetime import date
            >>> option_quotations_retriever = OptionQuotationsRetriever(Configurations.DataBase.DEFAULT_DB_CONFIG)
            >>> start_value_date = date(2017,3,14)
            >>> end_value_date = date(2017,3,20)
            >>> contract = Contract("GOLD", date(2017,12,1))
            >>> data = option_quotations_retriever.retrieve_for_dates(start_value_date, end_value_date, contract)
            >>> date = date(2017,3,17)
            >>> data_for_date = data[date]
            >>> print("Underlying Level: {0}, Option Quotation: {1}".format(data_for_date[0], data_for_date[1][0]))
            Underlying Level: 1243.0, Option Quotation: OptionType.CALL : Strike = 650.0, Maturity = 2017-11-27, Quote = 593.0 @ 2017-03-17

        """

        start_value_date_str = start_value_date.strftime(self.__date_format)
        end_value_date_str = end_value_date.strftime(self.__date_format)
        contract_date_str = contract.contract_date.strftime(self.__date_format)
        contract_index_name = contract.index_name
        query = ("SELECT "
                 "    p.date, "
                 "    p.instrument_id, "
                 "    p.strike, "
                 "    p.pc, "
                 "    e.date as expirationdate, "
                 "    p.close "
                 "FROM"
                 "    marketdata.prices p "
                 "        JOIN "
                 "    marketdata.underlyings u ON u.id = p.underlying_id "
                 "        JOIN "
                 "   marketdata.contracts c ON c.id = p.contract_id "
                 "        JOIN "
                 "    marketdata.expirations e ON (e.instrument_id = p.instrument_id "
                 "        AND c.id = e.contract_id "
                 "        AND e.underlying_id = u.id) "
                 "WHERE "
                 "    p.date >= '{0}' AND p.date <= '{1}' AND u.name = '{2}' "
                 "        AND c.date = '{3}' "
                 .format(start_value_date_str, end_value_date_str, contract_index_name, contract_date_str))

        data = mysqlconn.getDataQuery(query=query, config=self.__db_config.to_dict())
        data = data.sort_values(by="date")
        value_dates = data["date"].astype(date).unique()
        result = dict()
        for value_date in value_dates:
            value_date = date(value_date.year, value_date.month, value_date.day)
            data_by_date = data[data["date"] == value_date]
            data_by_date = data_by_date.sort_values(by="strike")
            underlying_level_data = data_by_date[data_by_date["instrument_id"]==1]
            option_data = data_by_date[data_by_date["instrument_id"]==2]

            underlying_level = underlying_level_data["close"].iloc[0]

            option_quotations = []

            for index, row in option_data.iterrows():

                strike = row[2]

                if str.upper(row[3]) == "C":
                    option_type = OptionType.CALL
                elif str.upper(row[3]) == "P":
                    option_type = OptionType.PUT
                else:
                    raise ValueError("Option type should be either {0} or {1} but was {2}".format("C", "P", row[2]))

                expiry_date = row[4]
                quote_value = row[5]

                american_option = AmericanOption(strike,option_type, expiry_date )
                option_quotation = OptionQuotation(value_date, american_option, quote_value)
                option_quotations.append(option_quotation)

            result[value_date] = (underlying_level, np.array(option_quotations))

        return collections.OrderedDict(sorted(result.items()))

    def retrieve_for_date(self, value_date: date, contract : Contract) -> Tuple[float, List[OptionQuotation]]:
        """ Retrieve american option quotations and underlying level for a given value date.

                Parameters
                ----------
                value_date : date
                    Date of the quotation of the option.
                contract : :class:`~Backends.Contract`
                    Underlying futures contract.

                Returns
                -------
                option_quotations : Tuple[float, List[:class:`~Smile.Implicitation.OptionQuotation`]]
                    Tuple (Underlying level, Option quotations)

                Examples
                --------
                >>> from Backends.OptionQuotationsRetriever import OptionQuotationsRetriever
                >>> from Backends.Contract import Contract
                >>> from Configurations import Configurations
                >>> from datetime import date
                >>> option_quotations_retriever = OptionQuotationsRetriever(Configurations.DataBase.DEFAULT_DB_CONFIG)
                >>> value_date = date(2017,3,17)
                >>> contract = Contract("GOLD", date(2017,12,1))
                >>> data = option_quotations_retriever.retrieve_for_date(value_date, contract)
                >>> print("Underlying Level: {0}, Option Quotation: {1}".format(data[0], data[1][0]))
                Underlying Level: 1243.0, Option Quotation: OptionType.PUT : Strike = 650.0, Maturity = 2017-11-27, Quote = 0.1 @ 2017-03-17

        """

        return self.retrieve_for_dates(value_date, value_date, contract)[value_date]

if __name__ == "__main__":
    import doctest
    doctest.testmod()



