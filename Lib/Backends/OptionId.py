from Products.OptionType import OptionType


class OptionId:
    """ Container class representing the parameters of an option.

        Parameters
        ----------
        strike : float
            Strike of the option.
        option_type : :class:`~Products.OptionType`
            Type of the option, can be :attr:`~Products.OptionType.CALL` or :attr:`~Products.OptionType.PUT`. 
                
        Examples
        --------
        >>> from Backends.OptionId import OptionId
        >>> from Products.OptionType import OptionType
        >>> option_id = OptionId(95.0, OptionType.CALL)
        >>> print(option_id)
        Option Id: 95.0-OptionType.CALL

    """

    def __init__(self, strike : float, option_type : OptionType):
        self.__strike = strike
        self.__option_type = option_type

    @property
    def strike(self):
        return self.__strike

    @property
    def option_type(self):
        return self.__option_type

    def __str__(self):
        return "Option Id: {0}-{1}".format(self.strike, self.option_type)

if __name__ == "__main__":
    import doctest
    doctest.testmod()
