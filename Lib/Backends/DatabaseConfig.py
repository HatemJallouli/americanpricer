class DatabaseConfig:
    """ Container class representing the configuration for a SQL database connexion.

        Parameters
        ----------
        user : str
            The username used for the connexion credentials.
        password : str
            The password used for the connexion credentials.
        host : str
            The host of the connexion 
        port : int
            The port used for the connexion
        database : str
            The name of the database one wishes to access
    """
    def __init__(self, user : str, password: str, host: str, port: int, database: str):
        self.__user = user
        self.__password = password
        self.__host = host
        self.__port = port
        self.__database = database

    def to_dict(self):
        """ Method to convert the instance in a dictionary format.

            Examples
            --------
            >>> from Backends.DatabaseConfig import DatabaseConfig
            >>> db_config = DatabaseConfig("user9715","A21u?cs","localhost", 22, "MyDataBase")
            >>> dict = db_config.to_dict()
            >>> print(dict['user'])
            user9715
            >>> print(dict['password'])
            A21u?cs
            >>> print(dict['port'])
            22
        """
        result = dict()
        result['user'] = self.__user
        result['password'] = self.__password
        result['host'] = self.__host
        result['port'] = str(self.__port)
        result['database'] = self.__database
        result['raise_on_warnings'] = True
        return result

if __name__ == "__main__":
    import doctest
    doctest.testmod()