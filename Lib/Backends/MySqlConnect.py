#!/usr/bin/python

import mysql.connector as mysqlconn
import pandas as pd
import datetime as dt

cnxConfig = {'user': 'username', 'password': 'userpwd', 
             'host': 'localhost', 'port': '3307',
             'database': 'marketdata', 'raise_on_warnings': True}

usrErrMsg = "Both user & password must be specified in the config args!"
class MySqlUserErr(Exception):
    pass

def getDataQuery(query, dfoutput=True, config=cnxConfig):
    """ 
        --------------------------------------------------
        --      Get data by executing MySql query       --
        --------------------------------------------------
        
        ARGUMENTS:
        
        #################################################################
        ##   NAME             TYPE              DESCRIPTION            ##
        #################################################################
        #    query           string     MySql query to be executed      #
        #---------------------------------------------------------------#
        #    dfoutput        boolean    ouput as a pandas' dataframe    #
        #---------------------------------------------------------------#
        #    config          dict       mysql connection parameters     #
        #################################################################
        
        RETURNS:
            
        If dfoutput == True:
            A panda's dataframe containing the datas with the fields as 
            column names.
        else:
            Two lists:
                - data  <-- list containing the data
                - flds  <-- List containing the names of the fields
                
        EXAMPLE:
                
        As an example, let's consider that we want to fetch all the data 
        contained in the Table 'underlyings' in the database 'marketdata'.
        
        To run this query, we would specify the parameters as follow:  
            
        query = "SELECT * FROM marketdata.underlyings;"
        
        dfoutput = True
        
        config = {'user': 'username', 'password': 'userpwd', 
                  'host': 'localhost', 'port': '3317', 
                  'database': 'marketdata', 'raise_on_warnings': True}
        
    """ 
    if config['user']=='username' or config['password']=='userpwd': 
        raise MySqlUserErr(usrErrMsg)
    try:
        # Connect to db and fetch data
        conn = mysqlconn.connect(**config)
        cur = conn.cursor(buffered=True)
        cur.execute(query)
        data = []
        flds = [t[0] for t in cur.description] 
        for row in cur.fetchall():     
            data.append(tuple(row))
 
    except Exception as e:
        raise e
 
    finally:
        if 'cur' in vars(): cur.close()
        if 'conn' in vars(): conn.close()
        if 'data' in vars():
            if dfoutput==True:
                return pd.DataFrame.from_records(data,columns=flds)
            else:
                return data, flds
    
def getDataProc(proc, param=(), dfoutput=True, config=cnxConfig):
    """ 
        --------------------------------------------------
        --  Get data by calling MySql stored procedure  --
        --------------------------------------------------
        
        ARGUMENTS:
        
        #################################################################
        ##   NAME             TYPE              DESCRIPTION            ##
        #################################################################
        #    proc            string     stored procedure's name         #
        #---------------------------------------------------------------#
        #    param           tuple      stored procedure's parameters   #
        #---------------------------------------------------------------#
        #    dfoutput        boolean    ouput as a pandas' dataframe    #
        #---------------------------------------------------------------#
        #    config          dict       mysql connection parameters     #
        #################################################################
        
        RETURNS:
            
        If dfoutput == True:
            A panda's dataframe containing the datas with the fields as 
            column names.
        else:
            Two lists:
                - data  <-- list containing the data
                - flds  <-- List containing the names of the fields
                
        EXAMPLE:
                
        As an example, let's consider the stored procedure getHistoFutSMile() 
        that we want to use to fetch data for a specific underlying and 
        specific term structure's line(s) from the database 'marketdata'.
        
        To call getHistoFutSMile(), we would specify the parameters as follow:  
            
        proc = "getHistoFutSMile"
        
        param = (underlyingId, iniDateTime, endDateTime, 
                 priceTypeID, firstLine, lastLine)
        
          with: underlyingId (int)      <-- id of the underlying market 
                iniDateTime (str)       <-- Start Date/Time
                endDateTime (str)       <-- End Date/Time    
                priceTypeID (int)       <-- 1: 'Exchange settlement'
                                            2: '2:30 settlement' 
                                            3: 'intraday'
                firstLine (int)         <-- First line to take into account
                lastLine (int)          <-- Last line to take into account
        
        dfoutput = True
        
        config = {'user': 'username', 'password': 'userpwd', 
                  'host': 'localhost', 'port': '3317', 
                  'database': 'marketdata', 'raise_on_warnings': True}
        
    """
    if config['user']=='username' or config['password']=='userpwd': 
        raise MySqlUserErr(usrErrMsg)    
    try:
        conn = mysqlconn.connect(**config)
        cur = conn.cursor(buffered=True)
        cur.callproc(proc, param)
        data = []
        for res in cur.stored_results():
            flds = [t[0] for t in res.description] 
            for row in res.fetchall():     
                data.append(tuple(row))
             
    except Exception as e:
        raise e
 
    finally:
        if 'cur' in vars(): cur.close()
        if 'conn' in vars(): conn.close()
        if 'data' in vars():
            if dfoutput==True:
                return pd.DataFrame.from_records(data,columns=flds)
            else:
                return data, flds
 
 
if __name__ == '__main__':
    # Specify MySql username and password here
    cnxConfig['user'] = 'hatem'
    cnxConfig['password'] = 'H@atempass'
    # Example of getDataQuery call
    query = "SELECT * FROM marketdata.underlyings;"
    queryResults = getDataQuery(query)
    print("Example of results for getDataQuery() \n")
    print(queryResults)
    # Example of getDataProc call
    storedProc = 'getHistoFutSMile'
    mkt_id = 1
    endDte = dt.date.today()
    iniDte = endDte.replace(year=endDte.year-1) # endDte - 1 year
    ptype_id = 1
    fstLine = 6
    lastLine = 6
    procResults = getDataProc(storedProc, (mkt_id, str(iniDte), str(endDte), 
                                                 ptype_id, fstLine, lastLine))
    print("Example of results for getDataProc() \n")
    print(procResults)
    
    