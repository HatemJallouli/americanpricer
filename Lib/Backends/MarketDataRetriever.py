from datetime import date

import numpy as np

from Backends.DatabaseConfig import DatabaseConfig
from Backends.RawDiscountRateCurveRetriever import RawDiscountRateCurveRetriever
from MarketQuotations.MarketData import MarketData

from Backends import MySqlConnect as mysqlconn


class MarketDataRetriever:
    """ Class to retrieve market data from a database.

        Parameters
        ----------
        db_config : :class:`~Backends.DatabaseConfig`
            Database connexion configuration.

    """

    def __init__(self, db_config : DatabaseConfig):
        self.__db_config = db_config
        self.__date_format = "%Y-%m-%d"

    def retrieve(self, value_date : date, contract_date : date, ticker : str) -> MarketData:
        """ Retrieve market data from the database.

            Parameters
            ----------
            value_date : date
                Quotation date of the market data.
            contract_date : date
                Maturity of the futures contract.
            ticker : str
                Ticker of the futures contract.
                
            Returns
            -------
            market_data : :class:`~MarketQuotations.MarketData`
                Container class of the market data 
                    
            Examples
            --------
            >>> from Backends.MarketDataRetriever import MarketDataRetriever
            >>> from Configurations import Configurations
            >>> from datetime import date
            >>> market_data_retriever = MarketDataRetriever(Configurations.DataBase.DEFAULT_DB_CONFIG)
            >>> value_date = date(2017,3,14)
            >>> contract_date = date(2017,12,1)
            >>> ticker = "lo"
            >>> market_data = market_data_retriever.retrieve(value_date, contract_date, ticker)
            >>> print("Underlying Level: {0}, Atm Volatility {1}".format(market_data.underlying_level, market_data.atm_vol))
            Underlying Level: 49.98, Atm Volatility 0.30798

        """

        raw_discount_rate_curve = RawDiscountRateCurveRetriever(self.__db_config).retrieve(value_date)
        value_date_str = value_date.strftime(self.__date_format)
        contract_date_str = contract_date.strftime(self.__date_format)
        query = ("SELECT "
                "d.flatprc, d.atmvol, d.skew1, d.skew2, d.skew3, d.skew4, t.ticker_exchange "
                "FROM "
                "marketdata.deltavols_tension d "
                "JOIN marketdata.contracts c ON d.contract_id = c.id "
                "JOIN marketdata.underlyings u ON d.underlying_id = u.id "
                "JOIN marketdata.tickers t ON t.underlying_id = d.underlying_id "
                "WHERE "
                "d.publish_type_id = 1 AND d.date = '{0}' AND t.ticker_exchange = \"{1}\" AND c.date = '{2}'"
                 .format(value_date_str, ticker, contract_date_str))

        data = mysqlconn.getDataQuery(query=query, config=self.__db_config.to_dict())
        underlying_level = data["flatprc"][0]
        atm_vol = data["atmvol"][0]
        skew_1 = data["skew1"][0]
        skew_2 = data["skew2"][0]
        skew_3 = data["skew3"][0]
        skew_4 = data["skew4"][0]
        # ticker = data["ticker_exchange"][0]
        moneyness = np.array([0.05, 0.25, 0.5, 0.75, 0.95])
        skew_values = np.array([skew_1, skew_2, 0, skew_3, skew_4])
        return MarketData(underlying_level, atm_vol, moneyness, skew_values, raw_discount_rate_curve)

if __name__ == "__main__":
    import doctest
    doctest.testmod()
