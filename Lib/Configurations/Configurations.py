from Backends.DatabaseConfig import DatabaseConfig
from Greeks.Common.GreeksDefinition import GreeksDefinition
from Pricing.PV.FirstGuessMethod import FirstGuessMethod
from Pricing.PV.PricingParameters import PricingParameters

from Pricing.PV.IterationType import IterationType


class Pricing:


    DEFAULT_PRICING_PARAMETERS = PricingParameters(
        n=5,
        l=9,
        m=3,
        p=21,
        first_guess_method=FirstGuessMethod.TRIVIAL,
        iteration_type=IterationType.PARTIAL_NEWTON_JACOBI)

class Greeks:


    DEFAULT_GREEKS_DEFINITION = GreeksDefinition(
        delta_relative_bump_size=0.1,
        gamma_relative_bump_size=1.5,
        vega_bump_size= 0.01,
        theta_in_days_bump_size= 1,
        rho_bump_size= 0.0001,
        volga_bump_size= 0.01,
        smile_risk_bump_size=0.01
        )

class DataBase:

    DEFAULT_DB_CONFIG = DatabaseConfig(
        'hatem',
        'H@tempass',
        'localhost',
        3307,
        'marketdata'
    )