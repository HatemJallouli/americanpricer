import numpy as np
from Backends.Contract import Contract
from Configurations import Configurations
from Greeks.Formatters.ResultsFormatter import ResultsFormatter
from Greeks.Runners.GreeksRunner import GreeksRunner
from MarketQuotations.Market import Market
from Products.AmericanOption import AmericanOption
from Smile.Dynamics.IVolatilityDynamicsModel import IVolatilityDynamicsModel
from Smile.Representations.MoneynessSmileRepresentation import MoneynessSmileRepresentation

from Smile.Implicitation.MoneynessSmileRepresentationBuilder import MoneynessSmileRepresentationBuilder


def price(market : Market, volatility_model : IVolatilityDynamicsModel, american_option : AmericanOption) -> None:

    pricing_parameters = Configurations.Pricing.DEFAULT_PRICING_PARAMETERS
    greeks_definition = Configurations.Greeks.DEFAULT_GREEKS_DEFINITION
    greeks_runner = GreeksRunner(pricing_parameters, greeks_definition)
    greeks_result = greeks_runner.run(market, volatility_model, american_option)
    results_formatter = ResultsFormatter()
    print("############ PRICE & GREEKS #########")
    print("")
    print(results_formatter.format(greeks_result))
    print("")


def build_smile(value_date, index_name, contract_date) -> MoneynessSmileRepresentation:
    db_config = Configurations.DataBase.DEFAULT_DB_CONFIG
    pricing_parameters = Configurations.Pricing.DEFAULT_PRICING_PARAMETERS
    moneynesses = np.array([0.05, 0.25, 0.5, 0.75, 0.95])
    contract = Contract(index_name, contract_date)
    smile_builder = MoneynessSmileRepresentationBuilder(db_config, pricing_parameters, moneynesses)

    return smile_builder.build(value_date, contract)