class MarketStateKey:
    """ Container class to identify a market state in a greeks run.

        Parameters
        ----------
        id : str
            String used to identify the market state.

        Examples
        --------
        >>> from Common.MarketStateKey import MarketStateKey
        >>> market_state_key = MarketStateKey("Delta_Up")
        >>> print(market_state_key)
        Delta_Up

    """

    def __init__(self, id):
        self.__id = id

    @property
    def id(self):
        return self.__id

    def __hash__(self):
        return hash(self.__id)

    def __eq__(self, other):
        return self.__id == other.__id

    def __ne__(self, other):
        return not (self == other)

    def __str__(self):
        return self.__id

if __name__ == "__main__":
    import doctest
    doctest.testmod()