class ResultKey:
    """ General container class to identify a computation result.

        Parameters
        ----------
        id : str
            String used to identify the market state.
        
        See Also
        --------
        MarketDataKey: :class:`~Common.MarketDataKey`
        GreekKey: :class:`~Greeks.Common.GreekKey`
        PriceKey: :class:`~Pricing.Common.PriceKey`
    """
    def __init__(self, id):
        self.__id = id

    @property
    def id(self):
        return self.__id

    def __hash__(self):
        return hash(self.__id)

    def __eq__(self, other):
        return self.__id == other.__id and isinstance(other, ResultKey)

    def __ne__(self, other):
        return not (self == other)

    def __str__(self):
        return self.__id


