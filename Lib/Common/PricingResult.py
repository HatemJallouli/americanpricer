class PricingResult:
    """ General container class containing computation result.

        Parameters
        ----------
        price : float
            The price of the option.
        underlying_level : float
            The underlying level that was used to compute the price
        atm_vol : float
            The at-the-money volatility used when pricing.
        bs_vol : float
            The effective volatility used when calling the Black-Scholes pricer
            
    """

    def __init__(self, price: float, underlying_level : float, atm_vol : float, bs_vol :float):
        self.__price = price
        self.__underlying_level = underlying_level
        self.__atm_vol = atm_vol
        self.__bs_vol = bs_vol


    @property
    def price(self):
        return self.__price

    @property
    def underlying_level(self):
        return self.__underlying_level

    @property
    def atm_vol(self):
        return self.__atm_vol

    @property
    def bs_vol(self):
        return self.__bs_vol