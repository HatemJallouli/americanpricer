from Common.MarketDataKey import MarketDataKey
from Greeks.Common.GreekKey import GreekKey

from Pricing.Common.PriceKey import PriceKey


class ResultKeys:
    """ Static class regrouping the different market state keys used for greeks computation.

        Examples
        --------
        >>> from Common.ResultKeys import ResultKeys
        >>> delta = ResultKeys.delta
        >>> print(delta)
        Delta
        >>> price = ResultKeys.price
        >>> print(price)
        Price
        >>> underlying_level = ResultKeys.underlying_level
        >>> print(underlying_level)
        UnderlyingLevel

    """

    delta = GreekKey("Delta")
    gamma = GreekKey("Gamma")
    vega = GreekKey("Vega")
    vanna = GreekKey("Vanna")
    volga = GreekKey("Volga")
    theta = GreekKey("Theta")
    rho = GreekKey("Rho")
    price = PriceKey("Price")
    underlying_level = MarketDataKey("UnderlyingLevel")
    atm_vol = MarketDataKey("AtmVol")
    option_vol = MarketDataKey("OptionVol")

    @staticmethod
    def smile_risk(moneyness: float) -> GreekKey:
        return GreekKey("Smile_Risk_{0}".format(str(moneyness)))

if __name__ == "__main__":
    import doctest
    doctest.testmod()