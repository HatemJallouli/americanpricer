from Common.ResultKey import ResultKey


class MarketDataKey(ResultKey):
    """Container class to identify a market data.

        Parameters
        ----------
        id : str
            String used to identify the market data.
            
        Examples
        --------
        >>> from Common.MarketDataKey import MarketDataKey
        >>> market_data_key = MarketDataKey("Volatility")
        >>> print(market_data_key)
        Volatility

    """
    def __init__(self, id):
        super(MarketDataKey, self).__init__(id)

    @property
    def id(self):
        return super().id

    def __hash__(self):
        return super().__hash__()

    def __eq__(self, other):
        return super().__eq__(other) and isinstance(other, MarketDataKey)

    def __ne__(self, other):
        return not (self == other)

    def __str__(self):
        return super().__str__()

if __name__ == "__main__":
    import doctest
    doctest.testmod()