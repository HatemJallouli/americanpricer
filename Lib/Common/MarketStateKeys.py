from Common.MarketStateKey import MarketStateKey

class MarketStateKeys:
    """ Static class regrouping the different market state keys used for greeks computation.

        Examples
        --------
        >>> from Common.MarketStateKeys import MarketStateKeys
        >>> delta_up_key = MarketStateKeys.delta_up
        >>> print(delta_up_key)
        Delta_Up
        >>> gamma_up_up_key = MarketStateKeys.gamma_up_up
        >>> print(gamma_up_up_key)
        Gamma_Up_Up
        >>> smile_risk_25_up = MarketStateKeys.smile_risk_up(0.25)
        >>> print(smile_risk_25_up)
        Smile_Risk_0.25_Up

    """

    # No deformation State
    price = MarketStateKey("Price")

    # Delta Scheme Definition
    delta_up = MarketStateKey("Delta_Up")
    delta_down = MarketStateKey("Delta_Down")

    # Gamma Scheme Definition
    gamma_up_up = MarketStateKey("Gamma_Up_Up")
    gamma_up_down = MarketStateKey("Gamma_Up_Down")
    gamma_down_up = MarketStateKey("Gamma_Down_Up")
    gamma_down_down = MarketStateKey("Gamma_Down_Down")

    # Vega Scheme Definition
    vega_up = MarketStateKey("Vega_Up")
    vega_down = MarketStateKey("Vega_Down")

    # Theta Scheme Definition
    theta_up = MarketStateKey("Theta_Up")

    # Rho Scheme Definition
    rho_up = MarketStateKey("Rho_Up")

    # Vanna Scheme Definition
    vanna_delta_up_vega_up = MarketStateKey("Vanna_Delta_Up_Vega_Up")
    vanna_delta_up_vega_down = MarketStateKey("Vanna_Delta_Up_Vega_Down")
    vanna_delta_down_vega_up = MarketStateKey("Vanna_Delta_Down_Vega_Up")
    vanna_delta_down_vega_down = MarketStateKey("Vanna_Delta_Down_Vega_Down")

    # Volga Scheme Definition
    volga_up_up = MarketStateKey("Volga_Up_Up")
    volga_up_down = MarketStateKey("Volga_Up_Down")
    volga_down_up = MarketStateKey("Volga_Down_Up")
    volga_down_down = MarketStateKey("Volga_Down_Down")

    # Smile Definition

    @staticmethod
    def smile_risk_up(moneyness : float) -> MarketStateKey:
        return MarketStateKey("Smile_Risk_{0}_Up".format(str(moneyness)))

    @staticmethod
    def smile_risk_down(moneyness : float) -> MarketStateKey:
        return MarketStateKey("Smile_Risk_{0}_Down".format(str(moneyness)))

if __name__ == "__main__":
    import doctest
    doctest.testmod()