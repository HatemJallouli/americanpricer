from datetime import date, timedelta

import numpy as np
import pytest
from Backends.Contract import Contract
from Configurations import Configurations
from Pricing.PV.ExerciceBoundaryIntegrationBlackScholesAmericanOptionPricer import ExerciceBoundaryIntegrationBlackScholesAmericanOptionPricer
from Products.AmericanOption import AmericanOption
from Products.OptionType import OptionType
from Rates.ConstantDiscountRateCurveFactory import ConstantDiscountRateCurveFactory
from Rates.DiscountRateCurveInterpolationFactory import DiscountRateCurveInterpolationFactory
from Smile.Implicitation.AmericanImpliedVolatilitySolver import AmericanImpliedVolatilitySolver
from Smile.Implicitation.OptionQuotation import OptionQuotation

from Lib.Smile.Implicitation.MoneynessSmileRepresentationBuilder import MoneynessSmileRepresentationBuilder


@pytest.fixture(scope = "module")
def vol_results_setup():

    value_date = date(2017, 3, 14)
    contract_date = date(2017,12,1)
    index_name = "WTI"

    db_config = Configurations.DataBase.DEFAULT_DB_CONFIG
    pricing_parameters = Configurations.Pricing.DEFAULT_PRICING_PARAMETERS
    moneynesses = np.array([0.05, 0.25, 0.5, 0.75, 0.95])
    contract = Contract(index_name, contract_date)
    smile_builder = MoneynessSmileRepresentationBuilder(db_config, pricing_parameters, moneynesses)

    return smile_builder.build_for_date(value_date, contract)


@pytest.fixture(scope = "module")
def vol_results_setup_many_dates():

    start_value_date = date(2017, 1, 15)
    end_value_date = date(2017,1,20)
    contract_date = date(2017,12,1)
    index_name = "WTI"

    db_config = Configurations.DataBase.DEFAULT_DB_CONFIG
    pricing_parameters = Configurations.Pricing.DEFAULT_PRICING_PARAMETERS
    moneynesses = np.array([0.05, 0.25, 0.5, 0.75, 0.95])
    contract = Contract(index_name, contract_date)
    smile_builder = MoneynessSmileRepresentationBuilder(db_config, pricing_parameters, moneynesses)

    return smile_builder.build_for_dates(start_value_date,end_value_date, contract)

@pytest.fixture(scope = "module")
def vol_solver_setup():
    value_date = date(2017,3,14)
    underlying_level = 57.0
    interest_rate = 0.01
    raw_curve = ConstantDiscountRateCurveFactory(value_date).build(timedelta(days=3), date(2018,3,14), interest_rate)
    interpolator = DiscountRateCurveInterpolationFactory.log_cubic_interpolation(value_date, raw_curve)
    implied_vol_solver = AmericanImpliedVolatilitySolver(underlying_level, interpolator, Configurations.Pricing.DEFAULT_PRICING_PARAMETERS)
    return implied_vol_solver, value_date, underlying_level, interest_rate

def test_vol_implicitation_many_dates(vol_results_setup_many_dates):
    try :
        x = vol_results_setup_many_dates
    except:
        pytest.fail("Implicitation failed")

def test_atm_vol(vol_results_setup):
    atm_vol = vol_results_setup.atm_vol
    assert atm_vol == 0.3079920087682748

def test_moneyness_005(vol_results_setup):
    moneyness = vol_results_setup.moneyness[0]
    assert moneyness == 0.05

def test_moneyness_025(vol_results_setup):
    moneyness = vol_results_setup.moneyness[1]
    assert moneyness == 0.25

def test_moneyness_050(vol_results_setup):
    moneyness = vol_results_setup.moneyness[2]
    assert moneyness == 0.5

def test_moneyness_075(vol_results_setup):
    moneyness = vol_results_setup.moneyness[3]
    assert moneyness == 0.75

def test_moneyness_095(vol_results_setup):
    moneyness = vol_results_setup.moneyness[4]
    assert moneyness == 0.95


def test_vol_shift_005(vol_results_setup):
    vol_shift = vol_results_setup.vol_shifts[0]
    assert vol_shift == 0.23238571380026762


def test_vol_shift_025(vol_results_setup):
    vol_shift = vol_results_setup.vol_shifts[1]
    assert vol_shift == 0.11576017976694231


def test_vol_shift_050(vol_results_setup):
    vol_shift = vol_results_setup.vol_shifts[2]
    assert vol_shift == 0.0


def test_vol_shift_075(vol_results_setup):
    vol_shift = vol_results_setup.vol_shifts[3]
    assert vol_shift == -0.10814518424598607


def test_vol_shift_095(vol_results_setup):
    vol_shift = vol_results_setup.vol_shifts[4]
    assert vol_shift == -0.13218923144324835



def test_implicitation_ref_atm(vol_solver_setup):
    implied_vol_solver, value_date, underlying_level, interest_rate = vol_solver_setup
    strike = underlying_level
    expiry_date = date(2017,6,14)
    american_option = AmericanOption(strike, OptionType.CALL, expiry_date)
    quote_value = 1.2
    option_quotation = OptionQuotation(value_date, american_option, quote_value)
    implied_vol = implied_vol_solver.solve(option_quotation)
    assert implied_vol == 0.10535407368974047

def test_implicitation_ref_itm(vol_solver_setup):
    implied_vol_solver, value_date, underlying_level, interest_rate = vol_solver_setup
    strike = underlying_level/2
    expiry_date = date(2017,6,14)
    american_option = AmericanOption(strike, OptionType.CALL, expiry_date)
    quote_value = 30.0
    option_quotation = OptionQuotation(value_date, american_option, quote_value)
    implied_vol = implied_vol_solver.solve(option_quotation)
    assert implied_vol == 1.2225617290665658

def test_implicitation_ref_otm(vol_solver_setup):
    implied_vol_solver, value_date, underlying_level, interest_rate = vol_solver_setup
    strike = underlying_level*2.0
    expiry_date = date(2017,6,14)
    american_option = AmericanOption(strike, OptionType.CALL, expiry_date)
    quote_value = 1.2
    option_quotation = OptionQuotation(value_date, american_option, quote_value)
    implied_vol = implied_vol_solver.solve(option_quotation)
    assert implied_vol == 0.9455509222060225

def test_implicitation_from_call_equals_from_put(vol_solver_setup):
    implied_vol_solver, value_date, underlying_level, interest_rate = vol_solver_setup
    strike = underlying_level
    expiry_date = date(2017,6,14)
    call_american_option = AmericanOption(strike, OptionType.CALL, expiry_date)
    put_american_option = AmericanOption(strike, OptionType.PUT, expiry_date)
    quote_value = 1.2
    call_option_quotation = OptionQuotation(value_date, call_american_option, quote_value)
    put_option_quotation = OptionQuotation(value_date, put_american_option, quote_value)
    implied_vol_call = implied_vol_solver.solve(call_option_quotation)
    implied_vol_put = implied_vol_solver.solve(put_option_quotation)
    assert implied_vol_call == implied_vol_put

def test_implicitation_reprices_call(vol_solver_setup):
    implied_vol_solver, value_date, underlying_level, interest_rate = vol_solver_setup
    strike = underlying_level
    expiry_date = date(2017,6,14)
    call_american_option = AmericanOption(strike, OptionType.CALL, expiry_date)
    quote_value = 1.2
    call_option_quotation = OptionQuotation(value_date, call_american_option, quote_value)
    implied_vol_call = implied_vol_solver.solve(call_option_quotation)
    bs_pricer = ExerciceBoundaryIntegrationBlackScholesAmericanOptionPricer(value_date, Configurations.Pricing.DEFAULT_PRICING_PARAMETERS)
    price = bs_pricer.price(underlying_level,interest_rate, implied_vol_call, call_american_option)
    assert abs(price - quote_value) < 1e-14