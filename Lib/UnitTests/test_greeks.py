from datetime import date, timedelta

import numpy as np
import pytest
from Configurations import Configurations
from Greeks.Runners.GreeksRunner import GreeksRunner
from MarketQuotations.Market import Market
from MarketQuotations.MarketData import MarketData
from Products.AmericanOption import AmericanOption
from Products.OptionType import OptionType
from Rates.ConstantDiscountRateCurveFactory import ConstantDiscountRateCurveFactory
from Smile.Dynamics.LogLinearVolatilityDynamicsModel import LogLinearVolatilityDynamicsModel

from Lib.Common.ResultKeys import ResultKeys


@pytest.fixture(scope = "module")
def greeks_results_setup():

    value_date = date(2017, 3, 14)
    maturity = date(2017,11,15)
    underlying_level = 50.0
    atm_vol = 0.3055
    moneyness = np.array([0.05, 0.25, 0.5, 0.75, 0.95])
    skew_values = np.array([0.238, 0.117, 0, -0.110, -0.110])
    interest_rate = 0.01
    constant_rates_factory = ConstantDiscountRateCurveFactory(value_date)
    raw_discount_rate_curve = constant_rates_factory.build(timedelta(days=1), maturity, interest_rate)
    steepness = -0.25

    market_data= MarketData(underlying_level = underlying_level, atm_vol= atm_vol, moneyness = moneyness, skew_values = skew_values, raw_discount_rate_curve=raw_discount_rate_curve)
    market = Market(value_date, market_data)

    volatility_model = LogLinearVolatilityDynamicsModel(underlying_level, steepness)

    american_option = AmericanOption(strike=60, option_type=OptionType.CALL, expiry_date=maturity)

    pricing_parameters = Configurations.Pricing.DEFAULT_PRICING_PARAMETERS
    greeks_definition = Configurations.Greeks.DEFAULT_GREEKS_DEFINITION

    greeks_runner = GreeksRunner(pricing_parameters, greeks_definition)
    greeks_result = greeks_runner.run(market, volatility_model, american_option)

    return greeks_result

def test_underlying_level(greeks_results_setup):
    underlying_level = greeks_results_setup[ResultKeys.underlying_level]
    assert underlying_level == 50.0

def test_atm(greeks_results_setup):
    atm = greeks_results_setup[ResultKeys.atm_vol]
    assert atm == 0.3055

def test_option_vol(greeks_results_setup):
    option_vol = greeks_results_setup[ResultKeys.option_vol]
    assert option_vol == 0.26777597216420113

def test_price(greeks_results_setup):
    price = greeks_results_setup[ResultKeys.price]
    assert price == 1.3606120073804913

def test_delta(greeks_results_setup):
    delta = greeks_results_setup[ResultKeys.delta]
    assert delta == 0.20139772847304518

def test_gamma(greeks_results_setup):
    gamma = greeks_results_setup[ResultKeys.gamma]
    assert gamma == 0.029146226490774377

def test_vega(greeks_results_setup):
    vega = greeks_results_setup[ResultKeys.vega]
    assert vega == 0.1175147971707966

def test_theta(greeks_results_setup):
    theta = greeks_results_setup[ResultKeys.theta]
    assert theta == -0.0072662666593117375

def test_vanna(greeks_results_setup):
    vanna = greeks_results_setup[ResultKeys.vanna]
    assert vanna == 0.011557656084050886

def test_volga(greeks_results_setup):
    volga = greeks_results_setup[ResultKeys.volga]
    assert volga == 0.0036218068613155241

def test_5P_smile(greeks_results_setup):
    smile_5p = greeks_results_setup[ResultKeys.smile_risk(0.05)]
    assert smile_5p == -0.00016669905760746673

def test_25P_smile(greeks_results_setup):
    smile_25p = greeks_results_setup[ResultKeys.smile_risk(0.25)]
    assert smile_25p == 0.0014573327812215098

def test_25C_smile(greeks_results_setup):
    smile_25c = greeks_results_setup[ResultKeys.smile_risk(0.75)]
    assert smile_25c == 0.045312095416682574

def test_5C_smile(greeks_results_setup):
    smile_5c = greeks_results_setup[ResultKeys.smile_risk(0.95)]
    assert smile_5c == 0.0017551417612805542