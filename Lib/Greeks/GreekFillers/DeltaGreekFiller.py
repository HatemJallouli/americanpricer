from typing import Dict

from Common.MarketStateKey import MarketStateKey
from Common.MarketStateKeys import MarketStateKeys
from Common.PricingResult import PricingResult
from Common.ResultKey import ResultKey
from Greeks.GreekFillers.IGreekFiller import IGreekFiller

from Common.ResultKeys import ResultKeys


class DeltaGreekFiller(IGreekFiller):
    """ Class that retrieves the relevant market states for delta computation, computes the finite difference and fills 
        the greeks dictionary.

        Parameters
        ----------
        delta_bump_size : float
            The additive bump :math:`\epsilon` applied on the underlying :math:`S` for delta computation.

    """
    def __init__(self, delta_bump_size):
        self.__delta_bump_size = delta_bump_size

    def fill(self, pvs: Dict[MarketStateKey, PricingResult], results: Dict[ResultKey, float]) -> None:
        """ Retrieve the relevant market states for delta computation, computes the finite difference and fills 
            the greeks dictionary .

                Parameters
                ----------
                pvs : Dict[:class:`~Common.MarketStateKey`, :class:`~Common.PricingResult`]
                    Dictionary that maps each market state to the option price given that state.    
                results : Dict[:class:`~Common.ResultKey`, float]
                    Dictionary that contains the results of greeks computation.
                
                Returns
                -------
                None

                Examples
                --------
                >>> from Greeks.GreekFillers.DeltaGreekFiller import DeltaGreekFiller
                >>> from Common.MarketStateKeys import MarketStateKeys
                >>> from Common.ResultKeys import ResultKeys
                >>> from Common.PricingResult import PricingResult
                >>> pvs = dict()
                >>> results = dict()
                >>> pvs[MarketStateKeys.delta_up] = PricingResult(1.54,0.0,0.0,0.0)
                >>> pvs[MarketStateKeys.delta_down] = PricingResult(1.50,0.0,0.0,0.0)
                >>> delta_filler = DeltaGreekFiller(delta_bump_size=1.0)
                >>> delta_filler.fill(pvs, results)
                >>> print(results[ResultKeys.delta])
                0.020000000000000018

        """
        delta_up = pvs[MarketStateKeys.delta_up].price
        delta_down = pvs[MarketStateKeys.delta_down].price
        delta = (delta_up - delta_down) / (2 * self.__delta_bump_size)
        results[ResultKeys.delta] = delta

if __name__ == "__main__":
    import doctest
    doctest.testmod()