from typing import Dict

from Common.MarketStateKey import MarketStateKey
from Common.MarketStateKeys import MarketStateKeys
from Common.PricingResult import PricingResult
from Common.ResultKey import ResultKey
from Greeks.GreekFillers.IGreekFiller import IGreekFiller

from Common.ResultKeys import ResultKeys


class VegaGreekFiller(IGreekFiller):
    """ Class that retrieves the relevant market states for vega computation, computes the finite difference and fills 
        the greeks dictionary.

        Parameters
        ----------
        vega_bump_size : float
            The additive bump :math:`\epsilon` applied on the At-The-Money annualized volatility :math:`\sigma` for 
            vega computation.

    """
    def __init__(self, vega_bump_size):
        self.__vega_bump_size = vega_bump_size

    def fill(self, pvs: Dict[MarketStateKey, PricingResult], results: Dict[ResultKey, float]) -> None:
        """ Retrieve the relevant market states for vega computation, computes the finite difference and fills 
            the greeks dictionary.

            Parameters
            ----------
            pvs : Dict[:class:`~Common.MarketStateKey`, :class:`~Common.PricingResult`]
                Dictionary that maps each market state to the option price given that state.
            results : Dict[:class:`~Common.ResultKey`, float]
                Dictionary that contains the results of greeks computation.

            Returns
            -------
            None

            Examples
            --------
            >>> from Greeks.GreekFillers.VegaGreekFiller import VegaGreekFiller
            >>> from Common.MarketStateKeys import MarketStateKeys
            >>> from Common.ResultKeys import ResultKeys
            >>> from Common.PricingResult import PricingResult
            >>> pvs = dict()
            >>> results = dict()
            >>> pvs[MarketStateKeys.vega_up] = PricingResult(1.60,0.0,0.0,0.0)
            >>> pvs[MarketStateKeys.vega_down] = PricingResult(1.52,0.0,0.0,0.0)
            >>> vega_filler = VegaGreekFiller(vega_bump_size= 0.01)
            >>> vega_filler.fill(pvs, results)
            >>> print(results[ResultKeys.vega])
            4.0000000000000036

        """
        vega_up = pvs[MarketStateKeys.vega_up].price
        vega_down = pvs[MarketStateKeys.vega_down].price
        vega = (vega_up - vega_down) / (2 * self.__vega_bump_size)
        results[ResultKeys.vega] = vega

if __name__ == "__main__":
    import doctest
    doctest.testmod()