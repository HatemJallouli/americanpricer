from typing import Dict

from Common.MarketStateKey import MarketStateKey
from Common.MarketStateKeys import MarketStateKeys
from Common.PricingResult import PricingResult
from Common.ResultKey import ResultKey
from Greeks.GreekFillers.IGreekFiller import IGreekFiller

from Common.ResultKeys import ResultKeys


class VolgaGreekFiller(IGreekFiller):
    """ Class that retrieves the relevant market states for volga computation, computes the finite difference and fills 
        the greeks dictionary.

        Parameters
        ----------
        vega_bump_size : float
            The additive bump :math:`\epsilon` applied on the At-The-Money annualized volatility :math:`\sigma` for vega computation.
        volga_bump_size : float
            The additive bump :math:`\epsilon` applied on the At-The-Money annualized volatility :math:`\sigma` for volga computation.

    """
    def __init__(self, vega_bump_size, volga_bump_size):

        self.__vega_bump_size = vega_bump_size
        self.__volga_bump_size = volga_bump_size

    def fill(self, pvs: Dict[MarketStateKey, PricingResult], results: Dict[ResultKey, float]) -> None:
        """ Retrieve the relevant market states for volga computation, computes the finite difference and fills 
            the greeks dictionary.

            Parameters
            ----------
            pvs : Dict[:class:`~Common.MarketStateKey`, :class:`~Common.PricingResult`]
                Dictionary that maps each market state to the option price given that state.
            results : Dict[:class:`~Common.ResultKey`, float]
                Dictionary that contains the results of greeks computation.

            Returns
            -------
            None

            Examples
            --------
            >>> from Greeks.GreekFillers.VolgaGreekFiller import VolgaGreekFiller
            >>> from Common.MarketStateKeys import MarketStateKeys
            >>> from Common.ResultKeys import ResultKeys
            >>> from Common.PricingResult import PricingResult
            >>> pvs = dict()
            >>> results = dict()
            >>> pvs[MarketStateKeys.volga_up_up] = PricingResult(1.60,0.0,0.0,0.0)
            >>> pvs[MarketStateKeys.volga_up_down] = PricingResult(1.52,0.0,0.0,0.0)
            >>> pvs[MarketStateKeys.volga_down_up] = PricingResult(1.50,0.0,0.0,0.0)
            >>> pvs[MarketStateKeys.volga_down_down] = PricingResult(1.48,0.0,0.0,0.0)
            >>> gamma_filler = VolgaGreekFiller(vega_bump_size=0.01, volga_bump_size= 0.005)
            >>> gamma_filler.fill(pvs, results)
            >>> print(results[ResultKeys.volga])
            300.0000000000003

        """
        volga_up_up = pvs[MarketStateKeys.volga_up_up].price
        volga_up_down = pvs[MarketStateKeys.volga_up_down].price
        volga_down_up = pvs[MarketStateKeys.volga_down_up].price
        volga_down_down = pvs[MarketStateKeys.volga_down_down].price

        vega_up = (volga_up_up - volga_up_down) / (2.0 * self.__vega_bump_size)
        vega_down = (volga_down_up - volga_down_down) / (2.0 * self.__vega_bump_size)
        volga = (vega_up - vega_down) / (2.0 * self.__volga_bump_size)

        results[ResultKeys.volga] = volga

if __name__ == "__main__":
    import doctest
    doctest.testmod()