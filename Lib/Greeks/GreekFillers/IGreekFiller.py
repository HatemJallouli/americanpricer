from typing import Dict

from Common.PricingResult import PricingResult
from Greeks.Common.GreekKey import *

from Common.MarketStateKey import *


class IGreekFiller:
    """ General interface that retrieves the relevant market states for greek computation, computes the finite 
        difference and fills the greeks dictionary.
        
        See Also
        --------
        DeltaGreekFiller: :class:`~Greeks.GreekFillers.DeltaGreekFiller`
        GammaGreekFiller: :class:`~Greeks.GreekFillers.GammaGreekFiller`
        RhoGreekFiller: :class:`~Greeks.GreekFillers.RhoGreekFiller`
        SmileRiskGreekFiller: :class:`~Greeks.GreekFillers.SmileRiskGreekFiller`
        ThetaGreekFiller: :class:`~Greeks.GreekFillers.ThetaGreekFiller`
        VannaGreekFiller: :class:`~Greeks.GreekFillers.VannaGreekFiller`
        VegaGreekFiller: :class:`~Greeks.GreekFillers.VegaGreekFiller`
        VolgaGreekFiller: :class:`~Greeks.GreekFillers.VolgaGreekFiller`
    """
    def fill(self, pvs: Dict[MarketStateKey, PricingResult], results: Dict[ResultKey, float]) -> None:
        """ Retrieve the relevant market states for greek computation, computes the finite difference and fills 
            the greeks dictionary.

            Parameters
            ----------
            pvs : Dict[:class:`~Common.MarketStateKey`, :class:`~Common.PricingResult`]
                Dictionary that maps each market state to the option price result given that state.
            results : Dict[:class:`~Common.ResultKey`, float]
                Dictionary that contains the results of greeks computation.

            Returns
            -------
            None
        """
        raise NotImplementedError("Method {0} is an abstract method of interface {1}".format(self.fill.__name__,
                                                                                             self.__class__.__name__))


