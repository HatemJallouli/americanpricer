from typing import Dict

from Common.MarketStateKey import MarketStateKey
from Common.MarketStateKeys import MarketStateKeys
from Common.PricingResult import PricingResult
from Common.ResultKey import ResultKey
from Greeks.GreekFillers.IGreekFiller import IGreekFiller

from Common.ResultKeys import ResultKeys


class GammaGreekFiller(IGreekFiller):
    """ Class that retrieves the relevant market states for gamma computation, computes the finite difference and fills 
        the greeks dictionary.

        Parameters
        ----------
        delta_bump_size : float
            The additive bump :math:`\epsilon` applied on the underlying :math:`S` for delta computation.
        gamma_bump_size : float
            The additive bump :math:`\epsilon` applied on the underlying :math:`S` for gamma computation.
    """
    def __init__(self, delta_bump_size, gamma_bump_size):
        self.__delta_bump_size = delta_bump_size
        self.__gamma_bump_size = gamma_bump_size

    def fill(self, pvs: Dict[MarketStateKey, PricingResult], results: Dict[ResultKey, float]) -> None:
        """ Retrieve the relevant market states for gamma computation, computes the finite difference and fills 
            the greeks dictionary.

            Parameters
            ----------
            pvs : Dict[:class:`~Common.MarketStateKey`, :class:`~Common.PricingResult`]
                Dictionary that maps each market state to the option price given that state.
            results : Dict[:class:`~Common.ResultKey`, float]
                Dictionary that contains the results of greeks computation.

            Returns
            -------
            None

            Examples
            --------
            >>> from Greeks.GreekFillers.GammaGreekFiller import GammaGreekFiller
            >>> from Common.MarketStateKeys import MarketStateKeys
            >>> from Common.ResultKeys import ResultKeys
            >>> from Common.PricingResult import PricingResult
            >>> pvs = dict()
            >>> results = dict()
            >>> pvs[MarketStateKeys.gamma_up_up] = PricingResult(1.60,0.0,0.0,0.0)
            >>> pvs[MarketStateKeys.gamma_up_down] = PricingResult(1.52,0.0,0.0,0.0)
            >>> pvs[MarketStateKeys.gamma_down_up] = PricingResult(1.50,0.0,0.0,0.0)
            >>> pvs[MarketStateKeys.gamma_down_down] = PricingResult(1.48,0.0,0.0,0.0)
            >>> gamma_filler = GammaGreekFiller(delta_bump_size=1.0, gamma_bump_size= 0.2)
            >>> gamma_filler.fill(pvs, results)
            >>> print(results[ResultKeys.gamma])
            0.07500000000000007

        """

        gamma_up_up = pvs[MarketStateKeys.gamma_up_up].price
        gamma_up_down = pvs[MarketStateKeys.gamma_up_down].price
        gamma_down_up = pvs[MarketStateKeys.gamma_down_up].price
        gamma_down_down = pvs[MarketStateKeys.gamma_down_down].price

        delta_up = (gamma_up_up - gamma_up_down) / (2.0 * self.__delta_bump_size)
        delta_down = (gamma_down_up - gamma_down_down) / (2.0 * self.__delta_bump_size)
        gamma = (delta_up - delta_down) / (2.0 * self.__gamma_bump_size)

        results[ResultKeys.gamma] = gamma

if __name__ == "__main__":
    import doctest
    doctest.testmod()