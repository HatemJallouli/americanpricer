from typing import List

from Greeks.Common.GreeksDefinition import GreeksDefinition
from Greeks.Common.ShockUtilities import *
from Greeks.GreekFillers.DeltaGreekFiller import DeltaGreekFiller
from Greeks.GreekFillers.GammaGreekFiller import GammaGreekFiller
from Greeks.GreekFillers.IGreekFiller import IGreekFiller
from Greeks.GreekFillers.RhoGreekFiller import RhoGreekFiller
from Greeks.GreekFillers.SmileRiskGreekFiller import SmilerRiskGreekFiller
from Greeks.GreekFillers.ThetaGreekFiller import ThetaGreekFiller
from Greeks.GreekFillers.VannaGreekFiller import VannaGreekFiller
from Greeks.GreekFillers.VegaGreekFiller import VegaGreekFiller
from MarketQuotations.Market import Market

from Greeks.GreekFillers.VolgaGreekFiller import VolgaGreekFiller


class GreekFillersFactory:
    """ Class that builds a list of greeks fillers given a greeks definition.

        Parameters
        ----------
        market : :class:`~MarketQuotations.Market`
            The market data necessary to convert relative bumps into absolute ones.

    """
    def __init__(self, market : Market):
        self.__market = market

    def build(self, greeks_definition: GreeksDefinition) -> List[IGreekFiller]:
        """ Build a list of greeks fillers given a greeks definition.

            Parameters
            ----------
            greeks_definition : :class:`~Greeks.Common.GreeksDefinition`
                The description of bumps that are applied on market data for greeks computation.

            Returns
            -------
            greek_fillers : List[:class:`~Greeks.GreekFillers.IGreekFiller`]
                The list of greek fillers that will be used to compute the greeks

        """

        result = []

        underlying_level = self.__market.market_data.underlying_level
        atm_vol = self.__market.market_data.atm_vol
        moneyness = self.__market.market_data.moneyness

        # Reading Greeks Definition
        delta_relative_bump_size = greeks_definition.delta_relative_bump_size
        gamma_relative_bump_size = greeks_definition.gamma_relative_bump_size
        vega_percent_bump_size = greeks_definition.vega_bump_size
        volga_percent_bump_size = greeks_definition.volga_bump_size
        smile_risk_percent_bump_size = greeks_definition.smile_risk_bump_size

        # Converting Greeks Definition
        delta_bump_size = ShockUtilities.to_absolute(delta_relative_bump_size, underlying_level, atm_vol)
        gamma_bump_size = ShockUtilities.to_absolute(gamma_relative_bump_size, underlying_level, atm_vol)
        vega_bump_size = vega_percent_bump_size * 100.0
        volga_bump_size = volga_percent_bump_size * 100.0
        smile_risk_bump_size = smile_risk_percent_bump_size * 100.0

        # Creating Greek Fillers
        delta_filler = DeltaGreekFiller(delta_bump_size)
        gamma_filler = GammaGreekFiller(delta_bump_size, gamma_bump_size)
        vega_filler = VegaGreekFiller(vega_bump_size)
        theta_filler = ThetaGreekFiller()
        rho_filler = RhoGreekFiller()
        vanna_filler = VannaGreekFiller(delta_bump_size, vega_bump_size)
        volga_filler = VolgaGreekFiller(vega_bump_size, volga_bump_size)

        # Creating the result
        result.append(delta_filler)
        result.append(gamma_filler)
        result.append(vega_filler)
        result.append(theta_filler)
        result.append(rho_filler)
        result.append(vanna_filler)
        result.append(volga_filler)

        for moneyness_level in moneyness:
            smile_risk_filler = SmilerRiskGreekFiller(moneyness_level, smile_risk_bump_size)
            result.append(smile_risk_filler)

        return result


