from typing import Dict

from Common.MarketStateKey import MarketStateKey
from Common.MarketStateKeys import MarketStateKeys
from Common.PricingResult import PricingResult
from Common.ResultKey import ResultKey
from Greeks.GreekFillers.IGreekFiller import IGreekFiller

from Common.ResultKeys import ResultKeys


class SmilerRiskGreekFiller(IGreekFiller):
    """ Class that retrieves the relevant market states for smile risk computation, computes the finite difference and fills 
        the greeks dictionary.

        Parameters
        ----------
        moneyness : float
            The moneyness expressed in delta to identify the shocked implied volatility.
        smile_risk_bump_size : float
            The additive bump :math:`\epsilon` applied on the smile for smile risk computation.


    """
    def __init__(self, moneyness, smile_risk_bump_size):

        self.__moneyness = moneyness
        self.__smile_risk_bump_size = smile_risk_bump_size

    def fill(self, pvs: Dict[MarketStateKey, PricingResult], results: Dict[ResultKey, float]) -> None:
        """ Retrieve the relevant market states for smile risk computation, computes the finite difference and fills 
            the greeks dictionary .

            Parameters
            ----------
            pvs : Dict[:class:`~Common.MarketStateKey`, :class:`~Common.PricingResult`]
                Dictionary that maps each market state to the option price given that state.
            results : Dict[:class:`~Common.ResultKey`, float]
                Dictionary that contains the results of greeks computation.

            Returns
            -------
            None

            Examples
            --------
            >>> from Greeks.GreekFillers.SmileRiskGreekFiller import SmilerRiskGreekFiller
            >>> from Common.MarketStateKeys import MarketStateKeys
            >>> from Common.ResultKeys import ResultKeys
            >>> from Common.PricingResult import PricingResult
            >>> pvs = dict()
            >>> results = dict()
            >>> moneyness = 0.95
            >>> pvs[MarketStateKeys.smile_risk_up(moneyness)] = PricingResult(1.60,0.0,0.0,0.0)
            >>> pvs[MarketStateKeys.smile_risk_down(moneyness)] = PricingResult(1.52,0.0,0.0,0.0)
            >>> smile_risk_filler = SmilerRiskGreekFiller(moneyness=moneyness, smile_risk_bump_size=0.01)
            >>> smile_risk_filler.fill(pvs, results)
            >>> print(results[ResultKeys.smile_risk(moneyness)])
            4.0000000000000036

        """
        smile_risk_up = pvs[MarketStateKeys.smile_risk_up(self.__moneyness)].price
        smile_risk_down = pvs[MarketStateKeys.smile_risk_down(self.__moneyness)].price
        smile_risk = (smile_risk_up - smile_risk_down) / (2.0 * self.__smile_risk_bump_size)
        results[ResultKeys.smile_risk(self.__moneyness)] = smile_risk

if __name__ == "__main__":
    import doctest
    doctest.testmod()