from typing import Dict

from Common.MarketStateKey import MarketStateKey
from Common.MarketStateKeys import MarketStateKeys
from Common.PricingResult import PricingResult
from Common.ResultKey import ResultKey
from Greeks.GreekFillers.IGreekFiller import IGreekFiller

from Common.ResultKeys import ResultKeys


class VannaGreekFiller(IGreekFiller):
    """ Class that retrieves the relevant market states for vanna computation, computes the finite difference and fills 
        the greeks dictionary.

        Parameters
        ----------
        delta_bump_size : float
            The additive bump :math:`\epsilon` applied on the underlying :math:`S` for delta computation.
        vega_bump_size : float
            The additive bump :math:`\epsilon` applied on the At-The-Money annualized volatility :math:`\sigma` for 
            vega computation.


    """
    def __init__(self, delta_bump_size, vega_bump_size):
        self.__delta_bump_size = delta_bump_size
        self.__vega_bump_size = vega_bump_size

    def fill(self, pvs: Dict[MarketStateKey, PricingResult], results: Dict[ResultKey, float]) -> None:
        """ Retrieve the relevant market states for vanna computation, computes the finite difference and fills 
            the greeks dictionary.

            Parameters
            ----------
            pvs : Dict[:class:`~Common.MarketStateKey`, :class:`~Common.PricingResult`]
                Dictionary that maps each market state to the option price given that state.
            results : Dict[:class:`~Common.ResultKey`, float]
                Dictionary that contains the results of greeks computation.

            Returns
            -------
            None

            Examples
            --------
            >>> from Greeks.GreekFillers.VannaGreekFiller import VannaGreekFiller
            >>> from Common.MarketStateKeys import MarketStateKeys
            >>> from Common.ResultKeys import ResultKeys
            >>> from Common.PricingResult import PricingResult
            >>> pvs = dict()
            >>> results = dict()
            >>> pvs[MarketStateKeys.vanna_delta_up_vega_up] = PricingResult(1.60,0.0,0.0,0.0)
            >>> pvs[MarketStateKeys.vanna_delta_up_vega_down] = PricingResult(1.52,0.0,0.0,0.0)
            >>> pvs[MarketStateKeys.vanna_delta_down_vega_up] = PricingResult(1.50,0.0,0.0,0.0)
            >>> pvs[MarketStateKeys.vanna_delta_down_vega_down] = PricingResult(1.48,0.0,0.0,0.0)
            >>> vanna_filler = VannaGreekFiller(delta_bump_size=1.0, vega_bump_size= 0.01)
            >>> vanna_filler.fill(pvs, results)
            >>> print(results[ResultKeys.vanna])
            1.5000000000000013

        """
        vanna_delta_up_vega_up = pvs[MarketStateKeys.vanna_delta_up_vega_up].price
        vanna_delta_up_vega_down = pvs[MarketStateKeys.vanna_delta_up_vega_down].price
        vanna_delta_down_vega_up = pvs[MarketStateKeys.vanna_delta_down_vega_up].price
        vanna_delta_down_vega_down = pvs[MarketStateKeys.vanna_delta_down_vega_down].price

        delta_up_vega = (vanna_delta_up_vega_up - vanna_delta_up_vega_down) / (2.0 * self.__vega_bump_size)
        delta_down_vega = (vanna_delta_down_vega_up - vanna_delta_down_vega_down) / (2.0 * self.__vega_bump_size)
        vanna = (delta_up_vega - delta_down_vega) / (2.0 * self.__delta_bump_size)

        results[ResultKeys.vanna] = vanna

if __name__ == "__main__":
    import doctest
    doctest.testmod()