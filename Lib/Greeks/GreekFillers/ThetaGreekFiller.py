from typing import Dict

from Common.MarketStateKey import MarketStateKey
from Common.MarketStateKeys import MarketStateKeys
from Common.PricingResult import PricingResult
from Common.ResultKey import ResultKey
from Greeks.GreekFillers.IGreekFiller import IGreekFiller

from Common.ResultKeys import ResultKeys


class ThetaGreekFiller(IGreekFiller):
    """ Class that retrieves the relevant market states for theta computation, computes the finite difference and fills 
        the greeks dictionary.

    """
    def fill(self, pvs: Dict[MarketStateKey, PricingResult], results: Dict[ResultKey, float]) -> None:
        """ Retrieve the relevant market states for rho computation, computes the finite difference and fills 
            the greeks dictionary .

            Parameters
            ----------
            pvs : Dict[:class:`~Common.MarketStateKey`, :class:`~Common.PricingResult`]
                Dictionary that maps each market state to the option price given that state.
            results : Dict[:class:`~Common.ResultKey`, float]
                Dictionary that contains the results of greeks computation.

            Returns
            -------
            None

            Examples
            --------
            >>> from Greeks.GreekFillers.ThetaGreekFiller import ThetaGreekFiller
            >>> from Common.MarketStateKeys import MarketStateKeys
            >>> from Common.ResultKeys import ResultKeys
            >>> from Common.PricingResult import PricingResult
            >>> pvs = dict()
            >>> results = dict()
            >>> moneyness = 0.95
            >>> pvs[MarketStateKeys.theta_up] = PricingResult(1.50,0.0,0.0,0.0)
            >>> pvs[MarketStateKeys.price] = PricingResult(1.52,0.0,0.0,0.0)
            >>> theta_filler = ThetaGreekFiller()
            >>> theta_filler.fill(pvs, results)
            >>> print(results[ResultKeys.theta])
            -0.020000000000000018

        """
        theta_up = pvs[MarketStateKeys.theta_up].price
        price = pvs[MarketStateKeys.price].price
        theta = (theta_up - price)
        results[ResultKeys.theta] = theta


if __name__ == "__main__":
    import doctest
    doctest.testmod()
