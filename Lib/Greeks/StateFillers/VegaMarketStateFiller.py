from typing import Dict

from Common.MarketStateKey import MarketStateKey
from Common.MarketStateKeys import MarketStateKeys
from Greeks.Common.GreeksDefinition import GreeksDefinition
from MarketQuotations.Market import Market
from MarketQuotations.MarketData import MarketData

from Greeks.StateFillers.IMarketStateFiller import IMarketStateFiller


class VegaMarketStateFiller(IMarketStateFiller):
    """ Class that defines the market states that are relevant for vega computation by finite difference.

        Parameters
        ----------
        market : :class:`~MarketQuotations.Market`
            The market data necessary to compute price and greeks.

    """
    def __init__(self, market : Market):
        self.__market = market

    def fill(self, greeks_definition: GreeksDefinition, market_states: Dict[MarketStateKey, Market]) -> None:
        """ Fills the market state dictionary by the relevant states necessary to vega calculation.

            Parameters
            ----------
            greeks_definition : :class:`~Greeks.Common.GreeksDefinition`
                The description of bumps that are applied on market data for greeks computation.   
            market_states : Dict[:class:`~Common.MarketStateKey`, float]
                Dictionary of market states identified by its market state key.

            Returns
            -------
            None

        """
        vega_up = MarketStateKeys.vega_up
        vega_down = MarketStateKeys.vega_down

        market = self.__market
        market_data = market.market_data

        value_date = market.ref_date
        underlying_level = market_data.underlying_level
        atm_vol = market_data.atm_vol
        moneyness = market_data.moneyness
        skew_values = market_data.skew_values
        raw_discount_rate_curve = market_data.raw_discount_rate_curve

        vega_percent_bump_size = greeks_definition.vega_bump_size

        atm_vol_up = atm_vol + vega_percent_bump_size
        atm_vol_down = atm_vol - vega_percent_bump_size

        market_up   =  Market(value_date, MarketData(underlying_level, atm_vol_up, moneyness,skew_values, raw_discount_rate_curve))
        market_down =  Market(value_date, MarketData(underlying_level, atm_vol_down, moneyness,skew_values, raw_discount_rate_curve))

        market_states[vega_up] = market_up
        market_states[vega_down] = market_down