from typing import Dict

import numpy as np
from Calendars.CalendarTimeMeasure import CalendarTimeMeasure
from Common.MarketStateKey import MarketStateKey
from Common.MarketStateKeys import MarketStateKeys
from Greeks.Common.GreeksDefinition import GreeksDefinition
from MarketQuotations.Market import Market
from MarketQuotations.MarketData import MarketData
from Rates.RawDiscountRateCurve import RawDiscountRateCurve

from Greeks.StateFillers.IMarketStateFiller import IMarketStateFiller


class RhoMarketStateFiller(IMarketStateFiller):
    """ Class that defines the market states that are relevant for rho computation by finite difference.

        Parameters
        ----------
        market : :class:`~MarketQuotations.Market`
            The market data necessary to compute price and greeks.

    """
    def __init__(self, market : Market):
        self.__market = market
        self.__time_measure = CalendarTimeMeasure(market.ref_date)
    def fill(self, greeks_definition: GreeksDefinition, market_states: Dict[MarketStateKey, Market]) -> None:
        """ Fills the market state dictionary by the relevant states necessary to rho calculation.

            Parameters
            ----------
            greeks_definition : :class:`~Greeks.Common.GreeksDefinition`
                The description of bumps that are applied on market data for greeks computation.   
            market_states : Dict[:class:`~Common.MarketStateKey`, float]
                Dictionary of market states identified by its market state key.

            Returns
            -------
            None

        """
        rho_up = MarketStateKeys.rho_up

        market = self.__market
        market_data = market.market_data

        value_date = market.ref_date
        underlying_level = market_data.underlying_level
        atm_vol = market_data.atm_vol
        moneyness = market_data.moneyness
        skew_values = market_data.skew_values
        raw_discount_rate_curve = market_data.raw_discount_rate_curve

        rate_bump = greeks_definition.rho_bump_size

        durations = []
        discount_factors_up = []

        for i in range(len(raw_discount_rate_curve.durations)):
            duration = raw_discount_rate_curve.durations[i]
            discount_factor = raw_discount_rate_curve.discount_factors[i]
            durations.append(duration)
            discount_factor_up = discount_factor * np.exp(-duration * rate_bump)
            discount_factors_up.append(discount_factor_up)

        durations = np.array(durations)
        discount_factors_up = np.array(discount_factors_up)

        raw_discount_rate_curve_up = RawDiscountRateCurve(raw_discount_rate_curve.rate_curve_id, durations, discount_factors_up)
        market_up = Market(value_date, MarketData(underlying_level, atm_vol, moneyness, skew_values, raw_discount_rate_curve_up))
        market_states[rho_up] = market_up

