from typing import Dict

from Greeks.Common.GreeksDefinition import GreeksDefinition
from MarketQuotations.Market import Market

from Common.MarketStateKey import *


class IMarketStateFiller:
    """ General interface that defines the market states that are relevant for greek computation by finite difference .
        
        See Also
        --------
        PriceMarketStateFiller: :class:`~Greeks.StateFillers.PriceMarketStateFiller`
        DeltaMarketStateFiller: :class:`~Greeks.StateFillers.DeltaMarketStateFiller`
        GammaMarketStateFiller: :class:`~Greeks.StateFillers.GammaMarketStateFiller`
        RhoMarketStateFiller: :class:`~Greeks.StateFillers.RhoMarketStateFiller`
        SmileRiskMarketStateFiller: :class:`~Greeks.StateFillers.SmileRiskMarketStateFiller`
        ThetaMarketStateFiller: :class:`~Greeks.StateFillers.ThetaMarketStateFiller`
        VannaMarketStateFiller: :class:`~Greeks.StateFillers.VannaMarketStateFiller`
        VegaMarketStateFiller: :class:`~Greeks.StateFillers.VegaMarketStateFiller`
        VolgaMarketStateFiller: :class:`~Greeks.StateFillers.VolgaMarketStateFiller`
    """
    def fill(self, greeks_definition : GreeksDefinition, market_states : Dict[MarketStateKey, Market]) -> None:
        """ Fills the market state dictionary by the relevant states necessary to the greek calculation.

            Parameters
            ----------
            greeks_definition : :class:`~Greeks.Common.Market`
                The description of bumps that are applied on market data for greeks computation.   
            market_states : Dict[:class:`~Common.MarketStateKey`, float]
                Dictionary of market states identified by its market state key.

            Returns
            -------
            None
        """
        raise NotImplementedError("Method {0} is an abstract method of interface {1}".format(self.fill.__name__,self.__class__.__name__))



