from typing import List

from Greeks.StateFillers.DeltaMarketStateFiller import DeltaMarketStateFiller
from Greeks.StateFillers.GammaMarketStateFiller import GammaMarketStateFiller
from Greeks.StateFillers.PriceMarketStateFiller import PriceMarketStateFiller
from Greeks.StateFillers.RhoMarketStateFiller import RhoMarketStateFiller
from Greeks.StateFillers.SmileMarketRiskStateFiller import SmileRiskMarketStateFiller
from Greeks.StateFillers.ThetaMarketStateFiller import ThetaMarketStateFiller
from Greeks.StateFillers.VannaMarketStateFiller import VannaMarketStateFiller
from Greeks.StateFillers.VegaMarketStateFiller import VegaMarketStateFiller
from Greeks.StateFillers.VolgaMarketStateFiller import VolgaMarketStateFiller
from MarketQuotations.Market import Market

from Greeks.StateFillers.IMarketStateFiller import IMarketStateFiller


class StateFillersFactory:
    """ Class that builds a list of market state fillers given an initial market.
    """
    @staticmethod
    def build(market: Market) -> List[IMarketStateFiller]:
        """ Build a list of market state fillers given an initial market.
        
            Parameters
            ----------
            market : :class:`~MarketQuotations.Market`
                The market data necessary to compute price and greeks.
            
            Returns
            -------
            market_state_fillers : List[:class:`~Greeks.StateFillers.IMarketStateFiller`]
        """

        result = []

        price_state_filler = PriceMarketStateFiller(market)
        delta_state_filler = DeltaMarketStateFiller(market)
        gamma_state_filler = GammaMarketStateFiller(market)
        vega_state_filler = VegaMarketStateFiller(market)
        theta_state_filler = ThetaMarketStateFiller(market)
        rho_state_filler = RhoMarketStateFiller(market)
        vanna_state_filler = VannaMarketStateFiller(market)
        volga_state_filler = VolgaMarketStateFiller(market)

        result.append(price_state_filler)
        result.append(delta_state_filler)
        result.append(gamma_state_filler)
        result.append(vega_state_filler)
        result.append(theta_state_filler)
        result.append(rho_state_filler)
        result.append(vanna_state_filler)
        result.append(volga_state_filler)

        for moneyness_index in range(len(market.market_data.moneyness)):
            smile_risk_state_filler = SmileRiskMarketStateFiller(market, moneyness_index)
            result.append(smile_risk_state_filler)

        return result
