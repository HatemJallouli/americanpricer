from typing import Dict

from Common.MarketStateKey import MarketStateKey
from Common.MarketStateKeys import MarketStateKeys
from Greeks.Common.GreeksDefinition import GreeksDefinition
from MarketQuotations.Market import Market
from MarketQuotations.MarketData import MarketData

from Greeks.StateFillers.IMarketStateFiller import IMarketStateFiller


class SmileRiskMarketStateFiller(IMarketStateFiller):
    """ Class that defines the market states that are relevant for smile risk computation by finite difference.

        Parameters
        ----------
        market : :class:`~MarketQuotations.Market`
            The market data necessary to compute price and greeks.

    """

    def __init__(self, market : Market, moneyness_index : int):
        self.__market = market
        self.__moneyness_index = moneyness_index

    def fill(self, greeks_definition: GreeksDefinition, market_states: Dict[MarketStateKey, Market]) -> None:
        """ Fills the market state dictionary by the relevant states necessary to smile risk calculation.

            Parameters
            ----------
            greeks_definition : :class:`~Greeks.Common.GreeksDefinition`
                The description of bumps that are applied on market data for greeks computation.   
            market_states : Dict[:class:`~Common.MarketStateKey`, float]
                Dictionary of market states identified by its market state key.

            Returns
            -------
            None

        """
        market = self.__market
        market_data = market.market_data

        value_date = market.ref_date
        underlying_level = market_data.underlying_level
        atm_vol = market_data.atm_vol
        moneyness_vec = market_data.moneyness
        skew_values = market_data.skew_values
        raw_discount_rate_curve = market_data.raw_discount_rate_curve
        moneyness_index = self.__moneyness_index

        moneyness = moneyness_vec[moneyness_index]
        vol_shift = skew_values[moneyness_index]

        smile_risk_up = MarketStateKeys.smile_risk_up(moneyness)
        smile_risk_down = MarketStateKeys.smile_risk_down(moneyness)

        smile_risk_percent_bump_size = greeks_definition.smile_risk_bump_size

        vol_shift_up = vol_shift + smile_risk_percent_bump_size
        vol_shift_down = vol_shift - smile_risk_percent_bump_size

        vol_shifts_up = skew_values.copy()
        vol_shifts_down = skew_values.copy()

        vol_shifts_up[moneyness_index] = vol_shift_up
        vol_shifts_down[moneyness_index] = vol_shift_down

        market_up   = Market(value_date, MarketData(underlying_level, atm_vol, moneyness_vec, vol_shifts_up, raw_discount_rate_curve))
        market_down = Market(value_date, MarketData(underlying_level, atm_vol, moneyness_vec, vol_shifts_down, raw_discount_rate_curve))

        market_states[smile_risk_up] = market_up
        market_states[smile_risk_down] = market_down