from typing import Dict

from Common.MarketStateKey import MarketStateKey
from Common.MarketStateKeys import MarketStateKeys
from Greeks.Common.GreeksDefinition import GreeksDefinition
from MarketQuotations.Market import Market
from MarketQuotations.MarketData import MarketData

from Greeks.StateFillers.IMarketStateFiller import IMarketStateFiller


class VolgaMarketStateFiller(IMarketStateFiller):
    """ Class that defines the market states that are relevant for volga computation by finite difference.

        Parameters
        ----------
        market : :class:`~MarketQuotations.Market`
            The market data necessary to compute price and greeks.

    """
    def __init__(self, market: Market):
        self.__market = market

    def fill(self, greeks_definition: GreeksDefinition, market_states: Dict[MarketStateKey, Market]) -> None:
        """ Fills the market state dictionary by the relevant states necessary to volga calculation.

            Parameters
            ----------
            greeks_definition : :class:`~Greeks.Common.GreeksDefinition`
                The description of bumps that are applied on market data for greeks computation.   
            market_states : Dict[:class:`~Common.MarketStateKey`, float]
                Dictionary of market states identified by its market state key.

            Returns
            -------
            None

        """
        volga_up_up = MarketStateKeys.volga_up_up
        volga_up_down = MarketStateKeys.volga_up_down
        volga_down_up = MarketStateKeys.volga_down_up
        volga_down_down = MarketStateKeys.volga_down_down

        market = self.__market
        market_data = market.market_data

        value_date = market.ref_date
        underlying_level = market_data.underlying_level
        atm_vol = market_data.atm_vol
        moneyness = market_data.moneyness
        skew_values = market_data.skew_values
        raw_discount_rate_curve = market_data.raw_discount_rate_curve

        vega_percent_bump_size = greeks_definition.vega_bump_size
        volga_percent_bump_size = greeks_definition.volga_bump_size

        atm_vol_up_up = atm_vol + vega_percent_bump_size + volga_percent_bump_size
        atm_vol_up_down = atm_vol + vega_percent_bump_size - volga_percent_bump_size
        atm_vol_down_up = atm_vol - vega_percent_bump_size + volga_percent_bump_size
        atm_vol_down_down = atm_vol - vega_percent_bump_size - volga_percent_bump_size

        market_up_up = Market(value_date,
                              MarketData(underlying_level, atm_vol_up_up, moneyness, skew_values, raw_discount_rate_curve))
        market_up_down = Market(value_date,
                                MarketData(underlying_level, atm_vol_up_down, moneyness, skew_values, raw_discount_rate_curve))
        market_down_up = Market(value_date,
                                MarketData(underlying_level, atm_vol_down_up, moneyness, skew_values, raw_discount_rate_curve))
        market_down_down = Market(value_date, MarketData(underlying_level, atm_vol_down_down, moneyness, skew_values,
                                                         raw_discount_rate_curve))

        market_states[volga_up_up] = market_up_up
        market_states[volga_up_down] = market_up_down
        market_states[volga_down_up] = market_down_up
        market_states[volga_down_down] = market_down_down
