from typing import Dict

from Common.MarketStateKey import MarketStateKey
from Common.MarketStateKeys import MarketStateKeys
from Greeks.Common.GreeksDefinition import GreeksDefinition
from MarketQuotations.Market import Market

from Greeks.StateFillers.IMarketStateFiller import IMarketStateFiller


class PriceMarketStateFiller(IMarketStateFiller):
    """ Class that defines the market states that are relevant for a price computation.

        Parameters
        ----------
        market : :class:`~MarketQuotations.MarketQuotations`
            The market data necessary to compute price and greeks.

    """
    def __init__(self, market: Market):
        self.__market = market

    def fill(self, greeks_definition: GreeksDefinition, market_states: Dict[MarketStateKey, Market]) -> None:
        """ Fills the market state dictionary by the relevant states necessary to price calculation.

            Parameters
            ----------
            greeks_definition : :class:`~Greeks.Common.Market`
                The description of bumps that are applied on market data for greeks computation.   
            market_states : Dict[:class:`~Common.MarketStateKey`, float]
                Dictionary of market states identified by its market state key.

            Returns
            -------
            None

        """
        price = MarketStateKeys.price
        market = self.__market
        market_states[price] = market
