from typing import Dict

from Common.MarketStateKey import MarketStateKey
from Common.MarketStateKeys import MarketStateKeys
from Greeks.Common.GreeksDefinition import GreeksDefinition
from Greeks.Common.ShockUtilities import ShockUtilities
from MarketQuotations.Market import Market
from MarketQuotations.MarketData import MarketData

from Greeks.StateFillers.IMarketStateFiller import IMarketStateFiller


class GammaMarketStateFiller(IMarketStateFiller):
    """ Class that defines the market states that are relevant for gamma computation by finite difference.

        Parameters
        ----------
        market : :class:`~MarketQuotations.Market`
            The market data necessary to compute price and greeks.

    """
    def __init__(self, market : Market):
        self.__market = market

    def fill(self, greeks_definition: GreeksDefinition, market_states: Dict[MarketStateKey, Market]) -> None:
        """ Fills the market state dictionary by the relevant states necessary to gamma calculation.

            Parameters
            ----------
            greeks_definition : :class:`~Greeks.Common.GreeksDefinition`
                The description of bumps that are applied on market data for greeks computation.   
            market_states : Dict[:class:`~Common.MarketStateKey`, float]
                Dictionary of market states identified by its market state key.

            Returns
            -------
            None

        """
        gamma_up_up = MarketStateKeys.gamma_up_up
        gamma_up_down = MarketStateKeys.gamma_up_down
        gamma_down_up = MarketStateKeys.gamma_down_up
        gamma_down_down = MarketStateKeys.gamma_down_down

        market = self.__market
        market_data = market.market_data

        value_date = market.ref_date
        underlying_level = market_data.underlying_level
        atm_vol = market_data.atm_vol
        moneyness = market_data.moneyness
        skew_values = market_data.skew_values
        raw_discount_rate_curve = market_data.raw_discount_rate_curve

        delta_relative_bump_size = greeks_definition.delta_relative_bump_size
        gamma_relative_bump_size = greeks_definition.gamma_relative_bump_size

        delta_bump_size = ShockUtilities.to_absolute(delta_relative_bump_size, underlying_level, atm_vol)
        gamma_bump_size = ShockUtilities.to_absolute(gamma_relative_bump_size, underlying_level, atm_vol)

        underlying_level_up_up = underlying_level + delta_bump_size + gamma_bump_size
        underlying_level_up_down = underlying_level + delta_bump_size - gamma_bump_size
        underlying_level_down_up = underlying_level - delta_bump_size + gamma_bump_size
        underlying_level_down_down = underlying_level - delta_bump_size - gamma_bump_size

        market_up_up     = Market(value_date, MarketData(underlying_level_up_up    , atm_vol, moneyness, skew_values, raw_discount_rate_curve))
        market_up_down   = Market(value_date, MarketData(underlying_level_up_down  , atm_vol, moneyness, skew_values, raw_discount_rate_curve))
        market_down_up   = Market(value_date, MarketData(underlying_level_down_up  , atm_vol, moneyness, skew_values, raw_discount_rate_curve))
        market_down_down = Market(value_date, MarketData(underlying_level_down_down, atm_vol, moneyness, skew_values, raw_discount_rate_curve))

        market_states[gamma_up_up] = market_up_up
        market_states[gamma_up_down] = market_up_down
        market_states[gamma_down_up] = market_down_up
        market_states[gamma_down_down] = market_down_down



