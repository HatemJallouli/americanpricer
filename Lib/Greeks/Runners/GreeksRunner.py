from typing import Dict

from Common.PricingResult import PricingResult
from Common.ResultKey import ResultKey
from Greeks.Common.GreeksDefinition import GreeksDefinition
from Greeks.GreekFillers.GreekFillersFactory import GreekFillersFactory
from Greeks.StateFillers.MarketStateFillersFactory import StateFillersFactory
from MarketQuotations.Market import Market
from Pricing.PV.PricingParameters import PricingParameters
from Pricing.PriceFillers.PriceFiller import PriceFiller
from Pricing.Runners.SequentialPvRunner import SequentialPvRunner
from Products.AmericanOption import AmericanOption
from Smile.Dynamics.IVolatilityDynamicsModel import IVolatilityDynamicsModel

from Common.ResultKeys import ResultKeys


class GreeksRunner:
    """ Class that computes the price of an american option and its greeks.

        Parameters
        ----------
        pricing_parameters : :class:`~Pricing.PV.PricingParameters`
            The numerical parameters to be passed to the american option pricer.
        greeks_definition : :class:`~Greeks.Common.GreeksDefinition`
            The description of bumps that are applied on market data for greeks computation.

    """
    def __init__(self, pricing_parameters : PricingParameters, greeks_definition : GreeksDefinition):
        self.__pricing_parameters = pricing_parameters
        self.__greeks_definition = greeks_definition

    def run(self, market: Market, volatility_model : IVolatilityDynamicsModel, american_option : AmericanOption) -> Dict[ResultKey, PricingResult]:
        """ Run price and greeks computation and return a dictionary of results.

            Parameters
            ----------
            market : :class:`~MarketQuotations.Market`
                The market data necessary to compute price and greeks.
            volatility_model : :class:`~Smile.Dynamics.IVolatilityDynamicsModel`
                The description of how the smile evolves given a market state.
            
            Returns
            -------
            greeks_result : Dict[:class:`~Common.ResultKey`, :class:`~Common.PricingResult`]
                The result of price and greeks computation in a dictionary format.

        """
        states_description_dict = dict()
        result_dic = dict()

        greeks_definition  = self.__greeks_definition
        pricing_parameters = self.__pricing_parameters

        underlying_level = market.market_data.underlying_level

        state_fillers = StateFillersFactory.build(market)

        for state_filler in state_fillers:
            state_filler.fill(greeks_definition, states_description_dict)

        pv_runner = SequentialPvRunner(american_option, volatility_model, pricing_parameters)

        pv_dict = pv_runner.run(states_description_dict)

        greek_fillers_factory = GreekFillersFactory(market)
        greek_fillers = greek_fillers_factory.build(greeks_definition)

        price_filler = PriceFiller()

        for greek_filler in greek_fillers:
            greek_filler.fill(pv_dict, result_dic)

        price_filler.fill(pv_dict, result_dic)



        return result_dic

