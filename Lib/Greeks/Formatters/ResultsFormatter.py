from typing import Dict

from Common.ResultKey import ResultKey

from Common.ResultKeys import ResultKeys


class ResultsFormatter:
    """ Class to convert a pricing result into a string.

    """
    def format(self, result_dic : Dict[ResultKey, float]) -> str:
        r"""Convert a pricing result into a readable string.

            Parameters
            ----------
            result_dic : Dict[:class:`~Common.ResultKey`, float])
                The result of a pricing expressed as a mapping of result keys to numerical values.

            Returns
            -------
            result_str : str`
                The string describing the pricing result.

            Examples
            --------
            >>> from Common.ResultKey import ResultKey
            >>> from Common.ResultKeys import ResultKeys
            >>> from Greeks.Formatters.ResultsFormatter import ResultsFormatter
            >>> result_dic = dict()
            >>> result_dic[ResultKeys.price] = 2.38
            >>> result_dic[ResultKeys.volga] = 0.34
            >>> result_dic[ResultKeys.underlying_level] = 54.3
            >>> formatter = ResultsFormatter()
            >>> formatter.format(result_dic)
            'F: 54.3\nPV: 2.38\nVolga: 0.34\n'

        """
        result = []

        key = ResultKeys.underlying_level
        if key in result_dic:
            result.append("F: " + str(result_dic[key]) + "\n")

        key = ResultKeys.atm_vol
        if key in result_dic:
            result.append("ATM: " + str(result_dic[key]) + "\n")

        key = ResultKeys.option_vol
        if key in result_dic:
            result.append("Option Vol: " + str(result_dic[key]) + "\n")

        key = ResultKeys.price
        if key in result_dic:
            result.append("PV: " + str(result_dic[key]) + "\n")

        key = ResultKeys.delta
        if key in result_dic:
            result.append("Delta: " + str(result_dic[key]) + "\n")

        key = ResultKeys.gamma
        if key in result_dic:
            result.append("Gamma: " + str(result_dic[key]) + "\n")

        key = ResultKeys.vega
        if key in result_dic:
            result.append("Vega: " + str(result_dic[key]) + "\n")

        key = ResultKeys.theta
        if key in result_dic:
            result.append("Theta: " + str(result_dic[key]) + "\n")

        key = ResultKeys.vanna
        if key in result_dic:
            result.append("Vanna: " + str(result_dic[key]) + "\n")

        key = ResultKeys.volga
        if key in result_dic:
            result.append("Volga: " + str(result_dic[key]) + "\n")

        key = ResultKeys.smile_risk(0.05)
        if key in result_dic:
            result.append("5P: " + str(result_dic[key]) + "\n")

        key = ResultKeys.smile_risk(0.25)
        if key in result_dic:
            result.append("25P: " + str(result_dic[key]) + "\n")

        key = ResultKeys.smile_risk(0.75)
        if key in result_dic:
            result.append("25C: " + str(result_dic[key]) + "\n")

        key = ResultKeys.smile_risk(0.95)
        if key in result_dic:
            result.append("5C: " + str(result_dic[key]) + "\n")

        return ''.join(result)

if __name__ == "__main__":
    import doctest
    doctest.testmod()