class GreeksDefinition:
    """ Container class to describe the bumps to be applied when computing greeks by finite differences.

        Parameters
        ----------
        delta_relative_bump_size : float
            The relative bump :math:`\epsilon` applied on the underlying :math:`S` for delta computation so that the 
            additive shock is :math:`\epsilon S \sigma \sqrt{1/252}` where :math:`\sigma` is the annualized 
            At-The-Money volatility.
        gamma_relative_bump_size : float
            The relative bump :math:`\epsilon` applied on the underlying :math:`S` for gamma computation so that the 
            additive shock is :math:`\epsilon S \sigma \sqrt{1/252}` where :math:`\sigma` is the annualized 
            At-The-Money volatility.
        vega_bump_size : float
            The additive bump :math:`\epsilon` applied on the At-The-Money annualized volatility :math:`\sigma` for 
            vega computation.
        theta_in_days_bump_size : float
            The additive bump in days :math:`\epsilon` applied on the pricing date :math:`t` for theta computation.
        rho_bump_size : float
            The additive bump :math:`\epsilon` applied on the interest rate :math:`r` for rho computation.
        volga_bump_size : float
            The additive bump :math:`\epsilon` applied on the At-The-Money annualized volatility :math:`\sigma` for 
            volga computation.
        smile_risk_bump_size : float
            The additive bump :math:`\epsilon` applied on the smile for smile risk computation.   
            
        Examples
        --------
        >>> from Greeks.Common.GreeksDefinition import GreeksDefinition
        >>> greeks_definition = GreeksDefinition(0.1,1.5,0.01,1,0.0001,0.01,0.01)
        >>> print(greeks_definition.vega_bump_size)
        0.01

    """
    def __init__(self, delta_relative_bump_size, gamma_relative_bump_size, vega_bump_size, theta_in_days_bump_size, rho_bump_size, volga_bump_size, smile_risk_bump_size):
        self.__delta_relative_bump_size = delta_relative_bump_size
        self.__gamma_relative_bump_size = gamma_relative_bump_size
        self.__vega_bump_size = vega_bump_size
        self.__theta_in_days_bump_size = theta_in_days_bump_size
        self.__rho_bump_size = rho_bump_size
        self.__volga_bump_size = volga_bump_size
        self.__smile_risk_bump_size = smile_risk_bump_size

    @property
    def delta_relative_bump_size(self):
        return self.__delta_relative_bump_size

    @property
    def gamma_relative_bump_size(self):
        return self.__gamma_relative_bump_size

    @property
    def vega_bump_size(self):
        return self.__vega_bump_size

    @property
    def theta_in_days_bump_size(self):
        return self.__theta_in_days_bump_size

    @property
    def rho_bump_size(self):
        return self.__rho_bump_size

    @property
    def volga_bump_size(self):
        return self.__volga_bump_size

    @property
    def smile_risk_bump_size(self):
        return self.__smile_risk_bump_size

if __name__ == "__main__":
    import doctest
    doctest.testmod()


