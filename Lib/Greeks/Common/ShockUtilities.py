import numpy as np


class ShockUtilities:
    """ Helper class to perform unit conversion on greeks shocks.

    """

    @staticmethod
    def to_absolute(relative_delta: float,  underlying_level :float, atm_vol : float) -> float:
        """ Static method the converts a relative underlying shock into an additive shock.

            Parameters
            ----------
            relative_delta : float
                The relative bump :math:`\epsilon` applied on the underlying :math:`S` for delta computation.
            underlying_level : float
                The market price of the underlying.
            atm_vol : float
                The At-The-Money volatility of the underlying.
                    
            Returns
            -------
            absolute_delta : float
                The additive bump :math:`\epsilon` applied on the underlying :math:`S` for delta computation.

            Examples
            --------
            >>> from Greeks.Common.ShockUtilities import ShockUtilities
            >>> relative_delta = 0.01
            >>> underlying_level = 100.0
            >>> atm_vol = 0.3
            >>> ShockUtilities.to_absolute(relative_delta, underlying_level, atm_vol)
            0.018898223650461361

        """
        return underlying_level * relative_delta * atm_vol * np.sqrt(1.0/252.0)


if __name__ == "__main__":
    import doctest
    doctest.testmod()
