from Common.ResultKey import ResultKey


class GreekKey(ResultKey):
    """ Container class to identify a greek computation result.

        Parameters
        ----------
        id : str
            String used to identify the greek.

        Examples
        --------
        >>> from Greeks.Common.GreekKey import GreekKey
        >>> greek_key = GreekKey("Vanna")
        >>> print(greek_key)
        Vanna

    """
    def __init__(self, id):
        super(GreekKey, self).__init__(id)

    @property
    def id(self):
        return super().id

    def __hash__(self):
        return super().__hash__()

    def __eq__(self, other):
        return super().__eq__(other) and isinstance(other, GreekKey)

    def __ne__(self, other):
        return not (self == other)

    def __str__(self):
        return super().__str__()


if __name__ == "__main__":
    import doctest
    doctest.testmod()


