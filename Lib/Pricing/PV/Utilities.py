import numpy as np

def Tau_From_Z(z, tau_max):
    return (0.5 * np.sqrt(tau_max) * (1 + z)) ** 2

def Chebychev_Nodes(n, tau_max):
  z = np.array([np.cos((i + 0.5) * np.pi / (n + 1)) for i in range(0,n+1)])
  return Tau_From_Z(z, tau_max)

def X_function(K,r,q, epsilon):
    if (q == 0.0):
        return K
    if (r == 0):
        return epsilon
    return K * np.minimum(1,r/q)






