from MarketQuotations.Market import Market

from Products.AmericanOption import AmericanOption


class IAmericanOptionPricer:
    """ General interface to describe an american option pricer.

        See Also
        --------
        AmericanOptionPricer : :class:`~Pricing.PV.AmericanOptionPricer`

    """
    def price(self, market : Market, american_option : AmericanOption) -> float:
        """ Compute the present value of the american option
        
        Parameters
        ----------
        market : :class:`~MarketQuotations.Market`
            The market that will be used to price the option.
        american_option : :class:`~Products.AmericanOption`
            The description of the american option that will be priced

        Returns
        -------
        price: float
            The present value of the american option.
        """
        raise NotImplementedError("Method {0} is an abstract method of interface {1}".format(self.price.__name__,
                                                                                             self.__class__.__name__))