from Pricing.PV.Utilities import *

#Factory that returns a Chebychev Interpolator given a discrete representation of the boundary
class Chebychev_Interpolator_Factory:

    def __init__(self, time_grid,  X, n, tau_max):
        self.__time_grid = time_grid
        self.__X = X
        self.__n = n
        self.__tau_max = tau_max

    def __a_coefficients(self, q, n):
        range = np.arange(0, n + 1)
        matrix = np.cos(
            (np.ones((n + 1, n + 1)) * (range.reshape((1, n + 1)) + 0.5)) * (range.reshape((n + 1, 1))) * np.pi / (
            n + 1))
        return 2 * matrix.dot(q) / (n + 1)

    def __H_from_B(self, B, X):
        return np.log(B / X) ** 2

    def Build(self, B_values):
        H_Values = self.__H_from_B(B_values, self.__X)
        a_coefs = self.__a_coefficients(H_Values, self.__n)
        return Chebychev_Interpolator(self.__time_grid, a_coefs, self.__X, self.__n, self.__tau_max)



#Chebychev interpolator that uses the discrete representation of the boundary and permits an
#interpolation of the boundary using Chebychev polynomials on a transformation H = f(Boundary)
class Chebychev_Interpolator :

    def __init__(self, time_grid, a_coeffs,  X, n, tau_max):
        self.__time_grid = time_grid
        self.__a_coefs = a_coeffs
        self.__X = X
        self.__n = n
        self.__tau_max = tau_max

    #Clenshaw Algorithm implementation for Chebychev polynomials
    def __q_interpolation(self, coefs, z, n):
        b_n_plus_two = 0
        b_n_plus_one = 0
        b_n = coefs[-1]
        for k in range(n - 1, -1, -1):
            b_n, b_n_plus_one, b_n_plus_two = (coefs[k] + 2 * z * b_n - b_n_plus_one, b_n, b_n_plus_one)

        return 0.5 * (b_n - b_n_plus_two)

    def __B_from_H(self, H, X):
        return X * np.exp(-np.sqrt(np.maximum(H, 0.0)))

    def __Z_From_Tau(self, tau, tau_max):
        return -1 + 2 * np.sqrt(tau / tau_max)

    def value_at(self, tau):
        z = self.__Z_From_Tau(tau, self.__tau_max)
        interpol_in_H = self.__q_interpolation(self.__a_coefs, z, self.__n)
        return self.__B_from_H(interpol_in_H, self.__X)

    @property
    def X(self):
        return self.__X