from Pricing.PV.BS_Formulae import Black_Schole_Formulae_Provider
from Pricing.PV.FirstGuessMethod import FirstGuessMethod
from scipy import optimize
from scipy.stats import norm

from Pricing.PV.Utilities import *


def Boundary_First_Guess(time_grid, K, r, q, epsilon):
    return np.ones(time_grid.shape) * X_function(K,r,q, epsilon);

class First_Guess_Finder():

    def __init__(self, K, q, r, sigma, first_guess_method, epsilon):
        self.__first_guess_method = first_guess_method
        self.__K = K
        self.__q = q
        self.__r = r
        self.__sigma = sigma
        self.__omega = self.Omega(r,q,sigma)
        self.__bsfp = Black_Schole_Formulae_Provider()
        self.__epsilon = epsilon

    def Omega(self, r, q, sigma):
        return 2 * (r-q) / (sigma**2)

    def H(self, r, tau):
        return 1 - np.exp(-r*tau)

    def Lambda(self, omega, r, sigma, h):
        omega_minus_one = omega - 1
        return -0.5 * (omega_minus_one + np.sqrt(omega_minus_one ** 2 + 0.125 * r / (h * sigma**2)))

    def Lambda_Prime(self, omega, r , sigma, h):
         return  2 * r /((sigma**2) * (h**2) * np.sqrt((omega-1)**2 + 0.125 * r /(h * sigma**2)))

    def C_Zero(self, tau, B):
        r = self.__r
        K = self.__K
        q = self.__q
        sigma = self.__sigma
        bsfp = self.__bsfp
        h = self.H(r,tau)
        _lambda = self.Lambda(self.__omega, r,sigma,h)
        _lambda_prime = self.Lambda_Prime(self.__omega, r, sigma, h)
        omega = self.__omega
        theta = self.Theta(tau, B)
        mult = - ((1-h) * 2 * r / (sigma ** 2))/(2 * _lambda + omega - 1)
        add_1 = 1/h
        add_2 = - np.exp(r*tau) * theta / (r * (K - B - bsfp.put_price(r,q,sigma,tau, B, K)))
        add_3 = _lambda_prime / (2 * _lambda + omega - 1)
        return mult* (add_1 + add_2 + add_3)

    def Target(self,B, tau):
        K = self.__K
        r = self.__r
        q = self.__q
        sigma = self.__sigma
        bsfp = self.__bsfp
        d_plus = bsfp.d_plus(r,q,sigma,tau, B/K)
        term_1 = - np.exp(-q*tau) * norm.cdf(-d_plus)
        h = self.H(r, tau)
        _lambda = self.Lambda(self.__omega, r, sigma, h)
        term_2 = (_lambda+ self.C_Zero(tau, B)) * (K - B - bsfp.put_price(r,q,sigma,tau, B, K)) / B
        term_3 = 1
        return term_1 + term_2 + term_3

    def Theta(self, tau, B):
        r = self.__r
        q = self.__q
        K = self.__K
        sigma = self.__sigma
        bsfp = self.__bsfp
        d_minus = bsfp.d_minus(r,q,sigma,tau, B/K)
        d_plus = bsfp.d_plus(r,q,sigma,tau, B/K)
        term_1 = r * K * np.exp(-r*tau) * norm.cdf(-d_minus)
        term_2 = - q * B * np.exp(-q*tau) * norm.cdf(-d_plus)
        term_3 = - sigma * B * np.exp(-q*tau) * norm.pdf(d_plus) /(2*np.sqrt(tau))
        return term_1 + term_2 + term_3

    def Solve(self, tau_grid):
        if self.__first_guess_method == FirstGuessMethod.TRIVIAL:

            return Boundary_First_Guess(tau_grid, self.__K, self.__r, self.__q, self.__epsilon)

        elif self.__first_guess_method == FirstGuessMethod.SMART:

            n = len(tau_grid)
            result = np.zeros((n, 1))
            X = X_function(self.__K, self.__r, self.__q, self.__epsilon)
            for i in range(n):
                tau = tau_grid[i]
                result[i] = optimize.brentq(self.Target, 0.0, X , args=(tau,))

            return result
        else:
            raise ValueError("Method " + str (self.__first_guess_method) + " is not handled.")
