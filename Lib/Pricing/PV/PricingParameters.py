from Pricing.PV import FirstGuessMethod

from Pricing.PV import IterationType


class PricingParameters:
    """ Class that contains the numerical parameters to be passed to the american pricer
        
        Parameters
        ----------
        n : int
            Number of points in the time-discretization of the boundary.
        l : int
            Number of points for Gauss-Legendre quadrature for 1D integration inside an iteration of fixed-point algorithm.
        m : int
            Number of iterations in the fixed point algorithm.
        p : int
            Number of points for Gauss-Legendre quadrature for 1D integration for the final price when the boundary is 
            computed.
        first_guess_method : :class:`~PV.Pricing.FirstGuessMethod`
            The algorithm used to choose a first guess of the exercice boundary. 
            
            Values can be :member:`~PV.Pricing.FirstGuessMethod.Trivial` or :member:`~PV.Pricing.FirstGuessMethod.Smart`.
            
            * Trivial uses a flat-firest guess corresponding to K * Max(1,r/q)
            * Smart uses a solver in order to have a good first guess (slows down the pricing time, so use if you want 
            a more precise result)
        iteration_type : :class:`~PV.Pricing.IterationType`
            The iterative algorithm that is used in order to solve the fixed point problem :math:`f(B)=B`
             
             Can be "Richardson", "Partial_Newton_Jacobi" or "Newton_Jacobi" to solve the fixed point problem f(B) = B
        
            * Richardson uses a naive fixed-point iteration
            * Newton_Jacobi mixes Richardson iterations with the Jacobi-Newton way to solve :math:`g(B) = 0` where 
            :math:`g(B) = f(B) - B`
            * Partial_Newton_Jacobi is like Newton_Jacobi but the derivative expression is approximated by putting at 0 
            smaller terms (faster than Newton_Jacobi)
           
    """

    def __init__(self, n : int, l : int, m : int, p : int, first_guess_method : FirstGuessMethod, iteration_type : IterationType):
        self.__n = n
        self.__l = l
        self.__m = m
        self.__p = p
        self.__first_guess_method = first_guess_method
        self.__iteration_type = iteration_type

    @property
    def n(self):
        return self.__n

    @property
    def l(self):
        return self.__n

    @property
    def m(self):
        return self.__m

    @property
    def p(self):
        return self.__p

    @property
    def first_guess_method(self):
        return self.__first_guess_method

    @property
    def iteration_type(self):
        return self.__iteration_type