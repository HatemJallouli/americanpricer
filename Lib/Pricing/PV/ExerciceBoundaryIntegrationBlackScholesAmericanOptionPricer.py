from datetime import date

from Calendars.CalendarTimeMeasure import CalendarTimeMeasure
from Pricing.PV.ExerciceBoundaryIntegrationAmericanPricer import ExerciceBoundaryIntegrationAmericanPricer
from Pricing.PV.IBlackScholesAmericanOptionPricer import IBlackScholesAmericanOptionPricer
from Pricing.PV.PricingParameters import PricingParameters
from Products.OptionType import OptionType

from Products.AmericanOption import AmericanOption


class ExerciceBoundaryIntegrationBlackScholesAmericanOptionPricer(IBlackScholesAmericanOptionPricer):
    """ Class that computes the present value of an american option in a Black-Scholes model using a 1D integration
        of a functional of the exercise boundary.

        Parameters
        ----------
        value_date : date
            The date for which the value will be computed
        pricing_parameters : :class:`~Pricing.PV.PricingParameters`
            The numerical parameters to be passed to the american option pricer.

    """
    def __init__(self, value_date : date, pricing_parameters : PricingParameters):
        self.__time_measure = CalendarTimeMeasure(value_date)
        self.__pricing_parameters = pricing_parameters

    def price(self, underlying_level: float, interest_rate: float, option_vol: float,
              american_option: AmericanOption) -> float:
        """ Compute the present value of the american option.

            Parameters
            ----------
            underlying_level : float
                The market quotation of the underlying of the american option
            interest_rate : float
                The Black-Scholes constant interest rate to be used.
            option_vol : float
                The Black-Scholes constant volatility paramter to be used.
            american_option : :class:`Products.AmericanOption`
                The american option to be priced

            Returns
            -------
            price : float
                The present value of the american option

        """
        pricing_parameters = self.__pricing_parameters

        strike = american_option.strike
        bCall = american_option.option_type == OptionType.CALL
        duration = self.__time_measure.to_duration(american_option.expiry_date)

        pricer = ExerciceBoundaryIntegrationAmericanPricer(underlying_level, strike, interest_rate, interest_rate, option_vol,
                                                           bCall, duration,
                                                           pricing_parameters)

        return pricer.PV(duration)