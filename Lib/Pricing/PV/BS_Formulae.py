import numpy as np
from scipy.stats import norm


#Provider of classical BS formulae
class Black_Schole_Formulae_Provider :

    def d_plus(self, r, q, sigma, tau, z):
        return (np.log(z) + (r-q + 0.5 * sigma**2) * tau) / (sigma * np.sqrt(tau))

    def d_minus(self,r, q, sigma, tau, z):
        return (np.log(z) + (r-q - 0.5 * sigma**2) * tau) / (sigma * np.sqrt(tau))

    def put_price(self, r, q, sigma, tau, S, K):
        d_minus = self.d_minus(r, q, sigma, tau,S/K)
        d_plus = self.d_plus(r, q, sigma, tau, S/K)
        return np.exp(-r * tau) * K * norm.cdf(-d_minus) - S * np.exp(-q * tau) * norm.cdf(-d_plus)

    def call_price(self, r, q, sigma, tau, S, K):
        d_minus = self.d_minus(r, q, sigma, tau,S/K)
        d_plus = self.d_plus(r, q, sigma, tau, S/K)
        return S * np.exp(-q * tau) * norm.cdf(d_plus)-np.exp(-r * tau) * K * norm.cdf(d_minus)

    def put_intrinstic_value(self, r, q, tau, S, K):
        diff = np.exp(-r * tau) * K  - S * np.exp(-q * tau)
        return diff * (diff > 0)
    def call_intrinstic_value(self,r, q, tau, S, K):
        diff = S * np.exp(-q * tau) -np.exp(-r * tau) * K
        return diff * (diff > 0)
