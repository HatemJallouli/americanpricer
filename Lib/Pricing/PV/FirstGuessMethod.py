from enum import Enum


class FirstGuessMethod(Enum):
    TRIVIAL = 1
    SMART = 2