from Pricing.PV.BS_Formulae import Black_Schole_Formulae_Provider
from Pricing.PV.FirstGuess import First_Guess_Finder
from Pricing.PV.Interpolator import *
from Pricing.PV.PricingParameters import PricingParameters
from Pricing.PV.Quadrature import *
from scipy.stats import norm

from Pricing.PV.IterationType import IterationType



class ExerciceBoundaryIntegrationAmericanPricer:

    """American options pricer.
    
    Compute the exercise boundary and price of an american option.

    Idea : Estimate an exercise boundary t -> B(t) such that if S_t < B(t) then exercise put
    Computation of the boundary is done by a fixed-point algorithm with a well-chosen contracting function in order
    to converge rapidly.

    Once the boundary is computed, several American options prices for different maturities are obtained using a 1D 
    integration of a functional of B

    Parameters
    ----------
    S : float
        Spot value of the underlying.
    K : float
        Strike of the option.
    q : float
        Continuous dividend-yield or spread over risk-free in Q-risk neutral diffusion in a general sense.
    r : float
        Continuous risk-free interest rate.    
    sigma : float
        Annualized volatility.
    bCall : bool
        True if pricing a call option and False if pricing a put option
    tau_max : float
        Maximum maturity expressed in year of the options that one desires to price.
    pricing_parameters : :class:`~PV.PricingParameters`
        The numerical parameters to be passed to the american pricer.
       
    References
    ----------
    
    .. [Andersen2015]
       L. Andersen, M. Lake, D. Offengenden
       *High Performance American Option Pricing*.
       https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2547027.

    """

    def __init__(self, S, K, q, r,sigma, bCall, tau_max, pricing_parameters : PricingParameters):

        n = pricing_parameters.n
        l = pricing_parameters.l
        m = pricing_parameters.m
        p = pricing_parameters.p
        first_guess_method = pricing_parameters.first_guess_method
        iteration_type = pricing_parameters.iteration_type

        y,w = Gauss_Legendre(l)
        y_p, w_p = Gauss_Legendre(p)
        tau = Chebychev_Nodes(n,tau_max)
        epsilon = 1e-12
        #This is a trick in order to convert an American Call to an equivalent American Put in Black-Scholes Model
        #Reference : McDonald, R. and M. Schroder (1998), “A Parity Result for American Options,” Journal
        #of Computational Finance, 1, 5–13
        
        effective_q = q * (1-bCall) + r * bCall
        effective_r = r * (1-bCall) + q * bCall
        effective_K = K * (1-bCall) + S * bCall
        effective_S = S * (1-bCall) + K * bCall

        B = First_Guess_Finder(effective_K,effective_q,effective_r,sigma,first_guess_method,epsilon).Solve(tau)
        self.__n = n
        self.__iteration_type = iteration_type
        self.__l = l
        self.__m = m
        self.__p = p
        self.__y = y.reshape((1,l))
        self.__w = w.reshape((1,l))
        self.__y_p = y_p
        self.__w_p = w_p
        self.__q = effective_q
        self.__r = effective_r
        self.__B = B.reshape((n+1,1))
        self.__K = effective_K
        self.__S = effective_S
        self.__epsilon = epsilon
        self.__X = X_function(effective_K,effective_r,effective_q, epsilon)
        self.__sigma = sigma
        self.__bsfp = Black_Schole_Formulae_Provider()
        self.__tau = tau.reshape((n+1,1))
        self.__tau_max = tau_max
        self.__cif = Chebychev_Interpolator_Factory(self.__tau, self.__X, n, self.__tau_max)
        self.__B_interp = self.__cif.Build(self.__B)
        self.__boundary_computed = False

    def __Solve_Boundary(self):

        bsfp = self.__bsfp
        sigma = self.__sigma
        r = self.__r
        q = self.__q
        tau = self.__tau
        K = self.__K
        y = self.__y

        for j in range (0, self.__m):

            B = self.__B
            B_interp = self.__B_interp

            d_minus = bsfp.d_minus(r,q,sigma,tau, B / K)
            d_plus = bsfp.d_plus(r,q,sigma,tau, B / K)
            denom = sigma * np.sqrt(tau)

            term = 0.25 * tau * (1 + y) ** 2
            interpolation = B_interp.value_at(tau - term)
            d_plus_interpol = bsfp.d_plus(r,q,sigma,term, B / interpolation)
            d_minus_interpol = bsfp.d_minus(r,q,sigma,term, B/interpolation)

            Integrand_K1 = 0.5 * np.exp(q * (tau - term)) * tau * (y + 1) * norm.cdf(d_plus_interpol)
            Integrand_K2 = np.exp(q * (tau - term)) * np.sqrt(tau) * norm.pdf(d_plus_interpol) / sigma
            Integrand_K3 = np.exp(r * (tau - term)) * np.sqrt(tau) * norm.pdf(d_minus_interpol) / sigma

            K1 = np.sum(Integrand_K1 * self.__w, axis=1).reshape((self.__n + 1, 1))
            K2 = np.sum(Integrand_K2* self.__w,axis=1).reshape((self.__n + 1, 1))
            K3 = np.sum(Integrand_K3 * self.__w,axis=1).reshape((self.__n + 1, 1))

            N = norm.pdf(d_minus) / denom + self.__r * K3
            D = norm.pdf(d_plus) / denom + norm.cdf(d_plus) + self.__q * (K1 + K2)

            K_Star = self.__K * np.exp(-(r - q) * tau)
            F_function = K_Star * N / D

            if self.__iteration_type == IterationType.RICHARDSON:

                F_Prime_Function = 0

            elif self.__iteration_type == IterationType.PARTIAL_NEWTON_JACOBI:

                N_Prime = - d_minus * (1 / (B * (denom ** 2))) * norm.pdf(d_minus)
                D_Prime = - d_minus * norm.pdf(d_plus) / (self.__B * denom ** 2)
                F_Prime_Function = K_Star / D * (N_Prime - D_Prime * N / D)

            elif self.__iteration_type == IterationType.NEWTON_JACOBI:

                N_Prime = - d_minus * (1 / (B * (denom ** 2))) * norm.pdf(d_minus)
                D_Prime = - d_minus * norm.pdf(d_plus) / (self.__B * denom ** 2)

                Integrand_K3_Prime = 2 * np.exp(r * (tau - term)) * (1 / ((sigma ** 2) * (1 + y))) * d_minus_interpol * norm.pdf(d_minus_interpol) * (1 / B - 1 / interpolation)
                K3_Prime = np.sum(Integrand_K3_Prime * self.__w, axis=1).reshape((self.__n + 1, 1))
                N_Prime += - r * K3_Prime

                Integrand_K1_Prime = np.exp(q * (tau - term)) * np.sqrt(tau) * norm.pdf(d_plus_interpol) * (1 / B - 1 / interpolation) / sigma
                Integrand_K2_Prime = 2 * np.exp(q * (tau - term)) * (1 / ((sigma ** 2) * (1 + y))) * d_plus_interpol * norm.pdf(d_plus_interpol) * (1 / B - 1 / interpolation)
                K1_Prime = np.sum(Integrand_K1_Prime * self.__w, axis=1).reshape((self.__n + 1, 1))
                K2_Prime = np.sum(Integrand_K2_Prime * self.__w, axis=1).reshape((self.__n + 1, 1))
                D_Prime += q * K1_Prime
                D_Prime += - q * K2_Prime
                F_Prime_Function = K_Star / D * (N_Prime - D_Prime * N / D)


            else:
                raise ValueError("Iteration Type " + str(self.__iteration_type) + " is not handled.")


            h_Function = F_Prime_Function / (F_Prime_Function - 1)
            result = (1 - h_Function) * F_function + h_Function * B
            result = np.maximum(result, self.__epsilon)

            self.__B = result
            self.__B_interp = self.__cif.Build(result)

        return self.__B_interp

    def PV(self, tau):

        """Compute the present value of the american option of maturity :math:'tau'

        Parameters
        ---------
        tau : float
            Maturity of the american option
        Returns
        ------
        price : float 
               returns the price of the american option.
        """

        #If boundary computed once, no need to recompute
        if (not(self.__boundary_computed)):
            self.__Solve_Boundary()
            self.__boundary_computed = True

        #Once boundary computed we integrate in order to obtain the price
        bsfp = self.__bsfp
        sigma = self.__sigma
        r = self.__r
        q = self.__q
        K = self.__K
        S = self.__S
        y_p = self.__y_p
        B_interp = self.__B_interp
        european_put_price = bsfp.put_price(r, q, sigma, tau, S, K)
        term = 0.25 * tau * (1 + self.__y_p) ** 2
        interpolation = B_interp.value_at(tau - term)
        d_plus = bsfp.d_plus(r, q, sigma, term, S / interpolation)
        d_minus = bsfp.d_minus(r, q, sigma, term, S / interpolation)
        Integrand_Price_1 =  0.5 * r * K * np.exp(-r * term) * norm.cdf(-d_minus) * tau * (1 + y_p)
        Integrand_Price_2 = -0.5 * q * S * np.exp(-q * term) * norm.cdf(-d_plus)  * tau * (1 + y_p)
        integral_1 = np.sum(Integrand_Price_1 * self.__w_p)
        integral_2 = np.sum(Integrand_Price_2 * self.__w_p)
        return european_put_price + integral_1 + integral_2

