from Products.AmericanOption import AmericanOption


class IBlackScholesAmericanOptionPricer:
    """ General interface to describe an american option pricer in a Black-Scholes setup.
        
        See Also
        --------
        ExerciceBoundaryIntegrationBlackScholesAmericanOptionPricer : :class:`~Pricing.PV.ExerciceBoundaryIntegrationBlackScholesAmericanOptionPricer`
    
    """

    def price(self, underlying_level : float, interest_rate : float, option_vol : float, american_option : AmericanOption) -> float:
        """ Compute the present value of the american option.
        
        Parameters
        ----------
        underlying_level : float
            The market quotation of the underlying of the american option
        interest_rate : float
            The Black-Scholes constant interest rate to be used.
        option_vol : float
            The Black-Scholes constant volatility paramter to be used.
        american_option : :class:`Products.AmericanOption`
            The american option to be priced

        Returns
        -------
        price: float
            The present value of the american option
            
        """
        raise NotImplementedError("Method {0} is an abstract method of interface {1}".format(self.price.__name__,
                                                                                             self.__class__.__name__))


