from enum import Enum


class IterationType(Enum):
    RICHARDSON = 1
    NEWTON_JACOBI = 2
    PARTIAL_NEWTON_JACOBI = 3