from Calendars.CalendarTimeMeasure import CalendarTimeMeasure
from Common.PricingResult import PricingResult
from MarketQuotations.Market import Market
from Pricing.PV.PricingParameters import PricingParameters
from Pricing.PV.ExerciceBoundaryIntegrationBlackScholesAmericanOptionPricer import ExerciceBoundaryIntegrationBlackScholesAmericanOptionPricer
from Pricing.PV.IAmericanOptionPricer import IAmericanOptionPricer
from Rates.DiscountRateCurveInterpolationFactory import DiscountRateCurveInterpolationFactory
from Smile.Dynamics.IVolatilityDynamicsModel import IVolatilityDynamicsModel
from Smile.Interpolation.CubicSplineImpliedVolatilityInterpolator import CubicSplineImpliedVolatilityInterpolator
from Smile.Representations.MoneynessSmileRepresentation import MoneynessSmileRepresentation
from Products.AmericanOption import AmericanOption


class AmericanOptionPricer(IAmericanOptionPricer):
    """ Class to compute the price of an american option using a Black-Scholes representation of volatility and given
        dynamics on the implied volatility smile.

        Parameters
        ----------
        volatility_model : :class:`~Smile.Dynamics.IVolatilityDynamicsModel`
            A description of how implied volatility evolves according to market evolution.
        pricing_parameters : :class:`~Pricing.PV.PricingParameters`
            The numerical parameters to be passed to the american pricer.

    """
    def __init__(self, volatility_model : IVolatilityDynamicsModel, pricing_parameters : PricingParameters):
        self.__volatility_model = volatility_model
        self.__pricing_parameters = pricing_parameters

    def price(self, market : Market,  american_option : AmericanOption) -> PricingResult:
        """ Compute the present value of the american option

            Parameters
            ----------
            market : :class:`~MarketQuotations.Market`
                The market that will be used to price the option.
            american_option : :class:`~Products.AmericanOption`
                The description of the american option that will be priced

            Returns
            -------
            price : PricingResult
                The present value of the american option and additional information.
        """
        vol_model = self.__volatility_model
        pricing_parameters = self.__pricing_parameters

        value_date = market.ref_date
        market_data = market.market_data

        strike = american_option.strike

        underlying_level = market_data.underlying_level
        raw_discount_rate_curve = market_data.raw_discount_rate_curve
        moneyness_smile = vol_model.smile(market)

        time_measure = CalendarTimeMeasure(value_date)
        duration = time_measure.to_duration(american_option.expiry_date)
        rate_interpolator = DiscountRateCurveInterpolationFactory.log_cubic_interpolation(value_date, raw_discount_rate_curve)
        interest_rate = rate_interpolator.linear_rate(american_option.expiry_date)
        vol_interpolator = CubicSplineImpliedVolatilityInterpolator(moneyness_smile, underlying_level, duration)
        option_vol = vol_interpolator.vol_at_strike(strike)
        pricer = ExerciceBoundaryIntegrationBlackScholesAmericanOptionPricer(value_date, pricing_parameters)

        pv = pricer.price(underlying_level, interest_rate, option_vol, american_option)
        return PricingResult(pv, underlying_level, moneyness_smile.atm_vol, option_vol)