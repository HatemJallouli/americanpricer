from numpy.polynomial import legendre as lg


# Returns abscissa and weights for Gauss-Legendre quadrature of order l
def Gauss_Legendre(l):
    x, w = lg.leggauss(l)
    return x, w

