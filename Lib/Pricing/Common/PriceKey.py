from Common.ResultKey import ResultKey


class PriceKey(ResultKey):
    """ Container class to identify a price computation result.

        Parameters
        ----------
        id : str
            String used to identify the price.

        Examples
        --------
        >>> from Pricing.Common.PriceKey import PriceKey
        >>> price_key = PriceKey("Price")
        >>> print(price_key)
        Price

    """
    def __init__(self, id):
        super(PriceKey, self).__init__(id)

    @property
    def id(self):
        return super().id

    def __hash__(self):
        return super().__hash__()

    def __eq__(self, other):
        return super().__eq__(other) and isinstance(other, PriceKey)

    def __ne__(self, other):
        return not (self == other)

    def __str__(self):
        return super().__str__()

if __name__ == "__main__":
    import doctest
    doctest.testmod()