import threading
import time
from typing import Dict

from Common.MarketStateKey import MarketStateKey
from Common.PricingResult import PricingResult
from MarketQuotations.Market import Market
from Pricing.PV.AmericanOptionPricer import AmericanOptionPricer
from Pricing.PV.PricingParameters import PricingParameters
from Pricing.Runners.IPvRunner import IPvRunner
from Smile.Dynamics.IVolatilityDynamicsModel import IVolatilityDynamicsModel

from Products.AmericanOption import AmericanOption


class ParallelPvRunner(IPvRunner):
    """ Class that takes a set of market states and returns a mapping (market state -> price). It uses multithreading
        in order to compute the different prices in parallel.

        Parameters
        ----------
        american_option : :class:`~Products.AmericanOption`
            The american option that one wants to price.
        volatility_model : :class:`~Smile.Dynamics.IVolatilityDynamicsModel`
            A description of how implied volatility evolves according to market evolution.
        pricing_parameters : :class:`~Pricing.PV.PricingParameters`
            The numerical parameters to be passed to the american option pricer.
    """
    def __init__(self, american_option: AmericanOption, volatility_model : IVolatilityDynamicsModel, pricing_parameters: PricingParameters):
        self.__american_option = american_option
        self.__volatility_model = volatility_model
        self.__pricing_parameters = pricing_parameters

    def pv(self,
           state_key : MarketStateKey,
           market_states : Dict[MarketStateKey, Market],
           results : Dict[MarketStateKey, PricingResult],
           volatility_model: IVolatilityDynamicsModel,
           pricing_parameters : PricingParameters,
           american_option : AmericanOption):

        # print ("Start Pricing")

        american_option_pricer = AmericanOptionPricer(volatility_model, pricing_parameters)
        pv = american_option_pricer.price(market_states[state_key], american_option)
        results[state_key] = pv

        # print(threading.active_count())
        # print("End Pricing")


    def run(self, market_states: Dict[MarketStateKey, Market]) -> Dict[MarketStateKey, PricingResult]:
        """ Run price for each market state and return a mapping (market state -> price) .

            Parameters
            ----------
            market_states : Dict[:class:`~Common.MarketStateKey`, :class:`~MarketQuotations.Market`]
                A dictionary of different market market states identified by their keys.

            Returns
            -------
            price_results : Dict[:class:`~Common.MarketStateKey`, :class:`~Common.PricingResult`]
                A dictionary of prices and additional information associated to each market state key.

        """
        t1 = time.time()
        pricing_parameters = self.__pricing_parameters
        american_option = self.__american_option
        volatility_model = self.__volatility_model
        result = dict()

        threads = []

        for state_key, market_state in market_states.items():
            t = threading.Thread(target = self.pv, args = (state_key, market_states, result, volatility_model ,pricing_parameters, american_option))
            t.start()
            threads.append(t)

        for t in threads:
            t.join()

        # t2 = time.time()
        # this_time = (t2 - t1) * 1000.0

        # print("Available Threads = ", multiprocessing.cpu_count())
        #
        # print("Going through Parallel Runner took {0} ms".format(this_time))

        return result



