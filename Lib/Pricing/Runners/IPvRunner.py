from typing import Dict

from MarketQuotations.Market import Market

from Common.MarketStateKey import MarketStateKey


class IPvRunner:
    """ General interface that takes a set of market states and returns a mapping (market state -> price)

        See Also
        --------
        ParallelPvRunner : :class:`~Pricing.Runners.ParallelPvRunner`
        SequentialPvRunner : :class:`~Pricing.Runners.SequentialPvRunner`

    """
    def run(self, market_states: Dict[MarketStateKey, Market]) -> Dict[MarketStateKey, float]:
        """ Run price for each market state and return a mapping (market state -> price) .

            Parameters
            ----------
            market_states : Dict[:class:`~Common.MarketStateKey`, :class:`~MarketQuotations.Market`]
                A dictionary of different market market states identified by their keys.

            Returns
            -------
            price_results : Dict[:class:`~Common.MarketStateKey`, float]
                A dictionary of prices associated to each market state key.

        """
        raise NotImplementedError("Method {0} is an abstract method of interface {1}".format(self.run.__name__,
                                                                                             self.__class__.__name__))


