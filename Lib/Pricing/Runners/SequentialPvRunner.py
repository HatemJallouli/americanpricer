from typing import Dict

from Common.MarketStateKey import MarketStateKey
from Common.PricingResult import PricingResult
from MarketQuotations.Market import Market
from Pricing.PV.AmericanOptionPricer import AmericanOptionPricer
from Pricing.PV.PricingParameters import PricingParameters
from Pricing.Runners.IPvRunner import IPvRunner
from Smile.Dynamics.IVolatilityDynamicsModel import IVolatilityDynamicsModel

from Products.AmericanOption import AmericanOption


class SequentialPvRunner(IPvRunner):
    """ Class that takes a set of market states and returns a mapping (market state -> price). It runs the different
        prices sequentially.

        Parameters
        ----------
        american_option : :class:`~Products.AmericanOption`
            The american option that one wants to price.
        volatility_model : :class:`~Smile.Dynamics.IVolatilityDynamicsModel`
            A description of how implied volatility evolves according to market evolution.
        pricing_parameters : :class:`~Pricing.PV.PricingParameters`
            The numerical parameters to be passed to the american option pricer.
    """
    def __init__(self, american_option: AmericanOption, volatility_model : IVolatilityDynamicsModel, pricing_parameters: PricingParameters):
        self.__american_option = american_option
        self.__volatility_model = volatility_model
        self.__pricing_parameters = pricing_parameters

    def run(self, market_states: Dict[MarketStateKey, Market]) -> Dict[MarketStateKey, PricingResult]:
        """ Run price for each market state and return a mapping (market state -> price) .

            Parameters
            ----------
            market_states : Dict[:class:`~Common.MarketStateKey`, :class:`~MarketQuotations.Market`]
                A dictionary of different market market states identified by their keys.

            Returns
            -------
            price_results : Dict[:class:`~Common.MarketStateKey`, :class:`~Common.PricingResult`]
                A dictionary of prices and additional information associated to each market state key.

        """
        pricing_parameters = self.__pricing_parameters
        volatility_model = self.__volatility_model
        american_option = self.__american_option
        american_option_pricer = AmericanOptionPricer(volatility_model, pricing_parameters)
        result = dict()
        for state_key, market in market_states.items():
            pv = american_option_pricer.price(market, american_option)
            result[state_key] = pv

        return result

