from typing import Dict

from Common.ResultKey import ResultKey

from Common.MarketStateKey import MarketStateKey


class IPriceFiller:
    """ General interface that retrieves the relevant market states for price computation, computes the price and fills 
        the results dictionary.

        See Also
        --------
        PriceFiller: :class:`~Pricing.PriceFillers.PriceFiller`
        
    """
    def fill(self, pvs: Dict[MarketStateKey, float], results: Dict[ResultKey, float]) -> None:
        """ Retrieve the relevant market states for price computation, computes the price and fills the results dictionary.

            Parameters
            ----------
            pvs : Dict[:class:`~Common.MarketStateKey`, float]
                Dictionary that maps each market state to the option price given that state.
            results : Dict[:class:`~Common.ResultKey`, float]
                Dictionary that contains the results of price computation.

            Returns
            -------
            None
        """
        raise NotImplementedError("Method {0} is an abstract method of interface {1}".format(self.fill.__name__,
                                                                                             self.__class__.__name__))