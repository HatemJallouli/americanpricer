from typing import Dict

from Common.MarketStateKey import MarketStateKey
from Common.MarketStateKeys import MarketStateKeys
from Common.PricingResult import PricingResult
from Common.ResultKey import ResultKey
from Common.ResultKeys import ResultKeys

from Pricing.PriceFillers.IPriceFiller import IPriceFiller


class PriceFiller(IPriceFiller):
    """ Class that retrieves the relevant market states for price computation, computes the price and fills 
        the results dictionary.

    """
    def fill(self, pvs: Dict[MarketStateKey, PricingResult], results: Dict[ResultKey, float]) -> None:
        """ Retrieve the relevant market states for price computation, computes the price and fills the results dictionary.

            Parameters
            ----------
            pvs : Dict[:class:`~Common.MarketStateKey`, :class:`~Common.PricingResult`]
                Dictionary that maps each market state to the pricing result given that state.
            results : Dict[:class:`~Common.ResultKey`, float]
                Dictionary that contains the results of price computation.

            Returns
            -------
            None
        """
        price_result = pvs[MarketStateKeys.price]
        results[ResultKeys.price] = price_result.price
        results[ResultKeys.underlying_level] = price_result.underlying_level
        results[ResultKeys.atm_vol] = price_result.atm_vol
        results[ResultKeys.option_vol] = price_result.bs_vol