from datetime import date


class ITimeMeasure:
    """ 
    General interface to compute the duration in years between two dates.
    
    See Also
    --------
    CalendarTimeMeasure: :class:`~Calendars.CalendarTimeMeasure`
    """
    def to_duration(self, date : date) -> float:

        """ Compute the duration in years between the reference date and the inputted date.

            Parameters
            ----------
            date : date
                Date that one wants to convert to a duration.

            Returns
            -------
            duration : float
                The duration in years between the inputted date and the reference date.
        """

        raise NotImplementedError("Method {0} is an abstract method of interface {1}".format(self.to_duration.__name__,self.__class__.__name__))