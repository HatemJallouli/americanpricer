from enum import Enum


class ContractMonthCodes(Enum):
    """ Enum to represent the futures month codes.

        Examples
        --------
        >>> from Calendars.ContractMonthCodes import ContractMonthCodes
        >>> ContractMonthCodes.M.value
        6
    """
    F = 1
    G = 2
    H = 3
    J = 4
    K = 5
    M = 6
    N = 7
    Q = 8
    U = 9
    V = 10
    X = 11
    Z = 12
