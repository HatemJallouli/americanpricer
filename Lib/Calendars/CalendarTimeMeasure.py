from datetime import date

from Calendars.ITimeMeasure import ITimeMeasure


class CalendarTimeMeasure(ITimeMeasure):
    """ Class to compute the duration in years between two dates using a 365 days per yer convention.

        Parameters
        ----------
        base_date : date
            Reference date based on which duration calculation is performed.

    """

    def __init__(self, base_date: date):
        self.__base_date = base_date
        self.__one_over_365 = 1.0 / 365;

    def to_duration(self, date: date) -> float:
        """ Compute the duration in years between the reference date and the inputted date.

            Parameters
            ----------
            date : date
                Date that one wants to convert to a duration.

            Returns
            -------
            duration : float
                The duration in years between the inputted date and the reference date.

            Examples
            --------
            >>> from Calendars.CalendarTimeMeasure import CalendarTimeMeasure
            >>> from datetime import date
            >>> base_date = date(2017,1,5)
            >>> calendar_time_measure = CalendarTimeMeasure(base_date)
            >>> date = date(2019,1,5)
            >>> calendar_time_measure.to_duration(date)
            2.0

        """
        return (date - self.__base_date).days * self.__one_over_365