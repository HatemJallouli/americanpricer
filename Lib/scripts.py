# Script to run a volatility implicitation from a start date to an end end
from datetime import date, timedelta
from enum import Enum

import numpy as np
from Backends.Contract import Contract
from Configurations import Configurations
from Greeks.Formatters.ResultsFormatter import ResultsFormatter
from Greeks.Runners.GreeksRunner import GreeksRunner
from MarketQuotations.Market import Market
from MarketQuotations.MarketData import MarketData
from Products.AmericanOption import AmericanOption
from Products.OptionType import OptionType
from Rates.ConstantDiscountRateCurveFactory import ConstantDiscountRateCurveFactory
from Smile.Dynamics.LogLinearVolatilityDynamicsModel import LogLinearVolatilityDynamicsModel

from Smile.Implicitation.MoneynessSmileRepresentationBuilder import MoneynessSmileRepresentationBuilder


class Scripts(Enum):
    VOL_IMPLICITATION = 1
    PRICE_AND_GREEKS = 2

#Change the script to run in order to choose which script to be run
script_to_run = Scripts.PRICE_AND_GREEKS

if '__main__' == __name__:

    #Script that prints the implied volatility smile for a given contract between a start and an end date
    if (script_to_run == Scripts.VOL_IMPLICITATION):

        # start_value_date = date(2017, 1, 1)
        # end_value_date = date(2017,1,21)
        # contract_date = date(2017,12,1)
        # index_name = "GOLD"
        # contract = Contract(index_name, contract_date)
        #
        # db_config = Configurations.DataBase.DEFAULT_DB_CONFIG
        # pricing_parameters = Configurations.Pricing.DEFAULT_PRICING_PARAMETERS
        # moneynesses = np.array([0.05, 0.25, 0.5, 0.75, 0.95])
        #
        # smile_builder = MoneynessSmileRepresentationBuilder(db_config, pricing_parameters, moneynesses)
        #
        # smiles = smile_builder.build_for_dates(start_value_date,end_value_date, contract)
        # # for (value_date, smile) in smiles.items() :
        # #     print ("Smile for date {0}:\n{1}".format(value_date, smile))

        # from Backends.Contract import Contract
        from datetime import date
        contract_maturity = date(2017, 12, 1)
        index_name = "WTI"
        contract = Contract(index_name, contract_maturity)
        start_value_date = date(2017, 1, 1)
        end_value_date = date(2017, 1, 4)
        import numpy as np
        moneynesses = np.array([0.05, 0.25, 0.5, 0.75, 0.95])
        pricing_parameters = Configurations.Pricing.DEFAULT_PRICING_PARAMETERS
        database_config = Configurations.DataBase.DEFAULT_DB_CONFIG
        smile_builder = MoneynessSmileRepresentationBuilder(database_config, pricing_parameters, moneynesses)
        smiles = smile_builder.build_for_dates(start_value_date, end_value_date, contract)
        print("Smile for date {0}:\n{1}".format(end_value_date, smiles[end_value_date]))

    #Script that print price and greeks for a given american option
    if (script_to_run == Scripts.PRICE_AND_GREEKS):

        value_date = date(2017, 3, 14)
        maturity = date(2017, 11, 15)
        underlying_level = 50.0
        atm_vol = 0.3055
        moneyness = np.array([0.05, 0.25, 0.5, 0.75, 0.95])
        skew_values = np.array([0.238, 0.117, 0, -0.110, -0.110])
        interest_rate = 0.01
        constant_rates_factory = ConstantDiscountRateCurveFactory(value_date)
        raw_discount_rate_curve = constant_rates_factory.build(timedelta(days=1), maturity, interest_rate)
        steepness = -0.25
        market_data = MarketData(underlying_level=underlying_level, atm_vol=atm_vol, moneyness=moneyness,
                                 skew_values=skew_values, raw_discount_rate_curve=raw_discount_rate_curve)
        market = Market(value_date, market_data)
        volatility_model = LogLinearVolatilityDynamicsModel(underlying_level, steepness)
        american_option = AmericanOption(strike=60, option_type=OptionType.CALL, expiry_date=maturity)
        pricing_parameters = Configurations.Pricing.DEFAULT_PRICING_PARAMETERS
        greeks_definition = Configurations.Greeks.DEFAULT_GREEKS_DEFINITION
        greeks_runner = GreeksRunner(pricing_parameters, greeks_definition)
        greeks_result = greeks_runner.run(market, volatility_model, american_option)
        results_formatter = ResultsFormatter()
        print("############ PRICE & GREEKS #########")
        print("")
        print(results_formatter.format(greeks_result))
        print("")