from datetime import date


class IDiscountRateCurveInterpolator:
    """ General interface to interpolate a discount rate curve.
        See Also
        --------
        LogCubicSplineDiscountRateCurveInterpolator: :class:`~Rates.LogCubicSplineDiscountRateCurveInterpolator`
    """
    def discount_factor(self, maturity: date) -> float:
        """ Compute the discount factor for a given maturity.

            Parameters
            ----------
            maturity : date
                Maturity of the cash flow to be discounted.
            
            Returns
            -------
            discount_factor : float
                The quantity :math:`D(t,T)` that depend on a present instant :math:`t` and a future date :math:`T` so 
                that a cash flow :math:`C_T` paid at time :math:`T` has value :math:`D(t,T)C_T`
        """
        raise NotImplementedError(
            "Method {0} is an abstract method of interface {1}".format(self.discount_factor.__name__,
                                                                       self.__class__.__name__))

    def linear_rate(self, maturity: date) -> float:
        """ Compute the linear rate for a given maturity defined by :math:`ln(Df(0,T))/T` where :math:`Df(0,T)` is the 
            discount factor for maturity :math:`T`.

            Parameters
            ----------
            maturity : date
                Maturity of the linear rate.
            
            Returns
            -------
            linear_rate : float
                The quantity :math:`L(t,T)` that depend on a present instant :math:`t` and a future date :math:`T` so 
                that borrowing 1 unit of cash at time :math:`t` and returning it back at time :math:`T` costs 
                :math:`L(t,T)\delta_(t,T)` units of cash with :math:`\delta(t,T)` being the year fraction between time
                :math:`t` and :math:`T`
        """
        raise NotImplementedError(
            "Method {0} is an abstract method of interface {1}".format(self.discount_factor.__name__,
                                                                       self.__class__.__name__))


