class RateCurveId:
    """ Container class to identify a discount rate curve.

        Parameters
        ----------
        id : str
            String used to identify the discount rate curve.

        Examples
        --------
        >>> from Rates.RateCurveId import RateCurveId
        >>> rate_curve_id = RateCurveId("USD-LIBOR")
        >>> print(rate_curve_id)
        USD-LIBOR

    """
    def __init__(self, id):
        self.__id = id

    @property
    def id(self):
        return self.__id

    def __hash__(self):
        return hash(self.__id)

    def __eq__(self, other):
        return self.__id == other.__id and isinstance(other, RateCurveId)

    def __ne__(self, other):
        return not (self == other)

    def __str__(self):
        return self.__id

if __name__ == "__main__":
    import doctest
    doctest.testmod()
