from typing import List

from Rates.RateCurveId import RateCurveId


class RawDiscountRateCurve:
    """ Container class to describe a discount rate curve.

        Parameters
        ----------
        rate_curve_id : :class:`~Rates.RateCurveId`
            Name of the discount rate curve.
        durations : List[float]
            The discretization of the discount rate curve in terms of durations
        discount_factors : List[float]
            The values of the discount factors for each duration

        Examples
        --------
        >>> from Rates.RateCurveId import RateCurveId
        >>> from Rates.RawDiscountRateCurve import RawDiscountRateCurve
        >>> import numpy as np
        >>> rate_curve_id = RateCurveId("USD-LIBOR")
        >>> durations = np.array([0.0,1.0,2.0,3.0])
        >>> discounts = np.array([1.0,0.98,0.87,0.75])
        >>> raw_discount_rate_curve = RawDiscountRateCurve(rate_curve_id, durations, discounts)
        >>> print(raw_discount_rate_curve.discount_factors[2])
        0.87

    """
    def __init__(self, rate_curve_id : RateCurveId, durations : List[float], discount_factors : List[float]):
        self.__rate_curve_id = rate_curve_id
        self.__durations = durations
        self.__discount_factors = discount_factors

    @property
    def rate_curve_id(self):
        return self.__rate_curve_id

    @property
    def durations(self):
        return self.__durations

    @property
    def discount_factors(self):
        return self.__discount_factors

if __name__ == "__main__":
    import doctest
    doctest.testmod()