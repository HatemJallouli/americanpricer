from datetime import date, timedelta

import numpy as np
from Calendars.CalendarTimeMeasure import CalendarTimeMeasure
from Rates.RawDiscountRateCurve import RawDiscountRateCurve

from Rates.RateCurveId import RateCurveId


class ConstantDiscountRateCurveFactory:
    """ Class to create a discount rate curve corresponding to a constant instantaneous interest rate.

        Parameters
        ----------
        value_date : Date
            The quotation date corresponding to the discount rate curve.
    """
    def __init__(self, value_date : date):
        self.__value_date = value_date

    def build(self, periodicity : timedelta, end_date : date, interest_rate :float) -> RawDiscountRateCurve:
        """ Create a discount rate curve corresponding to a constant instantaneous interest rate.

            Parameters
            ----------
            periodicity : timedelta
                The discretization of the discount rate curve in terms of duration.
            end_date : date
                The last maturity of the discount rate curve.
            interest_rate : float
                The constant instantaneous interest rate.

            Examples
            --------
            >>> from Rates.ConstantDiscountRateCurveFactory import ConstantDiscountRateCurveFactory
            >>> from datetime import date
            >>> from datetime import timedelta
            >>> value_date = date(2017,5,7)
            >>> rate_curve_factory = ConstantDiscountRateCurveFactory(value_date)
            >>> periodicity = timedelta(days=90)
            >>> end_date = date(2019,5,7)
            >>> interest_rate = 0.01
            >>> rate_curve = rate_curve_factory.build(periodicity, end_date, interest_rate)
            >>> print(rate_curve.discount_factors[2])
            0.992630032118

        """
        time_measure = CalendarTimeMeasure(self.__value_date)
        durations = []
        discount_factors = []

        current_date = self.__value_date + periodicity
        while(current_date < end_date):
            duration = time_measure.to_duration(current_date)
            durations.append(duration)
            discount_factor = np.exp(-interest_rate * duration)
            discount_factors.append(discount_factor)
            current_date = current_date + periodicity

        terminal_duration = time_measure.to_duration(end_date)
        durations.append(terminal_duration)
        discount_factor = np.exp(-interest_rate * terminal_duration)
        discount_factors.append(discount_factor)

        durations = np.array(durations)
        discount_factors = np.array(discount_factors)

        curve_id = RateCurveId("USD - LIBOR")
        return RawDiscountRateCurve(curve_id, durations, discount_factors)

if __name__ == "__main__":
    import doctest
    doctest.testmod()