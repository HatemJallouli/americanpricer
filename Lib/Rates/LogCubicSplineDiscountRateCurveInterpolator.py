from datetime import date

import numpy as np
from Calendars.ITimeMeasure import ITimeMeasure
from scipy.interpolate import CubicSpline

from Rates.IDiscountRateCurveInterpolator import IDiscountRateCurveInterpolator


class LogCubicSplineDiscountRateCurveInterpolator(IDiscountRateCurveInterpolator):
    """ Class to interpolate the natural logarithm of the discount curve using a cubic spline.

        Parameters
        ----------
        time_measure : :class:`~Calendars.ITimeMeasure`
            Converts maturities into durations.
        spline : CubicSpline
            The cubic spline interpolation of the discount curve
        first_duration : float
            The first duration of the raw discount curve in order to prohibit extrapolation
        last_duration : float
            The last duration of the raw discount curve in order to prohibit extrapolation

    """
    def __init__(self, time_measure : ITimeMeasure, spline : CubicSpline, first_duration : float, last_duration : float):
        self.__time_measure = time_measure
        self.__spline = spline
        self.__first_duration = first_duration
        self.__last_duration = last_duration

    def discount_factor(self, maturity: date) -> float:
        """ Compute the discount factor for a given maturity.
 
            Parameters
            ----------
            maturity : date
                Maturity of the cash flow to be discounted.
            
            Returns
            -------
            discount_factor : float
                The quantity :math:`D(t,T)` that depend on a present instant :math:`t` and a future date :math:`T` so 
                that a cash flow :math:`C_T` paid at time :math:`T` has value :math:`D(t,T)C_T`  
        """
        duration = self.__time_measure.to_duration(maturity)
        return np.exp(-self.linear_rate(maturity) * duration)

    def linear_rate(self, maturity: date):
        """ Compute the linear rate for a given maturity defined by :math:`ln(Df(0,T))/T` where :math:`Df(0,T)` is the 
            discount factor for maturity :math:`T`.

            Parameters
            ----------
            maturity : date
                Maturity of the linear rate.
            
            Returns
            -------
            linear_rate : float
                The quantity :math:`L(t,T)` that depend on a present instant :math:`t` and a future date :math:`T` so 
                that borrowing 1 unit of cash at time :math:`t` and returning it back at time :math:`T` costs 
                :math:`L(t,T)\delta_(t,T)` units of cash with :math:`\delta(t,T)` being the year fraction between time
                :math:`t` and :math:`T`
        """
        duration = self.__time_measure.to_duration(maturity)

        if duration < self.__first_duration:
            raise ValueError("The discount factor duration {0} was before the first available curve duration {1}".format(maturity, self.__first_duration))

        if duration > self.__last_duration:
            raise ValueError("The discount factor duration {0} was after the last available curve duration {1}".format(maturity, self.__last_duration))

        return self.__spline(duration)