from datetime import date

import numpy as np
from Calendars.CalendarTimeMeasure import CalendarTimeMeasure
from Rates.LogCubicSplineDiscountRateCurveInterpolator import LogCubicSplineDiscountRateCurveInterpolator
from Rates.RawDiscountRateCurve import RawDiscountRateCurve
from scipy.interpolate import CubicSpline

from Rates.IDiscountRateCurveInterpolator import IDiscountRateCurveInterpolator


class DiscountRateCurveInterpolationFactory:
    """ Class to create an interpolation of a raw discount curve.

    """
    @staticmethod
    def log_cubic_interpolation(value_date : date, raw_discount_rate_curve : RawDiscountRateCurve) -> IDiscountRateCurveInterpolator:
        """ Create a cubic spline interpolation of the natural logarithm of the discount curve.

            Parameters
            ----------
            value_date : date
                The quotation date of the discount rate curve.
            raw_discount_rate_curve : :class:`~Rates.RawDiscountRateCurve`
                The raw discount rate curve

            Examples
            --------
            >>> from Rates.DiscountRateCurveInterpolationFactory import DiscountRateCurveInterpolationFactory
            >>> from Rates.RawDiscountRateCurve import RawDiscountRateCurve
            >>> from datetime import date
            >>> import numpy as np
            >>> value_date = date(2017,5,7)
            >>> rate_curve_id = "USD-LIBOR"
            >>> durations = np.array([1.0,2.0,3.0])
            >>> discounts = np.array([0.98, 0.85, 0.78])
            >>> raw_discount_rate_curve = RawDiscountRateCurve(rate_curve_id, durations, discounts)
            >>> interpolated_curve = DiscountRateCurveInterpolationFactory.log_cubic_interpolation(value_date, raw_discount_rate_curve)
            >>> print(interpolated_curve.discount_factor(date(2019,5,7)))
            0.85
            >>> print(interpolated_curve.discount_factor(date(2020,4,1)))
            0.780691908992

        """
        durations = raw_discount_rate_curve.durations
        discount_factors = raw_discount_rate_curve.discount_factors

        time_measure = CalendarTimeMeasure(value_date)
        x = []
        y = []
        for i in range (len(durations)):
            duration = durations[i]
            discount_factor = discount_factors[i]
            x.append(duration)
            y.append(-np.log(discount_factor)/duration)

        x = np.array(x)
        y = np.array(y)
        cs = CubicSpline(x,y)
        return LogCubicSplineDiscountRateCurveInterpolator(time_measure, cs, 0.0, durations[-1])

if __name__ == "__main__":
    import doctest
    doctest.testmod()